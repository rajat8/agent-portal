import { Component, OnInit } from '@angular/core';
import{Validators,FormControl,FormGroup} from '@angular/forms';
import { NgbModal,NgbModalRef,NgbModule } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-value',
  templateUrl: './value.component.html',
  styleUrls: ['./value.component.scss']
})
export class ValueComponent implements OnInit {
  constructor() { }
  screen1;screen2;screen3;screen4;lengthError;voucher;
  transType;
  private transferForm;
  hero;
   ngOnInit() {
     this.screen1=true;
     this.screen2=false;
     this.screen3=false;
     this.screen4=false;
 
     this.transferForm = new FormGroup({
       'transactionType': new FormControl( [
         Validators.required,
         Validators.minLength(10),
        
       ]),
       'ReceiverActorId': new FormControl( [
         Validators.required,
         Validators.minLength(10),
        
       ]),
       'amount': new FormControl( [
         Validators.required,
         Validators.minLength(10),
        
       ]),
       'mpin': new FormControl( [
         Validators.required,
         Validators.minLength(10),
        
       ])
     });
   }
 screen2Show(type){
   if(type=='01'){
     this.transType='By Value';
   }
   else{
     this.transType='By Fiscal Stamp';
   }
   var a=this.transferForm.ReceiverActorId;
   var e=this.transferForm.mpin;
   var f=this.transferForm.amount;
 if(a!=undefined && e!=undefined && f!=undefined){
   if(a.toString().length!=0){
     this.screen2=false;
     this.screen1=false;
     this.screen3=true;
     this.screen4=false;
    }   
    if(a.toString().length==0) 
    { Swal.fire('Invalid Receiver ActorId', 'Receiver ActorId cannot be empty ') }
   
    if(type=='02'){
      this.voucher='e-voucher';
    }
 }
 else{
  Swal.fire('Operation Failed', 'All fields are required')
 }
 }
 screen3Show(){
   

 
   Swal.fire('Operation Failed', 'Amount and mpin cannot be empty');
  
    this.screen3=true;
    this.screen1=false;
    this.screen2=false;
    this.screen4=false;
   
  
   Swal.fire('Operation Failed', 'Amount and mpin cannot be empty');
 }
 cancel(){
   this.screen1=true;
   this.screen3=false;
   this.screen2=false;
   this.screen4=false;
 }
 screen4Show(){
  this.screen4=true;
  this.screen1=false;
   this.screen3=false;
   this.screen2=false;
 }

}
