import { Component, OnInit } from '@angular/core';
import{Validators,FormControl,FormGroup} from '@angular/forms';
import { NgbModal,NgbModalRef,NgbModule } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-e-top-up',
  templateUrl: './e-top-up.component.html',
  styleUrls: ['./e-top-up.component.scss']
})
export class ETopUpComponent implements OnInit {

  constructor(private modal:NgbModal) { }
 screen1;
 private topupForm;
 hero;
  ngOnInit() {
    this.screen1=true;


    this.topupForm = new FormGroup({
      'phone': new FormControl( [
        Validators.required,
        Validators.minLength(10),
       
      ]),
      'mpin': new FormControl( [
        Validators.required,
        Validators.minLength(10),
       
      ])
    });
  }

}
