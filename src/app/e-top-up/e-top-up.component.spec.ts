import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ETopUpComponent } from './e-top-up.component';

describe('ETopUpComponent', () => {
  let component: ETopUpComponent;
  let fixture: ComponentFixture<ETopUpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ETopUpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ETopUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
