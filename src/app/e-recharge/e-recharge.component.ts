import { Component, OnInit } from '@angular/core';
import{Validators,FormControl,FormGroup} from '@angular/forms';
import { NgbModal,NgbModalRef,NgbModule } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';
import{ApiService} from '../Services/api.service';
import{AuthGuard} from '../Services/auth.guard';
import { isNumber } from '@ng-bootstrap/ng-bootstrap/util/util';
import{AppConfig} from '../app.config';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import {  ViewportScroller } from '@angular/common'; 
import { interval as observableInterval } from "rxjs";
import { takeWhile, scan, tap } from "rxjs/operators";
import PerfectScrollbar from 'perfect-scrollbar';
import{AdminLayoutComponent} from '../layouts/admin-layout/admin-layout.component'
@Component({
  selector: 'app-e-recharge',
  templateUrl: './e-recharge.component.html',
  styleUrls: ['./e-recharge.component.scss']
})
export class ERechargeComponent implements OnInit {

  constructor( private unlock:AuthGuard,private router: Router,private viewPortScroller: ViewportScroller,config: NgbModalConfig, private modalService: NgbModal,private apiService:ApiService,private spinnerService: Ng4LoadingSpinnerService,private ad:AdminLayoutComponent
    ) {
    config.backdrop = 'static';
    config.keyboard = false;
   } 
 screen1;screen2;screen3;lengthError;voucher;amount:number;operator='';agenttype;totalamountwords;
 reviewVat:number;reviewTotal:number;
 topupForm;msisdnError=false;msisdnLength=AppConfig.msisdnLength;msisdnErrorText=AppConfig.msisdnError;
 hero;
 clicked = false;
 checkmsisdnFieldLength(elem){
  let upperCaseCharacters = /[A-Z]+/g;
  let lowerCaseCharacters = /[a-z]+/g;
  let numberCharacters = /[0-9]+/g;
  let specialCharacters = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;
  if ((this.topupForm.phone.toString().length != this.msisdnLength && (this.topupForm.phone.toString().length!=0 || this.topupForm.phone.toString().length!=undefined)) || numberCharacters.test(this.topupForm.phone) === false || upperCaseCharacters.test(this.topupForm.phone) === true || lowerCaseCharacters.test(this.topupForm.phone) === true  || specialCharacters.test(this.topupForm.phone) === true) {
   this.msisdnError=true;
          
      }
      else{
        this.msisdnError=false;
      }
  }

 a = [
  '',
  'One ',
  'Two ',
  'Three ',
  'Four ',
  'Five ',
  'Six ',
  'Seven ',
  'Eight ',
  'Nine ',
  'Ten ',
  'Eleven ',
  'Twelve ',
  'Thirteen ',
  'Fourteen ',
  'Fifteen ',
  'Sixteen ',
  'Seventeen ',
  'Eighteen ',
  'Nineteen '];

b = [
  '',
  '',
  'Twenty',
  'Thirty',
  'Forty',
  'Fifty',
  'Sixty',
  'Seventy',
  'Eighty',
  'Ninety'];

transform(value: any, args?: any): any {
  if (value) {
    let num: any = Number(value);
    if (num) {
      if ((num = num.toString()).length > 9)  { return 'We are not the Iron Bank, you can lower down the stakes :)'; }
      const n = ('000000000' + num).substr(-9).match(/^(\d{2})(\d{2})(\d{2})(\d{1})(\d{2})$/);
      if (!n) {return ''; }
      let str = '';
      str += (Number(n[1]) !== 0) ? (this.a[Number(n[1])] || this.b[n[1][0]] + ' ' + this.a[n[1][1]]) + 'CRORE ' : '';
      str += (Number(n[2]) !== 0) ? (this.a[Number(n[2])] || this.b[n[2][0]] + ' ' + this.a[n[2][1]]) + 'LAKH ' : '';
      str += (Number(n[3]) !== 0) ? (this.a[Number(n[3])] || this.b[n[3][0]] + ' ' + this.a[n[3][1]]) + 'THOUSAND ' : '';
      str += (Number(n[4]) !== 0) ? (this.a[Number(n[4])] || this.b[n[4][0]] + ' ' + this.a[n[4][1]]) + 'HUNDRED ' : '';
      str += (Number(n[5]) !== 0) ? ((str !== '') ? 'and ' : '') +
      (this.a[Number(n[5])] || this.b[n[5][0]] + ' ' +
      this.a[n[5][1]]) + '' : '';
    //   //  // console.log(str);
      
      return str;
    } else {
      return '';
    }
  } else {
    return '';
  }
}
 withDecimal(n) {
    var nums = n.toString().split('.')
    var whole = this.transform(nums[0])
    if (nums.length == 2) {
        var fraction = this.transform(nums[1])
        return whole + 'Us Dollars And ' + fraction + 'Cents Only';
    } else {
        return whole + 'Us Dollars Only';
    }
}

 scrollToTop(el) {
  //  const elemMainPanel = <HTMLElement>document.querySelector('.main-panel');
  //  const ps = new PerfectScrollbar(elemMainPanel);
  //     ps.update();
  //   el.scrollTop = 0;
  //   elemMainPanel.scrollTop = 0;
  this.ad.scrollToTop();
  }
fiscalStamp;
  ngOnInit() {
    this.agenttype=this.unlock.decryptData(localStorage.getItem('agenttype'));
    this.Vat=this.unlock.decryptData((localStorage.getItem('Vat')));
    this.screen1=true;
    this.screen2=false;
    this.screen3=false;
    // const container = document.querySelector('.list_style');
    // const ps = new PerfectScrollbar(container);
     
    // or just with selector string

    this.topupForm = new FormGroup({
      'phone': new FormControl( [
        Validators.required,
        Validators.minLength(10),
       
      ]),
      'mpin': new FormControl( [
        Validators.required,
        Validators.minLength(10),
       
      ])
    });

    this.apiService.billerApi().subscribe(result=>{ var b:any=result; var resultcode=b.estel.response.resultcode; if(resultcode!=undefined && (resultcode==1956 || resultcode=='1956') ){this.router.navigate(['']);localStorage.clear();}
  //   //  // console.log("biller resp",result);  
    this.billerResult=result;
    this.billerResultDesc=this.billerResult.estel.response.resultdescription;
    if(this.billerResultDesc=='Transaction Successful'){
      this.biller=this.billerResult.estel.response.records.record;
      
     
    }
    else{
      Swal.fire('Operation Failed',this.billerResultDesc);
    }
    })
  
  }
  biller;billerResult;billerResultDesc;products;productCount;productsResult;productsResultDesc;

  onChangeoperatorEvent(ev) {
  //   //  // console.log(ev.target.value);
  this.operator=ev.target.value;this.productCount=undefined;
  if(ev.target.value)
  this.apiService.prodDetailsApi(ev.target.value).subscribe(result=>{ var b:any=result; var resultcode=b.estel.response.resultcode; if(resultcode!=undefined && (resultcode==1956 || resultcode=='1956') ){this.router.navigate(['']);localStorage.clear();}
    
  //   //  // console.log("prod resp",result);  
    this.productsResult=result;
    this.productsResultDesc=this.productsResult.estel.response.resultdescription;
    this.productCount=this.productsResult.estel.response.recordcount;
    if(this.productsResultDesc=='Transaction Successful'){
      
      this.products=this.productsResult.estel.response.records.record;
      // this.products.productdesc.split("\n");
      if(this.productCount>1){
        this.amount=this.products[0].amount;
        this.totalamt=this.products[0].totalamount;
        this.productcode=this.products[0].productcode;
        this.pricewithoutvat=this.products[0].pricewithoutvat;
      this.products=  this.products.sort((a,b) => a.pricewithoutvat - b.pricewithoutvat);
      for(var i=0;i<this.products.length;i++){
        this.products[i].pricewithoutvat=this.products[i].pricewithoutvat.toString();
        if(this.products[i].pricewithoutvat.split(".")!=undefined){
          this.products[i].pricewithoutvat=this.products[i].pricewithoutvat.split(".");
         
        }
        
        if(this.products[i].productdesc!=undefined)
        {this.products[i].productdesc=this.products[i].productdesc.split("delimitter");
        for(var j=0;j<this.products[i].productdesc.length;j++){
          this.products[i].productdesc[j]=this.products[i].productdesc[j].split(":");
        }
      }
       
      }
      }
      if(this.productCount==1){
        this.amount=this.products.amount;
        this.totalamt=this.products.totalamount;
        this.productcode=this.products.productcode;
        this.pricewithoutvat=this.products.pricewithoutvat;
        this.products.pricewithoutvat=this.products.pricewithoutvat.toString();
        if(this.products.pricewithoutvat.split(".")!=undefined){
          this.products.pricewithoutvat=this.products.pricewithoutvat.split(".");
         
        }
        this.products.productdesc=this.products.productdesc.split("delimitter");
        for(var j=0;j<this.products.productdesc.length;j++){
          this.products.productdesc[j]=this.products.productdesc[j].split(":");
        }
       // const ps = new PerfectScrollbar('.list_style');
        const elemMainPanel = <HTMLElement>document.querySelector('.list_style');
        const ps = new PerfectScrollbar(elemMainPanel);
           ps.update();
      }
    }
    else{
      Swal.fire('Operation Failed',this.productsResultDesc);
    }
    })
 }
 print(): void {
  let printContents, popupWin;
  printContents = document.getElementById('print-section').innerHTML;
  popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
  popupWin.document.open();
  popupWin.document.write(`
    <html>
      <head>
        <title>Print tab</title>
        <style>
        @media print {
          html, body {
            width: 7.2cm;
            height: 15cm;
          }
          prints{
              width: 7.2cm;
              height: 15cm;
          }
          .fsize-10{
            font-size: 12px !important;
          }
          .fsize-12{
            font-size: 14px !important;
          }
          .text-center{
            text-align:center;
          }
        
      }
        //........Customized style.......
        </style>
      </head>
  <body onload="window.print();window.close()">${printContents}</body>
    </html>`
  );
  popupWin.document.close();
  this.modalReference.close();
  
}
pricewithoutvat;totalamt;productcode;modalReference = null; 
 onChangeamountEvent(ev) {
  // if(ev.target.value!=undefined){this.productcode=ev.target.value}
   this.productcode=ev;
  for(var i=0;i<this.products.length;i++){
    if(this.products[i].productcode==this.productcode){
      this.amount=this.products[i].amount;
this.amount=Number(this.amount);
this.totalamt=this.products[i].totalamount;
this.pricewithoutvat=this.products[i].pricewithoutvat;
//  // console.log(this.amount,this.totalamt);
    }
  }

}
scroll(el: HTMLElement) {
  el.scrollIntoView();
    // console.log("scroll")
}
pCode;
screen2Show(amt,totalamt,pcode,taxcount,taxtypes){
this.onChangeamountEvent(pcode);
this.pCode=pcode;
//window.scrollTo(0, 0);

if(taxcount>1){
  for(var i=0;i<taxtypes.tax.length;i++){
      // console.log(taxtypes.tax[i].taxtype.length);
    if(taxtypes.tax[i].taxtype.length>5){
      taxtypes.tax[i].taxtype= taxtypes.tax[i].taxtype.split(":");
    }
    
    var type=taxtypes.tax[i].taxtype[2];
    if(type=='VAT'){
      this.Vat=taxtypes.tax[i].taxtype[0];
    }
    if(type=='Fiscal'){
      this.fiscalStamp=taxtypes.tax[i].taxtype[0];
    }
      // console.log(taxtypes.tax[i].taxtype);
  }
}
if(taxcount==1){
  if(taxtypes.tax.taxtype.length>5){
    taxtypes.tax.taxtype= taxtypes.tax.taxtype.split(":");
  }
    var type=taxtypes.tax.taxtype[2];
    if(type=='VAT'){
      this.fiscalStamp=undefined;
      this.Vat=taxtypes.tax.taxtype[0];
    }
    if(type=='Fiscal'){
      this.fiscalStamp=taxtypes.tax.taxtype[0];
    }
      // console.log(taxtypes.tax.taxtype);
  
}
if(taxcount==0){
  
  
    this.Vat=0;
  
    this.fiscalStamp=undefined;
  

}
if(this.pricewithoutvat.length>1){
  this.pricewithoutvat=this.pricewithoutvat[0]+'.'+this.pricewithoutvat[1];
}
  // console.log(taxcount,taxtypes);
  var a=this.topupForm.phone;
  this.reviewVat=this.pricewithoutvat*this.Vat/100;
  this.reviewVat=Math.round(this.reviewVat * 100) / 100;
  this.reviewTotal=this.amount+this.reviewVat+this.fiscalStamp;
   // console.log(this.Vat,this.reviewVat,this.pricewithoutvat);
  
 this.reviewTotal= Math.round(this.reviewTotal * 100) / 100;
 
 //  // console.log(this.reviewTotal,this.amount,this.fiscalStamp);
 if(a!=undefined && this.operator!='' && this.operator!=undefined && this.amount!=undefined){
  if(this.msisdnError==false){
    this.screen2=true;
    this.screen1=false;
    this.screen3=false;
 
   
//     this.viewPortScroller.scrollToPosition([0, 0]);
//     var scrollDiv = document.getElementById("scroll");
// scrollDiv.scrollTop = 0;
window.scroll({ 
  top: 0, 
  left: 0, 
  behavior: 'smooth' 
});
   }   
   if(this.msisdnError==true) 
   { Swal.fire('Operation failed', 'Msisdn is not valid') }
  
 
}
else{
  Swal.fire('Operation Failed', 'All Fields are required ')
}
}
topupResult;topupResultDesc;topupResponseTime;transid;amountWithoutVat;Vat;totalwithVat;respVat;respTotal;
screen3Show(content){
  this.spinnerService.show();
 
this.apiService.eRechargeApi(this.amount,this.operator,this.topupForm.phone,this.pCode).subscribe(result=>{ var b:any=result; var resultcode=b.estel.response.resultcode; if(resultcode!=undefined && (resultcode==1956 || resultcode=='1956') ){this.router.navigate(['']);localStorage.clear();}
//   //  // console.log("topup resp",result);  
  this.topupResult=result;
  this.topupResultDesc=this.topupResult.estel.response.resultdescription;
  if(this.topupResultDesc=='Transaction Successful'){
    this.respVat=this.topupResult.estel.response.amount*this.Vat/100;
  //   //  // console.log(this.respVat);
    this.respVat= Math.round(this.respVat * 100) / 100;
    this.topupResult.estel.response.responsects=this.topupResult.estel.response.responsects.split(" ");
    this.topupResult.estel.response.responsects[1]=this.tConvert(this.topupResult.estel.response.responsects[1]);
    var total=Math.round(this.fiscalStamp+this.topupResult.estel.response.amount + this.topupResult.estel.response.amount*this.Vat/100)
    
    this.totalamountwords=this.withDecimal(this.topupResult.estel.response.totalamount);;
      this.totalamountwords.toUpperCase( );
    this.screen3=true;
    this.screen1=false;
    this.screen2=false;
    this.operator='';
    this.amount=0;
    this.productCount=undefined;
    this.topupForm.phone=undefined;
    this.spinnerService.hide();
    this.modalReference =  this.modalService.open(content);
    
  }
  else{
    if(this.topupResultDesc=='Insufficient Pin')
      {Swal.fire('Operation Failed','Product Out of Stock');
      this.spinnerService.hide();}

      else{
        if(this.topupResultDesc=='Total Amount Of Daily Transactions Limit Reached' || this.topupResult.estel.response.resultcode==431 || this.topupResult.estel.response.resultcode==432 || this.topupResult.estel.response.resultcode==433 || this.topupResult.estel.response.resultcode==434 || this.topupResult.estel.response.resultcode==435 || this.topupResult.estel.response.resultcode==436 || this.topupResult.estel.response.resultcode==437 || this.topupResult.estel.response.resultcode==4377 || this.topupResult.estel.response.resultcode=='431' || this.topupResult.estel.response.resultcode=='432' || this.topupResult.estel.response.resultcode=='433' || this.topupResult.estel.response.resultcode=='434' || this.topupResult.estel.response.resultcode=='435' || this.topupResult.estel.response.resultcode=='436' || this.topupResult.estel.response.resultcode=='437' || this.topupResult.estel.response.resultcode=='4377' ){
          if(this.topupResult.estel.response.resultcode==431 || this.topupResult.estel.response.resultcode=='431'){
            Swal.fire('Operation Failed', 'You have reached the Daily amount maximum transaction limit.Kindly contact our Help Line on 119 (free of charge)');
            this.spinnerService.hide();
          }
          if(this.topupResult.estel.response.resultcode==432 || this.topupResult.estel.response.resultcode=='432'){
            Swal.fire('Operation Failed', 'You have reached the Monthly amount maximum transaction limit.Kindly contact our Help Line on 119 (free of charge)');
            this.spinnerService.hide();
          }
          if(this.topupResult.estel.response.resultcode==433 || this.topupResult.estel.response.resultcode=='433'){
            Swal.fire('Operation Failed', 'You have reached the Weekly amount maximum transaction limit.Kindly contact our Help Line on 119 (free of charge)');
            this.spinnerService.hide();
          }
          if(this.topupResult.estel.response.resultcode==434 || this.topupResult.estel.response.resultcode=='434'){
            Swal.fire('Operation Failed', 'You have reached the Daily maximum transaction limit.Kindly contact our Help Line on 119 (free of charge)');
            this.spinnerService.hide();
          }
          if(this.topupResult.estel.response.resultcode==435 || this.topupResult.estel.response.resultcode=='435'){
            Swal.fire('Operation Failed', 'You have reached the Monthly maximum transaction limit.Kindly contact our Help Line on 119 (free of charge)');
            this.spinnerService.hide();
          }
          if(this.topupResult.estel.response.resultcode==436 || this.topupResult.estel.response.resultcode=='436'){
            Swal.fire('Operation Failed', 'You have reached the Weekly maximum transaction limit.Kindly contact our Help Line on 119 (free of charge)');
            this.spinnerService.hide();
          }
          if(this.topupResult.estel.response.resultcode==437 || this.topupResult.estel.response.resultcode=='437'){
            Swal.fire('Operation Failed', 'Your transaction amount is above the maximum authorized limit');
            this.spinnerService.hide();
          }
          if(this.topupResult.estel.response.resultcode==4377 || this.topupResult.estel.response.resultcode=='4377'){
            Swal.fire('Operation Failed', 'You are transferring amount less than the minimum transfer limit. Kindly contact our Help Line on 119 (free of charge)');
            this.spinnerService.hide();
          }
        
        }
        else{Swal.fire('Operation Failed',this.topupResultDesc);
        this.spinnerService.hide();}
         }
 
  }
})
   
  
}
cancel(){
  this.screen1=true;
  this.screen3=false;
  this.screen2=false;
  this.clicked=false;
  this.modalReference.close();
}
 tConvert (time) {
  // Check correct time format and split into components
  time = time.toString ().match (/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

  if (time.length > 1) { // If time format correct
    time = time.slice (1);  // Remove full string match value
    time[5] = +time[0] < 12 ? 'AM' : 'PM'; // Set AM/PM
    time[0] = +time[0] % 12 || 12; // Adjust hours
  }
  return time.join (''); // return adjusted time or original string
}

}
