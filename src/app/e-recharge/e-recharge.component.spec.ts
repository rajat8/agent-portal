import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ERechargeComponent } from './e-recharge.component';

describe('ERechargeComponent', () => {
  let component: ERechargeComponent;
  let fixture: ComponentFixture<ERechargeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ERechargeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ERechargeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
