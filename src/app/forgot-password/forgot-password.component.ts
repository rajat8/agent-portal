import { Component, OnInit } from '@angular/core';
import { dashCaseToCamelCase } from '@angular/compiler/src/util';
import { sharedStylesheetJitUrl } from '@angular/compiler';
import{AppConfig} from '../app.config';
import {Router} from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import Swal from 'sweetalert2'
import {ApiService} from '../Services/api.service'
import { FormGroup, FormControl, Validators, FormBuilder ,FormsModule,ReactiveFormsModule } from '@angular/forms';
@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {
msisdn;otp;getotp=false;changepin=false;newpin;confirmnewpin;Name;pinType='IPIN';
  form: FormGroup;
  public reactiveForm: FormGroup = new FormGroup({
    recaptchaReactive: new FormControl(null, Validators.required)
});
  public inputTypes;
  public loginData;
  public LoginDestination;
  model: any = {};
  constructor(private formBuilder: FormBuilder,private router: Router,private http: HttpClient,private apiService:ApiService) {}
  public aFormGroup: FormGroup;
  pintypeChange(ev){
    this.pinType=ev.target.value;
  }
  ngOnInit(){
    this.inputTypes=AppConfig.Logininputs;
  this.LoginDestination=AppConfig.LoginDestination;
  this.aFormGroup = this.formBuilder.group({
    recaptcha: ['', Validators.required]
  });
  }
  help(){
    Swal.fire({html:'<h2 style="color:#d0001b;">! How to login</h2> <p style="float:left;">1.Enter Your username registered with alfa operator.</p><p style="float:left;">2.Enter Your Password.</p><p style="float:left;">3.Select your preffered language and click login.</p>'});
  }
  login(){
  }
  agentcode;
  validateOtp(){
    if(this.otp!=undefined && this.otp!=''){
    this.apiService.validateOtpApi(this.msisdn,this.otp).subscribe(result=>{ var b:any=result; var resultcode=b.estel.response.resultcode; if(resultcode!=undefined && (resultcode==1956 || resultcode=='1956') ){this.router.navigate(['']);localStorage.clear();}
    //   //  // console.log(result);
      this.validateOtpResult=result;
      this.validateOtpResultDesc=this.validateOtpResult.estel.response.resultdescription;
      if(this.validateOtpResultDesc=='Transaction Successful'){
        this.changepin=true;
        this.agentcode=this.validateOtpResult.estel.response.agentcode;
      }
      else{
        Swal.fire('Operation Failed', this.validateOtpResultDesc); 
      }
    })}
    else{
      Swal.fire('Operation Failed', 'Enter Otp'); 
    }
    
  }
  private req;
  getOtpResult;getOtpResultDesc;validateOtpResult;validateOtpResultDesc;responseOtp;changepinResult;changepinResultDesc;
  submit() {  
    if(this.pinType!=undefined){
      if(this.msisdn!=undefined && this.msisdn!='' && (this.msisdn.toString().length>4 && this.msisdn.toString().length<31)){
        this.apiService.getOtpApi(this.msisdn).subscribe(result=>{ var b:any=result; var resultcode=b.estel.response.resultcode; if(resultcode!=undefined && (resultcode==1956 || resultcode=='1956') ){this.router.navigate(['']);localStorage.clear();}
        //   //  // console.log(result);
          this.getOtpResult=result;
          this.getOtpResultDesc=this.getOtpResult.estel.response.resultdescription;
          if(this.getOtpResultDesc=='Transaction Successful'){
            this.getotp=true;
          }
          else{
            Swal.fire('Operation Failed', this.getOtpResultDesc); 
          }
        })
      }
      else{
        Swal.fire('Operation Failed', 'Enter Valid Username'); 
      }
    }
    else{
      Swal.fire('Operation Failed', 'Please select Pin type'); 
    }
           
  }

  changeMpin(){
    this.newpin=this.newpin.toString();
    
    let upperCaseCharacters = /[A-Z]+/g;
  let lowerCaseCharacters = /[a-z]+/g;
  let numberCharacters = /[0-9]+/g;
  let specialCharacters = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;
  if (upperCaseCharacters.test(this.newpin) === false || lowerCaseCharacters.test(this.newpin) === false || numberCharacters.test(this.newpin) === false || specialCharacters.test(this.newpin) === false || this.newpin.length<8) {
     
    Swal.fire('Operation Failed', 'Password must be at least 8 characters and must contain the following: numbers, lowercase letters, uppercase letters and special characters.');
  }
  else{
    if(this.newpin==this.confirmnewpin){

      this.apiService.forgotPwdApi(this.agentcode,this.newpin,this.pinType,this.msisdn).subscribe(result=>{ var b:any=result; var resultcode=b.estel.response.resultcode; if(resultcode!=undefined && (resultcode==1956 || resultcode=='1956') ){this.router.navigate(['']);localStorage.clear();}
      //   //  // console.log(result);
        this.changepinResult=result;
        this.changepinResultDesc=this.changepinResult.estel.response.resultdescription;
        if(this.changepinResultDesc=='Transaction Successful'){
          Swal.fire('Success', 'Please Login using your new Password'); 
          this.router.navigate(['']);
        }
        else{
          Swal.fire('Operation Failed', this.changepinResultDesc); 
        }
      })

    }
    else{
      Swal.fire('Operation Failed', 'New Password and Confirm Password do not match'); 
    }
  }
   
  }
  screenLoginShow(){
    this.router.navigate(['/login']);
  }
}
