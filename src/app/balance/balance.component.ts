import { Component, OnInit } from '@angular/core';
import{Validators,FormControl,FormGroup} from '@angular/forms';
import { NgbModal,NgbModalRef,NgbModule } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';
import{ApiService} from '../Services/api.service';
import {NgbTabsetConfig} from '@ng-bootstrap/ng-bootstrap';
import {formatDate} from '@angular/common';
import {Router} from '@angular/router';
@Component({
  selector: 'app-balance',
  templateUrl: './balance.component.html',
  styleUrls: ['./balance.component.scss']
})
export class BalanceComponent implements OnInit {

  constructor(private apiService:ApiService,config: NgbTabsetConfig,private router:Router) {    config.justify = 'center';
  config.type = 'pills'; }
  screen1;screen2;lengthError;voucher;
  private balanceForm;
  msisdn;agentName;balance;balanceResults;balanceTime;balanceFiscal;balanceTransId;
  hero;
  fromChangeDateEvent(ev){
    
    ev=this.balanceForm.date.year+'-'+this.balanceForm.date.month+'-'+this.balanceForm.date.day;
    
    this.balanceForm.date=new Date(ev);
    this.balanceForm.date=formatDate(this.balanceForm.date,'yyyy/MM/dd','en')
   // this.fDate=new Date(this.balanceForm.date);
    this.fDate=formatDate(this.balanceForm.date, 'dd/MM/yyyy', 'en');
    this.fromDate=this.balanceForm.date;
    this.balanceForm.date=ev;
      // console.log(ev);
  }
  fromdatemodel;fromDate='';fDate='';
   ngOnInit() {
     this.screen1=true;
     this.screen2=false;
     
 
     this.balanceForm = new FormGroup({
       'mpin': new FormControl( [
         Validators.required,
         Validators.minLength(10),
        
       ])
     });
   }
   balanceDate;
 screen2Show(mpin){
   var a=this.balanceForm.mpin;
 //   //  // console.log('MPin',a);
   if( mpin==undefined || this.balanceForm.mpin=='')
  { Swal.fire('Mpin Cant be Empty', 'Please provide your mpin') }
 
 if(mpin!=undefined && mpin!='')
 this.apiService.balanceApi(a).subscribe(result=>{ var b:any=result; var resultcode=b.estel.response.resultcode; if(resultcode!=undefined && (resultcode==1956 || resultcode=='1956') ){this.router.navigate(['']);localStorage.clear();}
   
 //   //  // console.log("balance",result);
   this.balanceResults=result;
   this.balanceResultDesc=this.balanceResults.estel.response.resultdescription;
   if(this.balanceResultDesc=='Transaction Successful'){
    this.balance=this.balanceResults.estel.response.walletbalance;
    this.agentName=this.balanceResults.estel.response.agentname;
    this.msisdn=this.balanceResults.estel.response.phoneno;
    this.balanceTime=this.balanceResults.estel.response.responsects;
    this.balanceFiscal=this.balanceResults.estel.response.retcommission;
    this.balanceTransId=this.balanceResults.estel.response.transid;
    this.balanceDate=this.balanceTime.split(" ");
    if(mpin!=undefined){
      if(a.length!=0){
        this.screen2=true;
        this.screen1=false;
        a='';
        this.balanceForm.mpin='';
       } 
     }
   }
   else{
    Swal.fire('Operation failed', this.balanceResultDesc);
   }
   
 });  
 }
balanceResultDesc;
 cancel(){
   this.screen1=true;
   
   this.screen2=false;
 }
 viewWalletResult;viewWalletResultDesc;agentfirstname;agentlastname;agentposname;agentactorid;orders;ordercount;
 viewWallet(ev){
  
   if((ev.path!=undefined && ev.path[0].innerText=='VIEW WALLET')|| ev.target.firstChild.data==' VIEW WALLET'){
     this.apiService.viewWalletApi().subscribe(result=>{ var b:any=result; var resultcode=b.estel.response.resultcode; if(resultcode!=undefined && (resultcode==1956 || resultcode=='1956') ){this.router.navigate(['']);localStorage.clear();}
     //   //  // console.log(result);
      this.viewWalletResult=result;
      this.viewWalletResultDesc=this.viewWalletResult.estel.response.resultdescription;
      if(this.viewWalletResultDesc=='Transaction Successful'){
      //   //  // console.log('ok');
this.agentfirstname=this.viewWalletResult.estel.response.firstname;
this.agentlastname=this.viewWalletResult.estel.response.lastname;
this.agentposname=this.viewWalletResult.estel.response.posname;
this.agentactorid=this.viewWalletResult.estel.response.agentcode;
this.ordercount=this.viewWalletResult.estel.response.orders.ordercount;
this.orders=this.viewWalletResult.estel.response.orders.order;
if(this.ordercount==1 || this.ordercount=='1'){
  if(this.orders.orderdate){  this.orders.orderdate=this.orders.orderdate.split(" ");}
  if(this.orders.expirydate){  this.orders.expirydate=this.orders.expirydate.split(" ");}

}
if(this.ordercount>1){
  for(var i=0;i<this.orders.length;i++){
    if(this.orders[i].orderdate){ this.orders[i].orderdate=this.orders[i].orderdate.split(" ");}
       if(this.orders[i].expirydate){ this.orders[i].expirydate=this.orders[i].expirydate.split(" ");}
   
 
  }
}
this.viewWalletResult.estel.response.initialvaluebalance=Math.round(this.viewWalletResult.estel.response.initialvaluebalance * 100) / 100;
this.viewWalletResult.estel.response.updatedvaluebalance=Math.round(this.viewWalletResult.estel.response.updatedvaluebalance * 100) / 100;
this.viewWalletResult.estel.response.recdvalbalance=Math.round(this.viewWalletResult.estel.response.recdvalbalance * 100) / 100;
this.viewWalletResult.estel.response.usedvalbalance=Math.round(this.viewWalletResult.estel.response.usedvalbalance * 100) / 100;
this.viewWalletResult.estel.response.initialfiscalbalance=Math.round(this.viewWalletResult.estel.response.initialfiscalbalance * 100) / 100;
this.viewWalletResult.estel.response.updatedfiscalbalance=Math.round(this.viewWalletResult.estel.response.updatedfiscalbalance * 100) / 100;
this.viewWalletResult.estel.response.recdfiscalbalance=Math.round(this.viewWalletResult.estel.response.recdfiscalbalance * 100) / 100;
this.viewWalletResult.estel.response.usedfiscalbalance=Math.round(this.viewWalletResult.estel.response.usedfiscalbalance * 100) / 100;
      }
      else{
        Swal.fire('Operation failed', this.viewWalletResultDesc);
      }
     })
   }
 }
 searchWalletResult;searchWalletResultDesc;
 searchWallet(){
 //   //  // console.log('search');
   this.apiService.searchWalletApi(this.balanceForm.msisdn,this.balanceForm.date).subscribe(result=>{ var b:any=result; var resultcode=b.estel.response.resultcode; if(resultcode!=undefined && (resultcode==1956 || resultcode=='1956') ){this.router.navigate(['']);localStorage.clear();}
  //   //  // console.log(result);
   this.searchWalletResult=result;
   this.searchWalletResultDesc=this.searchWalletResult.estel.response.resultdescription;
   if(this.searchWalletResultDesc=='Transaction Successful'){
    this.viewWalletResult.estel.response.initialvaluebalance=Math.round(this.searchWalletResult.estel.response.initialvaluebalance * 100) / 100;
    this.viewWalletResult.estel.response.updatedvaluebalance=Math.round(this.searchWalletResult.estel.response.updatedvaluebalance * 100) / 100;
    this.viewWalletResult.estel.response.recdvalbalance=Math.round(this.searchWalletResult.estel.response.recdvalbalance * 100) / 100;
    this.viewWalletResult.estel.response.usedvalbalance=Math.round(this.searchWalletResult.estel.response.usedvalbalance * 100) / 100;
    this.viewWalletResult.estel.response.initialfiscalbalance=Math.round(this.searchWalletResult.estel.response.initialfiscalbalance * 100) / 100;
    this.viewWalletResult.estel.response.updatedfiscalbalance=Math.round(this.searchWalletResult.estel.response.updatedfiscalbalance * 100) / 100;
    this.viewWalletResult.estel.response.recdfiscalbalance=Math.round(this.searchWalletResult.estel.response.recdfiscalbalance * 100) / 100;
    this.viewWalletResult.estel.response.usedfiscalbalance=Math.round(this.searchWalletResult.estel.response.usedfiscalbalance * 100) / 100;
    this.viewWalletResult.estel.response.expiredvaluebalance=Math.round(this.searchWalletResult.estel.response.expiredvaluebalance * 100) / 100;
    this.viewWalletResult.estel.response.expiredfiscalbalance=Math.round(this.searchWalletResult.estel.response.expiredfiscalbalance * 100) / 100;
this.agentfirstname=this.searchWalletResult.estel.response.firstname;
this.agentlastname=this.searchWalletResult.estel.response.lastname;
this.agentposname=this.searchWalletResult.estel.response.posname;
this.agentactorid=this.searchWalletResult.estel.response.agentcode;
      if(this.searchWalletResult.estel.response.orders.order){
        this.ordercount=this.searchWalletResult.estel.response.orders.ordercount;
this.orders=this.searchWalletResult.estel.response.orders.order;
if(this.ordercount==1 || this.ordercount=='1'){
  if(this.orders.orderdate){  this.orders.orderdate=this.orders.orderdate.split(" ");}
  if(this.orders.expirydate){  this.orders.expirydate=this.orders.expirydate.split(" ");}

}
if(this.ordercount>1){
  for(var i=0;i<this.orders.length;i++){
    if(this.orders[i].orderdate){ this.orders[i].orderdate=this.orders[i].orderdate.split(" ");}
       if(this.orders[i].expirydate){ this.orders[i].expirydate=this.orders[i].expirydate.split(" ");}
   
 
  }
}
      }
      else{
        Swal.fire('Operation failed', 'No order Found');
        this.orders=undefined;
      }
   }
   else{
     Swal.fire('Operation failed', this.searchWalletResultDesc);
   }
  })
 }

}
