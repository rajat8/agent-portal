import { Component, OnInit } from '@angular/core';
import{Validators,FormControl,FormGroup} from '@angular/forms';
import { NgbModal,NgbModalRef,NgbModule } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';
import {ApiService} from '../Services/api.service';
import {AuthGuard} from '../Services/auth.guard';
import {formatDate} from '@angular/common';
import {ActivatedRoute,Route,Router} from '@angular/router';
import{AppConfig} from '../app.config';
@Component({
  selector: 'app-edit-order',
  templateUrl: './edit-order.component.html',
  styleUrls: ['./edit-order.component.scss']
})
export class EditOrderComponent implements OnInit {ndateerror=false;
  needChangeDateEvent(ev){
    
    ev=this.needdatemodel.year+'-'+this.needdatemodel.month+'-'+this.needdatemodel.day;
    
    this.needdatemodel=new Date(ev);
    this.ndate=formatDate(this.needdatemodel, 'dd/MM/yyyy', 'en');
    this.needDate=this.needdatemodel;
    var ncheck=new Date(ev); var ocheck=new Date(this.editorder.orderdate); var echeck=new Date(this.editorder.expirydate);
    //   // console.log(ncheck,echeck,ocheck)
    //   // console.log((ncheck.getTime() > echeck.getTime()) || (ncheck.getTime() < ocheck.getTime()));
    if((ncheck.getTime() > echeck.getTime()) || (ncheck.getTime() < ocheck.getTime())) {
     
      Swal.fire('Operation Failed', 'Needed date must be between order date and expiry date.');
      this.ndateerror=true;
  }

  }
  needdatemodel;needDate='';tDate='';ndate;edate;odate;msisdnError=false;msisdnLength=AppConfig.msisdnLength;msisdnErrorText=AppConfig.msisdnError;
  checkmsisdnFieldLength(elem){
    let upperCaseCharacters = /[A-Z]+/g;
    let lowerCaseCharacters = /[a-z]+/g;
    let numberCharacters = /[0-9]+/g;
    let specialCharacters = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;
    if ((this.editorder.destination.toString().length != this.msisdnLength && (this.editorder.destination.toString().length!=0 || this.editorder.destination.toString().length!=undefined)) || numberCharacters.test(this.editorder.destination) === false || upperCaseCharacters.test(this.editorder.destination) === true || lowerCaseCharacters.test(this.editorder.destination) === true  || specialCharacters.test(this.editorder.destination) === true ) {
     this.msisdnError=true;
            
        }
        else{
          this.msisdnError=false;
        }
    }
  expChangeDateEvent(ev){
      // console.log(this.expdatemodel,ev);
    ev=this.expdatemodel.year+'-'+this.expdatemodel.month+'-'+this.expdatemodel.day;
      // console.log(this.expdatemodel,ev);
    this.expdatemodel=new Date(ev);
    this.edate=formatDate(this.expdatemodel, 'dd/MM/yyyy', 'en');
    this.expDate=this.expdatemodel;
      // console.log(this.expdatemodel,ev);
  }
  expdatemodel;expDate='';
  createOrder;public createOrderForm;screen3;screen4;config;editorder;

  constructor(private unlock:AuthGuard,private apiService:ApiService,private route:ActivatedRoute,private router:Router) {
  //   //  // console.log(this.router.getCurrentNavigation().extras.queryParams.order);
    this.order=this.router.getCurrentNavigation().extras.queryParams.order;
   }
items;itemPrice=0;itemName;itemResults;total;vat=11;selectedItem;selectedItemIncode;transactionType;agenttype;
onChangeItemEvent(ev) {
   // should print option1
  for(var i=0;i<this.items.length;i++){
    if(ev.target.value==this.items[i].incode){
      this.itemPrice=this.items[i].value;
      this.selectedItem=this.items[i].name;
      this.selectedItemIncode=this.items[i].incode;
    //   //  // console.log(this.itemPrice,this.items[i].name);
      this.editorder.item=this.items[i].incode;
    }
  }
}
onChangeTransTypeEvent(ev) {
  // should print option1
 
   if(ev.target.value==true){
     this.vat=11;
    this.createOrderForm.type=true; 
   }
   else{
     this.vat=0;
     this.createOrderForm.type=false; 
   }
 
}vath;totalh;
amount(ev){
if(this.editorder.wallettype==true){
  this.vath=this.editorder.quantity*this.vat/100;
  this.totalh=this.editorder.quantity+this.vath;
  this.editorder.quantity=Math.round(this.editorder.quantity * 100) / 100;
  this.vath=Math.round(this.vath * 100) / 100;
  this.totalh=Math.round(this.totalh * 100) / 100;
}
  else{
    this.vath=0;
    this.totalh=this.editorder.quantity+this.vath;
    this.editorder.quantity=Math.round(this.editorder.quantity * 100) / 100;
  
    this.totalh=Math.round(this.totalh * 100) / 100;
  }
 
}notifiers;
  ngOnInit() {

    this.vat=this.unlock.decryptData((localStorage.getItem('Vat')));
    this.agenttype=this.unlock.decryptData(localStorage.getItem('agenttype'));
    this.editorder=this.order;
   this.apiService.getItemApi().subscribe(result=>{ var b:any=result; var resultcode=b.estel.response.resultcode; if(resultcode!=undefined && (resultcode==1956 || resultcode=='1956') ){this.router.navigate(['']);localStorage.clear();}
    
this.itemResults=result;
this.items=this.itemResults.estel.response.records.record;
this.itemcount=this.itemResults.estel.response.recordcount;
this.notifiers=this.itemResults.estel.response.srcnotify;

this.notifiers=this.notifiers.split(",");
for(var i=0;i<this.notifiers.length;i++){
  
 
  this.notifiers[i]=this.notifiers[i].split("0");
  for(var j=0;j<this.notifiers[i].length;j++){
    this.notifiers[i][1]=this.notifiers[i][1].replace(/\s/g, "");
    this.notifiers[i][j]=this.notifiers[i][j].replace('(', "");
    this.notifiers[i][j]=this.notifiers[i][j].replace(')', "");
  }
}
if(this.itemcount>1){
  this.itemPrice=this.items[0].value;
this.selectedItem=this.items[0].name;
this.selectedItemIncode=this.items[0].incode;
}
if(this.itemcount==1){
  this.itemPrice=this.items.value;
this.selectedItem=this.items.name;
this.selectedItemIncode=this.items.incode;
}
   })
    this.createOrder=true;
    this.config=true;
    this.screen3=false;
    this.screen4=false;
    this.createOrderForm = new FormGroup({
      'orderType': new FormControl( [
        Validators.required,
       
      ]),
      'orderDate': new FormControl( [
        Validators.required,
       
      ]),
    
      'quantity': new FormControl( [
        Validators.required,
       
      ]),
      'price': new FormControl( [
        Validators.required,
       
      ]),  'vat': new FormControl( [
        Validators.required,
       
      ]),  'total': new FormControl( [
        Validators.required,
       
      ]),  'expiryDate': new FormControl( [
        Validators.required,
       
      ]),
      'notifier': new FormControl( [
        Validators.required,
       
      ]),  'neededDate': new FormControl( [
        Validators.required,
       
      ]),
    });
    this.vath=this.editorder.vat;this.totalh=this.editorder.totalamount;
   if(this.editorder.date!=undefined){
    var dateParts = this.editorder.date.split("/");

 
    this.editorder.date = new Date(+dateParts[2], dateParts[1] - 1, +dateParts[0]);
  
    this.editorder.date=formatDate(this.editorder.date, 'yyyy-MM-dd', 'en')
   }
   if(this.editorder.neededdate!=undefined){
     this.ndate=this.editorder.neededdate;
    var dateParts = this.editorder.neededdate.split("/");

 
    this.editorder.neededdate = new Date(+dateParts[2], dateParts[1] - 1, +dateParts[0]);
  
    this.editorder.neededdate=formatDate(this.editorder.neededdate, 'yyyy-MM-dd', 'en')
   }
   if(this.editorder.orderdate!=undefined){
     this.odate=this.editorder.orderdate;
    var dateParts = this.editorder.orderdate.split("/");

 
    this.editorder.orderdate = new Date(+dateParts[2], dateParts[1] - 1, +dateParts[0]);

    this.editorder.orderdate=formatDate(this.editorder.orderdate, 'yyyy-MM-dd', 'en')
   }
   if(this.editorder.expirydate!=undefined){
     this.edate=this.editorder.expirydate;
    var dateParts =this.editorder.expirydate.split("/");

 
    this.editorder.expirydate = new Date(+dateParts[2], dateParts[1] - 1, +dateParts[0]);
    
    this.editorder.expirydate=formatDate(this.editorder.expirydate, 'yyyy-MM-dd', 'en')
   }
  
//   //  // console.log("order",this.order);
  }
 order;
  mpinEdit(){
this.createOrder=true;
this.config=false;
  }
orderDate;expiryDate;neededDate;
  screen3Show(){
    var a=this.createOrderForm.orderDate;
    var b=this.createOrderForm.quantity;
    var c=this.createOrderForm.price;
    var d=this.vat;
    var e=this.createOrderForm.expiryDate;
    var f=this.createOrderForm.notifier;
    var g=this.createOrderForm.neededDate;
    var todaysDate = formatDate(new Date(), 'dd/MM/yyyy', 'en')
  //   //  // console.log("todays date",todaysDate);
    if(a!=undefined && e!=undefined && g!=undefined){
      this.orderDate=formatDate(a, 'dd/MM/yyyy', 'en')
      this.expiryDate=formatDate(e, 'dd/MM/yyyy', 'en')
      this.neededDate=formatDate(g, 'dd/MM/yyyy', 'en')
      a=formatDate(a, 'dd/MM/yyyy', 'en')
      e=formatDate(e, 'dd/MM/yyyy', 'en')
      g=formatDate(g, 'dd/MM/yyyy', 'en')

      if(a < todaysDate ) {
        Swal.fire('Operation Failed', 'Order date cannot be before current date');
    }
    else{
      if(a > e) {
        Swal.fire('Operation Failed', 'Expiry cannot be before order date');
    }
    else{
      if(g > e || g < a) {
        Swal.fire('Operation Failed', 'Needed date must be between order date and expiry date.');
    }
    else{
      if(a==undefined || b==undefined || d==undefined || e==undefined || f==undefined || g==undefined || a=='' || b=='' || e=="" || f=="" || g=="")
      Swal.fire('Operation Failed', 'All fields are required');
      else{
       this.screen3=true;
      
       this.screen4=false;  this.createOrder=false;
       this.config=false;
      }
    }
  
    }
    }
    
    }
    else{
      Swal.fire('Operation Failed', 'All fields are required');
    }

   
    
  }
  cancel(){ 
    this.screen3=false;
  this.createOrder=true;
    this.screen4=false;
    this.config=true;
  }
  close(){ 
 this.router.navigate(['/search-order'])
  }
editOrderResponse;
  updateOrder(){
    this.msisdnError=true;

    for(var i=0;i<this.notifiers.length;i++){
      if(this.notifiers[i][1]==this.editorder.destination){
        this.msisdnError=false;
      }
    }
    if(this.msisdnError==false && this.ndateerror==false){
  //   //  // console.log(this.editorder);
    var editOrderResults;var editOrderResultsDesc;
    if(this.editorder.orderdate){
      var orderdate=formatDate(this.editorder.orderdate, 'dd/MM/yyyy', 'en')
    }
    if(this.expDate){
      var expirydate=formatDate(this.expDate, 'dd/MM/yyyy', 'en')
    }
    if(this.needDate){
      var neededdate=formatDate(this.needDate, 'dd/MM/yyyy', 'en')
    }
      // console.log(this.needDate)
    var total=this.totalh;
    total=Math.round(total * 100) / 100;
    this.apiService.updateOrderApi(this.editorder.orderid,this.itemPrice*this.editorder.quantity,this.selectedItemIncode,this.editorder.quantity,this.vath,total,expirydate,neededdate,this.editorder.destination,this.editorder.wallettype).subscribe(result=>{ var b:any=result; var resultcode=b.estel.response.resultcode; if(resultcode!=undefined && (resultcode==1956 || resultcode=='1956') ){this.router.navigate(['']);localStorage.clear();}
    
      editOrderResults=result;
      editOrderResultsDesc=editOrderResults.estel.response.resultdescription;
      if(editOrderResultsDesc=="Transaction Successful"){
this.editOrderResponse=editOrderResults.estel.response;

//  // console.log("update order",this.editOrderResponse);
        
   
      this.screen4=true;
      this.config=false;
       this.screen3=false;
       this.createOrder=false;
    
   
  
      }
      else{
        Swal.fire('Operation Failed',editOrderResultsDesc);
      }
    })
  }
  else{
    if(this.msisdnError==true) 
    { Swal.fire('Operation failed', 'Notifier Number is not valid') }
    if(this.ndateerror==true){
     Swal.fire('Operation Failed', 'Needed date must be between order date and expiry date.');
    }
  }
  }
  itemcount;
  screen4Show(){
    var walletType;
    if(this.createOrderForm.type=='Value'){
      walletType=true;
    }
    else{
      walletType=false;
    }
    var createOrderResults;
    var createOrderResultsDesc;
    this.total=(this.itemPrice*this.createOrderForm.quantity)+(this.itemPrice*this.createOrderForm.quantity)*this.vat/100
  //   this.apiService.createOrderApi(this.itemPrice,walletType,this.selectedItemIncode,this.createOrderForm.quantity,this.vat,this.total,this.orderDate,this.expiryDate,this.neededDate).subscribe(result=>{ var b:any=result; var resultcode=b.estel.response.resultcode; if(resultcode!=undefined && (resultcode==1956 || resultcode=='1956') ){this.router.navigate(['']);localStorage.clear();}
  //   //   //  // console.log("create order",result);
  //     createOrderResults=result;
  //     createOrderResultsDesc=createOrderResults.estel.response.resultdescription;
  //     if(createOrderResultsDesc=="Transaction Successful"){

  //       this.screen4=true;
  //  this.config=false;
  //   this.screen3=false;
  //   this.createOrder=false;
  //     }
  //     else{
  //       Swal.fire('Operation Failed',createOrderResultsDesc);
  //     }
  //   })
   

}
}
