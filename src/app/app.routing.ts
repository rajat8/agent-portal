import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule  } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';
import{AuthGuard} from './Services/auth.guard'
import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { LoginComponent } from './login/login.component';
import { ForcefulPinChangeComponent } from './forceful-pin-change/forceful-pin-change.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
const routes: Routes =[
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full',
  },
  { path: 'forceful-pin-change',    
    component: ForcefulPinChangeComponent }, 
    { path: 'forgot-password',       
     component: ForgotPasswordComponent }, 
  {
    path: 'login',
    component: LoginComponent,
   }, {
    path: '',
    component: AdminLayoutComponent,
    canActivate: [AuthGuard],
    children: [
        {
      path: '',
      loadChildren: './layouts/admin-layout/admin-layout.module#AdminLayoutModule'
  }]},
  {
    path: '**',
    redirectTo: 'login'
  }
];

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes)
  ],
  exports: [
  ],
})
export class AppRoutingModule { }
