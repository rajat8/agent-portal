import { Component, OnInit } from '@angular/core';
import{Validators,FormControl,FormGroup} from '@angular/forms';
import { NgbModal,NgbModalRef,NgbModule } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';
import{ApiService} from '../Services/api.service';
import{AuthGuard} from '../Services/auth.guard';
import {Router} from '@angular/router';
@Component({
  selector: 'app-change-language',
  templateUrl: './change-language.component.html',
  styleUrls: ['./change-language.component.scss']
})
export class ChangeLanguageComponent implements OnInit {

  mpinScreen;languageScreen;public configurationForm;screen3;screen4;config;
  constructor(private unlock:AuthGuard,private apiService:ApiService,private router:Router) { }
language;
  ngOnInit() {
    this.language=this.unlock.decryptData((localStorage.getItem('language')));
      // console.log(this.language);
    this.languageScreen=true;
    this.mpinScreen=false;
    this.config=true;
    this.screen3=false;
    this.screen4=false;
    this.configurationForm = new FormGroup({
      'mpin': new FormControl( [
        Validators.required,
        Validators.minLength(10),
       
      ])
    });
  }
  mpinEdit(){
this.mpinScreen=true;
this.config=false;
  }
  languageEdit(){
    this.languageScreen=true;
    this.config=false;
  }
  changeLanguageResult; changeLanguageResultDesc;
  screen3Show(){
    var a=this.configurationForm.mpin;
    
    var type=this.configurationForm.lang;
   if(a==undefined)
    Swal.fire('Operation Failed', 'Mpin is required');
    else{
      this.apiService.changeLanguageApi(a,type).subscribe(result=>{ var b:any=result; var resultcode=b.estel.response.resultcode; if(resultcode!=undefined && (resultcode==1956 || resultcode=='1956') ){this.router.navigate(['']);localStorage.clear();}
        this.changeLanguageResult=result;
        this.changeLanguageResultDesc=this.changeLanguageResult.estel.response.resultdescription;
        if(this.changeLanguageResultDesc=='Transaction Successful'){
          this.screen4=true;
          this.languageScreen=false;
           this.screen3=false;
           this.mpinScreen=false;
        }
        else{
          Swal.fire('Operation Failed',this.changeLanguageResultDesc,'error');
        }
        
      })
    
    }
    if(a=='')
    Swal.fire('Operation Failed', 'Mpin is required');
  }
  cancel(){
   this.languageScreen=true;
    this.screen3=false;
  this.mpinScreen=false;
    this.screen4=false;
    this.config=true;
    this.configurationForm.mpin='';
  }
  screen4Show(){
   this.screen4=true;
   this.languageScreen=false;
    this.screen3=false;
    this.mpinScreen=false;
  }

}
