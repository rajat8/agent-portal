import { Component, OnInit } from '@angular/core';
import{ApiService} from '../Services/api.service';
import{AuthGuard} from '../Services/auth.guard';
import{Validators,FormControl,FormGroup} from '@angular/forms';
import Swal from 'sweetalert2';
import { NgbModal,NgbModalRef,NgbModalConfig,NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {Router} from '@angular/router';
@Component({
  selector: 'app-agent-hierarchy',
  templateUrl: './agent-hierarchy.component.html',
  styleUrls: ['./agent-hierarchy.component.scss']
})
export class AgentHierarchyComponent implements OnInit {

  constructor(private unlock:AuthGuard,private apiService:ApiService, private modalService: NgbModal,private router:Router) { }
  screen1;screen2;lengthError;voucher;
  private historyForm;
  msisdn;agentName;history;historyResults;agenttype;fiscalStamp=0.17;respVat;
  hero;
transactionHistory;modalRecord;
openModal(content,record){
  this.modalService.open(content,  { windowClass : "myCustomModalClass"});
  this.modalRecord=record;
//   //  // console.log("modal record",record);
  this.respVat=this.modalRecord.amount/11;
  this.respVat=Math.round(this.respVat * 100) / 100;
}
  ngOnInit() {
    
    this.agenttype=this.unlock.decryptData(localStorage.getItem('agenttype'));
    this.screen1=true;
     this.screen2=false;
     
     this.historyForm = new FormGroup({
       'mpin': new FormControl( [
         Validators.required,
         Validators.minLength(10),
        
       ])
     });
    // this.apiService.historyApi().subscribe(result=>{ var b:any=result; var resultcode=b.estel.response.resultcode; if(resultcode!=undefined && (resultcode==1956 || resultcode=='1956') ){this.router.navigate(['']);localStorage.clear();}
    // //   //  // console.log("history",result);
    //   this.transactionHistory=result;
    //   this.transactionHistory=this.transactionHistory.estel.response.records.record;
    // //   //  // console.log("records",this.transactionHistory);
    // });
  }
  recordCount;
  screen2Show(mpin){
    var a=this.historyForm.mpin;
  //   //  // console.log('MPin',a);
    if( mpin==undefined)
   { Swal.fire('Mpin Cant be Empty', 'Please provide your mpin') }
  
  if(mpin!=undefined && mpin!='')
  this.apiService.agentHierarchyApi(mpin).subscribe(result=>{ var b:any=result; var resultcode=b.estel.response.resultcode; if(resultcode!=undefined && (resultcode==1956 || resultcode=='1956') ){this.router.navigate(['']);localStorage.clear();}
    this.historyResults=result;
 //   //  // console.log(this.historyResults.estel.response.resultdescription);
    this.historyResultDesc=this.historyResults.estel.response.resultdescription;
    if(this.historyResultDesc=='Transaction Successful'){
      this.recordCount=this.historyResults.estel.response.recordcount;
      if(this.recordCount>0){
        this.history=this.historyResults.estel.response.wallethistory;
        this.agentName=this.historyResults.estel.response.agentname;
        this.msisdn=this.historyResults.estel.response.phoneno;
      //   //  // console.log("history",result);
        this.transactionHistory=result;
        this.transactionHistory=this.transactionHistory.estel.response.records.record;
        
      //   //  // console.log("records",this.transactionHistory); 
      //   //  // console.log("history",result);
        this.historyResults=result;
   
        if(mpin!=undefined){
          if(a.length!=0){
            this.screen2=true;
            this.screen1=false;
            this.historyForm.mpin='';
            mpin='';
           } 
         }
      }
      else{Swal.fire('Operation failed', 'No record Found');}
   
    }
    else{
     Swal.fire('Operation failed', this.historyResultDesc);
    }
    
  });  
  }
 historyResultDesc;
  cancel(){
    this.screen1=true;
    
    this.screen2=false;
  }
  print(): void {
    let printContents, popupWin;
    printContents = document.getElementById('print-section').innerHTML;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>Print tab</title>
          <style>
        @media print {
          html, body {
            width: 7.2cm;
            height: 15cm;
          }
          prints{
              width: 7.2cm;
              height: 15cm;
          }
        
      }
        //........Customized style.......
        </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
    );
    popupWin.document.close();
  }

}
