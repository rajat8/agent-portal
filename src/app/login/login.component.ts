import { Component, OnInit } from '@angular/core';
import { dashCaseToCamelCase } from '@angular/compiler/src/util';
import { sharedStylesheetJitUrl } from '@angular/compiler';
import{AppConfig} from '../app.config';
import {Router} from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import Swal from 'sweetalert2'
import * as CryptoJS from 'crypto-js';
import {AuthenticationService} from '../Services/authentication.service'
import {AuthGuard } from '../Services/auth.guard'
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { FormGroup, FormControl, Validators, FormBuilder ,FormsModule,ReactiveFormsModule } from '@angular/forms';
const httpOptions = {
  headers: new HttpHeaders({
    "Content-Type":"application/json",
 
   "auth.security.enable":"true",
   "authorization":"2DEF34A67B7C82850E2F531A0168375E4CF5977E6C1984553ECBA9869E379BA1"
  })
};
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  encryptData(dataToEncrypt: string) {
 
    
    var key = CryptoJS.enc.Utf8.parse(window.atob("MTIzNDU2Nzg5MDEyMzQ1Ng=="));
    // var key = "1234567890123456";
    var encrypted = CryptoJS.AES.encrypt(dataToEncrypt, key
    , {
      mode: CryptoJS.mode.ECB,
      padding: CryptoJS.pad.Pkcs7
    }
    );
    
    var enc=CryptoJS.enc.Hex.stringify(encrypted.ciphertext);

    return enc.toUpperCase();
  }
  form: FormGroup;
  public reactiveForm: FormGroup = new FormGroup({
    recaptchaReactive: new FormControl(null, Validators.required)
});
  public inputTypes;
  public loginData;
  public LoginDestination;
  model: any = {};
  constructor(private lock:AuthGuard,private formBuilder: FormBuilder,private router: Router,private http: HttpClient,private authService:AuthenticationService,private spinnerService: Ng4LoadingSpinnerService) {}
  public aFormGroup: FormGroup;
  cities = [
    { id: 1, name: 'Vilnius' },
    { id: 2, name: 'Kaunas' },
    { id: 3, name: 'Pavilnys', disabled: true }
];
selectedCityId: number = null;
  ngOnInit(){
    this.inputTypes=AppConfig.Logininputs;
  this.LoginDestination=AppConfig.LoginDestination;
  this.aFormGroup = this.formBuilder.group({
    recaptcha: ['', Validators.required]
  });
  }
  help(){
    Swal.fire({html:'<h2 style="color:#d0001b;">! How to login</h2> <p >1.Enter Your Mobile Number registered with alfa operator.</p><p >2.Enter Your Password.</p>'});
  }
  login(){
  }
  clicked = false;
  private req;
  submit() {  
    this.clicked=true;
    this.spinnerService.show();
  //   //  // console.log('Data submitted: ', this.model);
    // this.loginData=(this.model);
    // localStorage.setItem("username", (this.model.username)); 
  //   localStorage.setItem("password", (this.model.password)); 
  //   localStorage['Ckey']=( (this.model.username));  
  //  this.req= {
  //   "header":{"requesttype":"LOGIN"}, 
  //   request:{agentcode:"986232477609",pin:"3D072E01C535B6B0A8537A11AD864B94AA5B8A155BB160389EC2591366EFF903",vendorcode:"ALFA",comments:[],clienttype:"ANDROID",version:"2.5",sign:"4n5MPDjukoJqotK5YbL5ExO2gvw"}
  // }
  //   //   //  // console.log("request",this.req);
  //   this.http.post('/services/alfaRestService',this.req,httpOptions).subscribe(result => {
  //   //   //  // console.log("Response",result);});
  var username=this.model.username;
  var password=this.model.password;
  if(username!=undefined && password !=undefined && username!="" && password !='') { 
      this.authService.login(this.model).subscribe(result=>{
        var menu;
          //   //  // console.log("Response",result);
             this.check=result;
              this.loginResults=result;
             menu=this.check.estel.response.records.record;
             var loginName=this.check.estel.response.agentname;
             var lastname=this.check.estel.response.lastname;
            this.check=this.check.estel.response.resultdescription;
          
            var mobile=this.loginResults.estel.response.source;
            var sessionid=this.loginResults.estel.response.sessionid;
            if(this.check=='Transaction Successful'){
              var orderexpiry=this.loginResults.estel.response.orderexpiry;
             
              if(this.loginResults.estel.response.status=='CREATED'){
                
 localStorage.setItem('username', this.lock.encryptData(username));
                localStorage.setItem('currentUser',this.lock.encryptData(mobile) );
                this.spinnerService.hide();
                this.router.navigate(['/forceful-pin-change']);
                this.authService.lout();
              }
              else{
               if(this.loginResults.estel.response.taxes){
                 var taxes=this.loginResults.estel.response.taxes.tax;
                 for(var i=0;i<taxes.length;i++){
                    if(taxes[i].taxcode=='VAT'){
                      localStorage.setItem('Vat',this.lock.encryptData((taxes[i].taxamount)) );
                    }
                    if(taxes[i].taxcode=='FISCALSTAMP'){
                      localStorage.setItem('Fiscal',this.lock.encryptData((taxes[i].taxamount)) );
                    }
                 }
               }
               if(this.loginResults.estel.response.languages){
                localStorage.setItem('language',this.lock.encryptData((this.loginResults.estel.response.languages))  );
               }
               var quantity=this.loginResults.estel.response.quantity;
                localStorage.setItem('currentUser',this.lock.encryptData(mobile));
                localStorage.setItem('agentname', this.lock.encryptData(loginName));
                localStorage.setItem('lastname',this.lock.encryptData(lastname) );
                localStorage.setItem('username',this.lock.encryptData(username) );
                localStorage.setItem('orderexpiry',this.lock.encryptData(orderexpiry) );
                localStorage.setItem('quantity',this.lock.encryptData(quantity) );
                localStorage.setItem('sessionid',this.lock.encryptData(sessionid.toString()) );
                this.router.navigate([AppConfig.LoginDestination]);
                if(this.loginResults.estel.response.categoryid==1){
                  localStorage.setItem('agenttype', this.lock.encryptData('Direct'));
                }
                else{
                  localStorage.setItem('agenttype', this.lock.encryptData('Indirect'));
                }
            localStorage.setItem('menu', this.lock.encryptData(menu));
          //   //  // console.log("menu",(localStorage.getItem('menu') || "[]"));
            this.spinnerService.hide(); }
            
            }
            else{
             this.clicked=false;
              if(this.check=='Subscriber/Agent Not Found'){
                Swal.fire('Operation failed', 'Login name or password wrong'); 
                this.spinnerService.hide();
              }else{
                if(this.check=='Invalid PIN'){
                  Swal.fire('Operation failed', 'Login name or password wrong'); 
                  this.spinnerService.hide();}
                else{
                  if(this.check=='Subscriber/Agent Blocked due to Wrong Attempts' || this.check=='Customer Blocked'){
                    Swal.fire('Operation failed', 'Your account is blocked kindly contact our helpline number 119(Free of charge)'); 
                    this.spinnerService.hide();}
                  else{
                    if(this.check=='Locked Subscriber/Agent'){
                      Swal.fire('Operation failed',' Your account is locked, kindly contact our Help Line on 119 (free of charge)'); 
                      this.spinnerService.hide();
                    }
                    else{
      
                      if(this.check=='Closed Subscriber/Agent'){
                        Swal.fire('Operation failed',' Your account is Closed, kindly contact our Help Line on 119 (free of charge)'); 
                        this.spinnerService.hide();
                      }
                      else{
                        
                        if(this.check=='Suspend Subscriber/Agent'){
                          Swal.fire('Operation failed',' Your account is Suspended,Kindly follow the process of Forget password in order to reactivate your account'); 
                          this.spinnerService.hide();
                        }
                        else{
                          if(this.loginResults.estel.response.resultcode==61){
                            localStorage.setItem('username', this.lock.encryptData(username));
                            localStorage.setItem('currentUser', this.lock.encryptData(mobile));
                            this.spinnerService.hide();
                            this.router.navigate(['/forceful-pin-change']);
                          }
                          else{
                            Swal.fire('Operation Failed', this.check); 
                            this.spinnerService.hide();
                          }
                        }
      
                        
                      }
                      
                    }
                    
                  }
                 }
        }      }
             
      });   
    }  
      
    else{
      this.clicked=false;
        Swal.fire('Operation Failed', 'Kindly Enter Login Name and Password'); 
        this.spinnerService.hide();
    }       
  }
  private  loginResults;
  check;
logOutRequest
  lout(){
    this.logOutRequest={
      "header": {
         "requesttype": "LOGOUT"
      },
      "request": {
        //96321014,
         "agentcode":this.encryptData("87643684"),
          "vendorcode": "ALFA",
         "comments": [],
         "clienttype": "SELFCARE",
         "version": "2.5",
         "sign": "4n5MPDjukoJqotK5YbL5ExO2gvw",
         "sessionid":this.encryptData("6919687549"),
      }
   }

      this.http.post('/services/alfaRestService',this.logOutRequest,httpOptions).subscribe(result=>
        {
          localStorage.removeItem('currentUser');
          localStorage.removeItem('agenttype');
          this.router.navigate(['']);
         
          localStorage.clear();
        })
   
   
  }

}
