import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy, ViewEncapsulation, OnInit } from '@angular/core';
import{ApiService} from '../Services/api.service';
import{Validators,FormControl,FormGroup} from '@angular/forms';
import Swal from 'sweetalert2';
import { NgbModal,NgbModalRef,NgbModalConfig,NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {Router} from '@angular/router';
import {formatDate} from '@angular/common';
import{AdminLayoutComponent} from '../layouts/admin-layout/admin-layout.component'
import{AuthGuard} from '../Services/auth.guard';
@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss']
})
export class HistoryComponent implements OnInit {
  constructor(private unlock:AuthGuard,private ad:AdminLayoutComponent,private apiService:ApiService, private modalService: NgbModal,private router:Router) { }
  screen1;screen2;lengthError;voucher; dateModel;dateModel2;
  private historyForm;
  msisdn;agentName;history;historyResults;agenttype;fiscalStamp=0.17;respVat;
  hero;firstname;lastname;
transactionHistory;modalRecord;
openModal(content,record){
  this.modalService.open(content,  { windowClass : "myCustomModalClass"});
  this.modalRecord=record;
//   //  // console.log("modal record",record);
if(this.modalRecord.voucherrecords){
  if(this.modalRecord.voucherrecords.vouchercount==1){
    this.modalRecord.voucherrecords.voucherrecord.expirydate=this.modalRecord.voucherrecords.voucherrecord.expirydate.split(" ");
                    this.modalRecord.voucherrecords.voucherrecord.expirydate=this.modalRecord.voucherrecords.voucherrecord.expirydate[0];
                    var stringDate1=this.modalRecord.voucherrecords.voucherrecord.expirydate;
    var splitDate1 = stringDate1.split('-');
    if(stringDate1.split('-')!=null && stringDate1.split('-').length==3){
      
    var year1  = splitDate1[0];
    var month1 = splitDate1[1];
    var day1 = splitDate1[2];
    this.modalRecord.voucherrecords.voucherrecord.expirydate =day1+"/"+ month1+"/"+year1;
    }
  }
  else{
    for(var i=0;i<this.modalRecord.voucherrecords.voucherrecord.length;i++){
                          this.modalRecord.voucherrecords.voucherrecord[i].expirydate=this.modalRecord.voucherrecords.voucherrecord[i].expirydate.split(" ");
                          this.modalRecord.voucherrecords.voucherrecord[i].expirydate=this.modalRecord.voucherrecords.voucherrecord[i].expirydate[0];
                          var stringDate1=this.modalRecord.voucherrecords.voucherrecord[i].expirydate;
          var splitDate1 = stringDate1.split('-');
          if(stringDate1.split('-')!=null && stringDate1.split('-').length==3){
            
            var year1  = splitDate1[0];
            var month1 = splitDate1[1];
            var day1 = splitDate1[2];
            this.modalRecord.voucherrecords.voucherrecord[i].expirydate =day1+"/"+ month1+"/"+year1;
        
          }
  
                        }
  }
}
  this.respVat=this.modalRecord.amount/11;
  this.respVat=Math.round(this.respVat * 100) / 100;
}screen0;
scrollToTop(el) {
  //  const elemMainPanel = <HTMLElement>document.querySelector('.main-panel');
  //  const ps = new PerfectScrollbar(elemMainPanel);
  //     ps.update();
  //   el.scrollTop = 0;
  //   elemMainPanel.scrollTop = 0;
  this.ad.scrollToTop();
  }Vat;
  ngOnInit() {
    this.Vat=this.unlock.decryptData((localStorage.getItem('Vat')));
    this.agenttype=this.unlock.decryptData(localStorage.getItem('agenttype'));
    this.firstname=this.unlock.decryptData(localStorage.getItem('agentname'));
    this.lastname=this.unlock.decryptData(localStorage.getItem('lastname'));
    this.screen1=false;
     this.screen2=false;
     this.screen0=true;
     this.historyForm = new FormGroup({
       'mpin': new FormControl( [
         Validators.required,
         Validators.minLength(10),
         
        
       ]),
       'status': new FormControl( [
        Validators.required,
        Validators.minLength(10),
        
       
      ])
     });
    // this.apiService.historyApi().subscribe(result=>{ var b:any=result; var resultcode=b.estel.response.resultcode; if(resultcode!=undefined && (resultcode==1956 || resultcode=='1956') ){this.router.navigate(['']);localStorage.clear();}
    // //   //  // console.log("history",result);
    //   this.transactionHistory=result;
    //   this.transactionHistory=this.transactionHistory.estel.response.records.record;
    // //   //  // console.log("records",this.transactionHistory);
    // });
 
  }
  recordCount;f1=false;f2=false;f3=false;refcode;
  screen2Show(mpin){
    var a=mpin;
  //   //  // console.log('MPin',a);
    if( mpin==undefined || mpin=='')
   { Swal.fire('Operation failed', 'Please provide your mpin') }
   if(this.historyForm.dateto && this.historyForm.datefrom){
    var b=formatDate(this.historyForm.dateto,'dd/MM/yyyy','en');
    
    var k=formatDate(this.historyForm.dateto,'mediumTime','en')
    var dateto=b+' '+k;

    var d=formatDate(this.historyForm.datefrom,'dd/MM/yyyy','en');
    
    var f=formatDate(this.historyForm.datefrom,'mediumTime','en')
    var datefrom=d+' '+f;
    //   // console.log('2',dateto,datefrom);
   }
   else{
     var dateto='';var datefrom='';
    //    // console.log('3',this.historyForm.dateto,this.historyForm.datefrom);
   }
//    // console.log('1',this.historyForm.dateto,this.historyForm.datefrom);
//console.log(this.f1,this.f2,this.f3)
  if(mpin!=undefined && mpin!=''){
    if(this.f1==true){
      this.apiService.transf1Api(mpin,this.historyForm.username,this.historyForm.state,this.historyForm.operationtype,datefrom,dateto).subscribe(result=>{ var b:any=result; var resultcode=b.estel.response.resultcode; if(resultcode!=undefined && (resultcode==1956 || resultcode=='1956') ){this.router.navigate(['']);localStorage.clear();}
        this.historyResults=result;
     //   //  // console.log(this.historyResults.estel.response.resultdescription);
        this.historyResultDesc=this.historyResults.estel.response.resultdescription;
        if(this.historyResultDesc=='Transaction Successful'){
          this.calculateIndexes();
          this.recordCount=this.historyResults.estel.response.recordcount;
          if(this.recordCount>0){
            this.history=this.historyResults.estel.response.wallethistory;
            this.agentName=this.historyResults.estel.response.agentname;
            this.msisdn=this.historyResults.estel.response.phoneno;
          //   //  // console.log("history",result);
            this.transactionHistory=result;
            this.transactionHistory=this.transactionHistory.estel.response.records.record;
            
            if(this.recordCount==1 || this.recordCount=='1'){
              this.transactionHistory['vouchertotal']=this.transactionHistory.amountpre * this.transactionHistory.quantity;
              this.transactionHistory.vouchertotal=Math.round(this.transactionHistory.vouchertotal * 100) / 100
               this.transactionHistory['totalamountwords']=Math.round((this.transactionHistory.amountpre+this.transactionHistory.sourcevat+this.transactionHistory.fiscalvat)* 100) / 100
              // console.log(this.transactionHistory.sourcevat)
          
                    
                  this.transactionHistory.totalamountwords=this.totalwords(this.transactionHistory.totalamountwords)
              this.transactionHistory.requestdate=this.transactionHistory.requestdate.split(" ");
              this.transactionHistory.requestdate[1]=this.tConvert(this.transactionHistory.requestdate[1]);
                    var stringDate1=this.transactionHistory.requestdate[0];
  var splitDate1 = stringDate1.split('-');
  var year1  = splitDate1[0];
  var month1 = splitDate1[1];
  var day1 = splitDate1[2];
  this.transactionHistory.requestdate[0] =day1+"/"+ month1+"/"+year1;
//               if(this.transactionHistory.voucherrecords.voucherrecord.expirydate){
//                 this.transactionHistory.voucherrecords.voucherrecord.expirydate=this.transactionHistory.voucherrecords.voucherrecord.expirydate.split(" ");
//                 this.transactionHistory.voucherrecords.voucherrecord.expirydate=this.transactionHistory.voucherrecords.voucherrecord.expirydate[0];
//                 var stringDate1=this.transactionHistory.voucherrecords.voucherrecord.expirydate;
// var splitDate1 = stringDate1.split('-');
// var year1  = splitDate1[0];
// var month1 = splitDate1[1];
// var day1 = splitDate1[2];
// this.transactionHistory.voucherrecords.voucherrecord.expirydate =day1+"/"+ month1+"/"+year1;
//               }
//               else{
//                 if(this.transactionHistory.voucherrecords.voucherrecord){
//                   for(var i=0;i<this.transactionHistory.voucherrecords.voucherrecord.length;i++){
//                     this.transactionHistory.voucherrecords.voucherrecord[i].expirydate=this.transactionHistory.voucherrecords.voucherrecord[i].expirydate.split(" ");
//                     this.transactionHistory.voucherrecords.voucherrecord[i].expirydate=this.transactionHistory.voucherrecords.voucherrecord[i].expirydate[0];
//                     var stringDate1=this.transactionHistory.voucherrecords.voucherrecord[i].expirydate;
//     var splitDate1 = stringDate1.split('-');
//     var year1  = splitDate1[0];
//     var month1 = splitDate1[1];
//     var day1 = splitDate1[2];
//     this.transactionHistory.voucherrecords.voucherrecord[i].expirydate =day1+"/"+ month1+"/"+year1;
//                   }
//                 }
//               }
              
              if(this.transactionHistory.usernumber.toString().length==11){
               this.transactionHistory.usernumber=this.transactionHistory.usernumber.toString();
                this.transactionHistory.usernumber.slice(3);
              }
            }
            if(this.recordCount>1){
              
              for(var i=0;i<this.transactionHistory.length;i++){
                this.transactionHistory[i]['vouchertotal']=this.transactionHistory[i].amountpre * this.transactionHistory[i].quantity;
                this.transactionHistory[i].vouchertotal=Math.round(this.transactionHistory[i].vouchertotal * 100) / 100
                this.transactionHistory[i]['totalamountwords']=Math.round((this.transactionHistory[i].amountpre+this.transactionHistory[i].sourcevat+this.transactionHistory[i].fiscalvat)* 100) / 100
          
                    
                    this.transactionHistory[i].totalamountwords=this.totalwords(this.transactionHistory[i].totalamountwords)
                  //  console.log(this.transactionHistory[i].totalamountwords)  
                  this.transactionHistory[i].requestdate=this.transactionHistory[i].requestdate.split(" ");
                this.transactionHistory[i].requestdate[1]=this.tConvert(this.transactionHistory[i].requestdate[1]);
                         var stringDate1=this.transactionHistory[i].requestdate[0];
  var splitDate1 = stringDate1.split('-');
  var year1  = splitDate1[0];
  var month1 = splitDate1[1];
  var day1 = splitDate1[2];
  this.transactionHistory[i].requestdate[0] =day1+"/"+ month1+"/"+year1;
  //               if(this.transactionHistory[i].voucherrecords.voucherrecord.expirydate){
  //                 this.transactionHistory[i].voucherrecords.voucherrecord.expirydate=this.transactionHistory[i].voucherrecords.voucherrecord.expirydate.split(" ");
  //                 this.transactionHistory[i].voucherrecords.voucherrecord.expirydate=this.transactionHistory[i].voucherrecords.voucherrecord.expirydate[0];
  //                 var stringDate1=this.transactionHistory[i].voucherrecords.voucherrecord.expirydate;
  // var splitDate1 = stringDate1.split('-');
  // var year1  = splitDate1[0];
  // var month1 = splitDate1[1];
  // var day1 = splitDate1[2];
  // this.transactionHistory[i].voucherrecords.voucherrecord.expirydate =day1+"/"+ month1+"/"+year1;
  //               }
  //               else{
  //                 if(this.transactionHistory[i].voucherrecords.voucherrecord){
  //                   for(var i=0;i<this.transactionHistory[i].voucherrecords.voucherrecord.length;i++){
  //                     this.transactionHistory[i].voucherrecords.voucherrecord[i].expirydate=this.transactionHistory[i].voucherrecords.voucherrecord[i].expirydate.split(" ");
  //                     this.transactionHistory[i].voucherrecords.voucherrecord[i].expirydate=this.transactionHistory[i].voucherrecords.voucherrecord[i].expirydate[0];
  //                     var stringDate1=this.transactionHistory[i].voucherrecords.voucherrecord[i].expirydate;
  //     var splitDate1 = stringDate1.split('-');
  //     var year1  = splitDate1[0];
  //     var month1 = splitDate1[1];
  //     var day1 = splitDate1[2];
  //     this.transactionHistory[i].voucherrecords.voucherrecord[i].expirydate =day1+"/"+ month1+"/"+year1;
  //                   }
  //                 }
  //               }
                if(this.transactionHistory[i].usernumber.toString().length==11){
                  this.transactionHistory[i].usernumber=this.transactionHistory[i].usernumber.toString();
                  this.transactionHistory[i].usernumber=this.transactionHistory[i].usernumber.slice(3);

             


                }
              }
            }
          //   //  // console.log("records",this.transactionHistory); 
          //   //  // console.log("history",result);
            this.historyResults=result;
       
            if(mpin!=undefined && mpin!=''){
              if(a.length!=0){
               
                this.screen2=true;
                this.screen1=false;
                this.historyForm.mpin='';
                mpin='';
                this.screen0=false;
                this.scrollToTop('scroll');
                
               } 
             }
             else{Swal.fire('Operation failed', 'Please provide mpin');
             this.f1=false;
    this.f2=false;
    this.f3=false;}
          }
          else{Swal.fire('Operation failed', 'No record Found');
          this.f1=false;
    this.f2=false;
    this.f3=false;}
       
        }
        else{
         Swal.fire('Operation failed', this.historyResultDesc);
         this.f1=false;
    this.f2=false;
    this.f3=false;
        }
        
      });
    }
    if(this.f2==true){
      this.apiService.transf2Api(mpin,this.historyForm.transactionid).subscribe(result=>{ var b:any=result; var resultcode=b.estel.response.resultcode; if(resultcode!=undefined && (resultcode==1956 || resultcode=='1956') ){this.router.navigate(['']);localStorage.clear();}
        this.historyResults=result;
     //   //  // console.log(this.historyResults.estel.response.resultdescription);
        this.historyResultDesc=this.historyResults.estel.response.resultdescription;
        if(this.historyResultDesc=='Transaction Successful'){
          this.calculateIndexes();
          this.recordCount=this.historyResults.estel.response.recordcount;
          if(this.recordCount>0){
            this.history=this.historyResults.estel.response.wallethistory;
            this.agentName=this.historyResults.estel.response.agentname;
            this.msisdn=this.historyResults.estel.response.phoneno;
          //   //  // console.log("history",result);
            this.transactionHistory=result;
            this.transactionHistory=this.transactionHistory.estel.response.records.record;
            if(this.recordCount==1){
              this.transactionHistory['vouchertotal']=this.transactionHistory.amountpre * this.transactionHistory.quantity;
              this.transactionHistory.vouchertotal=Math.round(this.transactionHistory.vouchertotal * 100) / 100
               this.transactionHistory['totalamountwords']=Math.round((this.transactionHistory.amountpre+this.transactionHistory.sourcevat+this.transactionHistory.fiscalvat)* 100) / 100
            //   console.log(this.transactionHistory.sourcevat)
          
                    
                  this.transactionHistory.totalamountwords=this.totalwords(this.transactionHistory.totalamountwords)
              this.transactionHistory.requestdate=this.transactionHistory.requestdate.split(" ");
              this.transactionHistory.requestdate[1]=this.tConvert(this.transactionHistory.requestdate[1]);
                    var stringDate1=this.transactionHistory.requestdate[0];
  var splitDate1 = stringDate1.split('-');
  var year1  = splitDate1[0];
  var month1 = splitDate1[1];
  var day1 = splitDate1[2];
  this.transactionHistory.requestdate[0] =day1+"/"+ month1+"/"+year1;
//              if(this.transactionHistory.voucherrecords.voucherrecord.expirydate){
//                 this.transactionHistory.voucherrecords.voucherrecord.expirydate=this.transactionHistory.voucherrecords.voucherrecord.expirydate.split(" ");
//                 this.transactionHistory.voucherrecords.voucherrecord.expirydate=this.transactionHistory.voucherrecords.voucherrecord.expirydate[0];
//                 var stringDate1=this.transactionHistory.voucherrecords.voucherrecord.expirydate;
// var splitDate1 = stringDate1.split('-');
// var year1  = splitDate1[0];
// var month1 = splitDate1[1];
// var day1 = splitDate1[2];
// this.transactionHistory.voucherrecords.voucherrecord.expirydate =day1+"/"+ month1+"/"+year1;
//               }
//               else{
//                 if(this.transactionHistory.voucherrecords.voucherrecord){
//                   for(var i=0;i<this.transactionHistory.voucherrecords.voucherrecord.length;i++){
//                     this.transactionHistory.voucherrecords.voucherrecord[i].expirydate=this.transactionHistory.voucherrecords.voucherrecord[i].expirydate.split(" ");
//                     this.transactionHistory.voucherrecords.voucherrecord[i].expirydate=this.transactionHistory.voucherrecords.voucherrecord[i].expirydate[0];
//                     var stringDate1=this.transactionHistory.voucherrecords.voucherrecord[i].expirydate;
//     var splitDate1 = stringDate1.split('-');
//     var year1  = splitDate1[0];
//     var month1 = splitDate1[1];
//     var day1 = splitDate1[2];
//     this.transactionHistory.voucherrecords.voucherrecord[i].expirydate =day1+"/"+ month1+"/"+year1;
//                   }
//                 }
//               }
              this.transactionHistory.requestdate[1]=this.tConvert(this.transactionHistory.requestdate[1]);
              if(this.transactionHistory.usernumber.toString().length==11){
               this.transactionHistory.usernumber=this.transactionHistory.usernumber.toString();
                this.transactionHistory.usernumber.slice(3);
              }
            }
            if(this.recordCount>1){
               // console.log(this.transactionHistory.length,'hi');
              for(var i=0;i<this.transactionHistory.length;i++){
                  // console.log(this.transactionHistory[i].usernumber.toString().length);
                  this.transactionHistory[i]['totalamountwords']=Math.round((this.transactionHistory[i].amountpre+this.transactionHistory[i].sourcevat+this.transactionHistory[i].fiscalvat)* 100) / 100
          
                    
                  this.transactionHistory[i].totalamountwords=this.totalwords(this.transactionHistory[i].totalamountwords)
                 // console.log(this.transactionHistory[i].totalamountwords)  
             
                  this.transactionHistory[i]['vouchertotal']=this.transactionHistory[i].amountpre * this.transactionHistory[i].quantity;
                  this.transactionHistory[i].vouchertotal=Math.round(this.transactionHistory[i].vouchertotal * 100) / 100
                if(this.transactionHistory[i].usernumber.toString().length==11){
                  this.transactionHistory[i].usernumber=this.transactionHistory[i].usernumber.toString();
                  this.transactionHistory[i].usernumber=this.transactionHistory[i].usernumber.slice(3);
                }
              }
            }
          //   //  // console.log("records",this.transactionHistory); 
          //   //  // console.log("history",result);
            this.historyResults=result;
       
            if(mpin!=undefined && mpin!=''){
              if(a.length!=0){
                
                this.screen2=true;
                this.screen1=false;
                this.historyForm.mpin='';
                mpin='';
                this.screen0=false;
                this.scrollToTop('scroll');
               } 
             }
             else{Swal.fire('Operation failed', 'No record Found');
             this.f1=false;
    this.f2=false;
    this.f3=false;}
          }
          else{Swal.fire('Operation failed', 'No record Found');
          this.f1=false;
    this.f2=false;
    this.f3=false;}
       
        }
        else{
         Swal.fire('Operation failed', this.historyResultDesc);
         this.f1=false;
    this.f2=false;
    this.f3=false;
        }
        
      });
    }
    if(this.f3==true){
      this.apiService.transf3Api(mpin,this.historyForm.reciptid).subscribe(result=>{ var b:any=result; var resultcode=b.estel.response.resultcode; if(resultcode!=undefined && (resultcode==1956 || resultcode=='1956') ){this.router.navigate(['']);localStorage.clear();}
        this.historyResults=result;
     //   //  // console.log(this.historyResults.estel.response.resultdescription);
        this.historyResultDesc=this.historyResults.estel.response.resultdescription;
        if(this.historyResultDesc=='Transaction Successful'){
          this.calculateIndexes();
          this.recordCount=this.historyResults.estel.response.recordcount;
          this.refcode=this.historyResults.estel.response.referencecode;
          if(this.recordCount>0){
            this.history=this.historyResults.estel.response.wallethistory;
            this.agentName=this.historyResults.estel.response.agentname;
            this.msisdn=this.historyResults.estel.response.phoneno;
          //   //  // console.log("history",result);
            this.transactionHistory=result;
            this.transactionHistory=this.transactionHistory.estel.response.records.record;
            if(this.recordCount==1){
              this.transactionHistory['vouchertotal']=this.transactionHistory.amountpre * this.transactionHistory.quantity;
              this.transactionHistory.vouchertotal=Math.round(this.transactionHistory.vouchertotal * 100) / 100
               this.transactionHistory['totalamountwords']=Math.round((this.transactionHistory.amountpre+this.transactionHistory.sourcevat+this.transactionHistory.fiscalvat)* 100) / 100
               console.log(this.transactionHistory.sourcevat)
          
                    
                  this.transactionHistory.totalamountwords=this.totalwords(this.transactionHistory.totalamountwords)
              this.transactionHistory.requestdate=this.transactionHistory.requestdate.split(" ");
              this.transactionHistory.requestdate[1]=this.tConvert(this.transactionHistory.requestdate[1]);
                    var stringDate1=this.transactionHistory.requestdate[0];
  var splitDate1 = stringDate1.split('-');
  var year1  = splitDate1[0];
  var month1 = splitDate1[1];
  var day1 = splitDate1[2];
  this.transactionHistory.requestdate[0] =day1+"/"+ month1+"/"+year1;
//              if(this.transactionHistory.voucherrecords.voucherrecord.expirydate){
//                 this.transactionHistory.voucherrecords.voucherrecord.expirydate=this.transactionHistory.voucherrecords.voucherrecord.expirydate.split(" ");
//                 this.transactionHistory.voucherrecords.voucherrecord.expirydate=this.transactionHistory.voucherrecords.voucherrecord.expirydate[0];
//                 var stringDate1=this.transactionHistory.voucherrecords.voucherrecord.expirydate;
// var splitDate1 = stringDate1.split('-');
// var year1  = splitDate1[0];
// var month1 = splitDate1[1];
// var day1 = splitDate1[2];
// this.transactionHistory.voucherrecords.voucherrecord.expirydate =day1+"/"+ month1+"/"+year1;
//               }
//               else{
//                 if(this.transactionHistory.voucherrecords.voucherrecord){
//                   for(var i=0;i<this.transactionHistory.voucherrecords.voucherrecord.length;i++){
//                     this.transactionHistory.voucherrecords.voucherrecord[i].expirydate=this.transactionHistory.voucherrecords.voucherrecord[i].expirydate.split(" ");
//                     this.transactionHistory.voucherrecords.voucherrecord[i].expirydate=this.transactionHistory.voucherrecords.voucherrecord[i].expirydate[0];
//                     var stringDate1=this.transactionHistory.voucherrecords.voucherrecord[i].expirydate;
//     var splitDate1 = stringDate1.split('-');
//     var year1  = splitDate1[0];
//     var month1 = splitDate1[1];
//     var day1 = splitDate1[2];
//     this.transactionHistory.voucherrecords.voucherrecord[i].expirydate =day1+"/"+ month1+"/"+year1;
//                   }
//                 }
//               }
              this.transactionHistory.requestdate[1]=this.tConvert(this.transactionHistory.requestdate[1]);
              if(this.transactionHistory.usernumber.toString().length==11){
               this.transactionHistory.usernumber=this.transactionHistory.usernumber.toString();
                this.transactionHistory.usernumber.slice(3);
              }
            }
            if(this.recordCount>1){
              
              for(var i=0;i<this.transactionHistory.length;i++){
                this.transactionHistory[i]['totalamountwords']=Math.round((this.transactionHistory[i].amountpre+this.transactionHistory[i].sourcevat+this.transactionHistory[i].fiscalvat)* 100) / 100
          
                    
                this.transactionHistory[i].totalamountwords=this.totalwords(this.transactionHistory[i].totalamountwords)
                console.log(this.transactionHistory[i].totalamountwords)  
           
                  // console.log(this.transactionHistory[i].usernumber.toString().length);
                if(this.transactionHistory[i].usernumber.toString().length==11){
                  this.transactionHistory[i].usernumber=this.transactionHistory[i].usernumber.toString();
                  this.transactionHistory[i].usernumber=this.transactionHistory[i].usernumber.slice(3);
                }
              }
            }
          //   //  // console.log("records",this.transactionHistory); 
          //   //  // console.log("history",result);
            this.historyResults=result;
       
            if(mpin!=undefined && mpin!=''){
              if(a.length!=0){
                
                this.screen2=true;
                this.screen1=false;
                this.historyForm.mpin='';
                mpin='';
                this.screen0=false;
                this.scrollToTop('scroll');
               } 
             }
             else{Swal.fire('Operation failed', 'No record Found');
             this.f1=false;
    this.f2=false;
    this.f3=false;}
          }
          else{Swal.fire('Operation failed', 'No record Found');
          this.f1=false;
    this.f2=false;
    this.f3=false;}
       
        }
        else{
         Swal.fire('Operation failed', this.historyResultDesc);
         this.f1=false;
    this.f2=false;
    this.f3=false;
        }
        
      });
    }
  }
    
  }
 historyResultDesc;
  cancel(){
    this.screen0=true;
    this.screen1=false;
    this.screen2=false;
    this.f1=false;
    this.f2=false;
    this.f3=false;
    this.historyForm.username=undefined;
    this.historyForm.dateto=undefined;
    this.historyForm.datefrom=undefined;
    this.historyForm.state=undefined;
    this.historyForm.operationtype=undefined;
    this.historyForm.mpin=undefined;
    this.historyForm.mpin1=undefined;
    this.historyForm.transactionid=undefined;
    this.historyForm.mpin2=undefined;
    this.historyForm.reciptid=undefined;
    this.pageSize=10;
    this.csvdata=[];
  }
  print(): void {
    let printContents, popupWin;
    printContents = document.getElementById('print-section').innerHTML;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>Print tab</title>
          <style>
        @media print {
          html, body {
            width: 7.2cm;
            height: 15cm;
          }
          prints{
              width: 7.2cm;
              height: 15cm;
          }
          .fsize-10{
            font-size: 12px !important;
          }
          .fsize-12{
            font-size: 14px !important;
          }
          .text-center{
            text-align:center;
          }
      }
        //........Customized style.......
        </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
    );
    popupWin.document.close();
  }
  trans(){
    this.apiService.transStatusApi(this.historyForm.mobile,this.historyForm.pin).subscribe(result=>{ var b:any=result; var resultcode=b.estel.response.resultcode; if(resultcode!=undefined && (resultcode==1956 || resultcode=='1956') ){this.router.navigate(['']);localStorage.clear();}
      this.historyResults=result;
      //   //  // console.log(this.historyResults.estel.response.resultdescription);
         this.historyResultDesc=this.historyResults.estel.response.resultdescription;
         if(this.historyResultDesc=='Transaction Successful'){
          this.calculateIndexes();
           this.recordCount=this.historyResults.estel.response.recordcount;
           if(this.recordCount>0){
             this.history=this.historyResults.estel.response.wallethistory;
             this.agentName=this.historyResults.estel.response.agentname;
             this.msisdn=this.historyResults.estel.response.phoneno;
           //   //  // console.log("history",result);
             this.transactionHistory=result;
             this.transactionHistory=this.transactionHistory.estel.response.records.record;
             
           //   //  // console.log("records",this.transactionHistory); 
           //   //  // console.log("history",result);
             this.historyResults=result;
        
             
           }
           else{Swal.fire('Operation failed', 'No record Found');}
        
         }
         else{
          Swal.fire('Operation failed', this.historyResultDesc);
         }
    })
  }
  totalamountwords;
totalwords(total){
this.totalamountwords=this.withDecimal(total);
return this.withDecimal(total)
}


 a = [
  '',
  'One ',
  'Two ',
  'Three ',
  'Four ',
  'Five ',
  'Six ',
  'Seven ',
  'Eight ',
  'Nine ',
  'Ten ',
  'Eleven ',
  'Twelve ',
  'Thirteen ',
  'Fourteen ',
  'Fifteen ',
  'Sixteen ',
  'Seventeen ',
  'Eighteen ',
  'Nineteen '];

b = [
  '',
  '',
  'Twenty',
  'Thirty',
  'Forty',
  'Fifty',
  'Sixty',
  'Seventy',
  'Eighty',
  'Ninety'];
  
  transform(value: any, args?: any): any {
    if (value) {
      let num: any = Number(value);
      if (num) {
        if ((num = num.toString()).length > 9)  { return 'We are not the Iron Bank, you can lower down the stakes :)'; }
        const n = ('000000000' + num).substr(-9).match(/^(\d{2})(\d{2})(\d{2})(\d{1})(\d{2})$/);
        if (!n) {return ''; }
        let str = '';
        str += (Number(n[1]) !== 0) ? (this.a[Number(n[1])] || this.b[n[1][0]] + ' ' + this.a[n[1][1]]) + 'CRORE ' : '';
        str += (Number(n[2]) !== 0) ? (this.a[Number(n[2])] || this.b[n[2][0]] + ' ' + this.a[n[2][1]]) + 'LAKH ' : '';
        str += (Number(n[3]) !== 0) ? (this.a[Number(n[3])] || this.b[n[3][0]] + ' ' + this.a[n[3][1]]) + 'THOUSAND ' : '';
        str += (Number(n[4]) !== 0) ? (this.a[Number(n[4])] || this.b[n[4][0]] + ' ' + this.a[n[4][1]]) + 'HUNDRED ' : '';
        str += (Number(n[5]) !== 0) ? ((str !== '') ? 'and ' : '') +
        (this.a[Number(n[5])] || this.b[n[5][0]] + ' ' +
        this.a[n[5][1]]) + '' : '';
      //   //  // console.log(str);
        
        return str;
      } else {
        return '';
      }
    } else {
      return '';
    }
  }
   withDecimal(n) {
      var nums = n.toString().split('.')
      var whole = this.transform(nums[0])
      if (nums.length == 2) {
          var fraction = this.transform(nums[1])
          return whole + 'Us Dollars And ' + fraction + 'Cents Only';
      } else {
          return whole + 'Us Dollars Only';
      }
  }
  

  screenf1(){
    if(this.historyForm.username==undefined || this.historyForm.username=='' || this.historyForm.state==undefined || this.historyForm.state=='' || this.historyForm.operationtype==undefined || this.historyForm.operationtype=='')
    {
      if(this.historyForm.operationtype==undefined || this.historyForm.operationtype==''){
        Swal.fire('Operation Failed', 'Please select Operation Type');
      }
      if(this.historyForm.state==undefined || this.historyForm.state==''){
        Swal.fire('Operation Failed', 'Please select status');
      }
if(this.historyForm.username==undefined || this.historyForm.username==''){
  Swal.fire('Operation Failed', 'Please provide Vaild Actor Id / Username');
}
    }
    else{
      this.f1=true;
     this.screen2Show(this.historyForm.mpin);
    }
   
  }
  screenf2(){ 
    let upperCaseCharacters = /[A-Z]+/g;
  let lowerCaseCharacters = /[a-z]+/g;
  let numberCharacters = /[0-9]+/g;
  let specialCharacters = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;
    if((this.historyForm.transactionid!=undefined && this.historyForm.transactionid!='') && (lowerCaseCharacters.test(this.historyForm.transactionid)==false && numberCharacters.test(this.historyForm.transactionid)==true && upperCaseCharacters.test(this.historyForm.transactionid)==false && specialCharacters.test(this.historyForm.transactionid)==false) ){
      this.f2=true;
      this.screen2Show(this.historyForm.mpin1);
    }
    else{
      Swal.fire('Operation Failed', 'Please provide Vaild Transaction Id.');
       
    }
  }
  screenf3(){
    let upperCaseCharacters = /[A-Z]+/g;
  let lowerCaseCharacters = /[a-z]+/g;
  let numberCharacters = /[0-9]+/g;
  let specialCharacters = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;
    if((this.historyForm.reciptid!=undefined && this.historyForm.reciptid!='') && (lowerCaseCharacters.test(this.historyForm.reciptid)==false && numberCharacters.test(this.historyForm.reciptid)==true && upperCaseCharacters.test(this.historyForm.reciptid)==false && specialCharacters.test(this.historyForm.reciptid)==false)){
      this.f3=true;
      this.screen2Show(this.historyForm.mpin2);
        // console.log(this.historyForm.mpin2);
    }
    else{
      Swal.fire('Operation Failed', 'Please provide Valid Recipt Id');
       
    }
  }
  csvdata=[];
  csvdownload(){
    this.csvdata=[];
    //'Transaction Id','Date', 'Sender', 'Destination', 'Channel', 'Type', 'Sender Profile', 'Result Description', 
    //'Status','Amount without vat','Vat','Amount with vat','Fiscal Stamp','Total amount','Sender pre balance','Sender post balance','Receiver pre balance','Receiver post balance'
   // type csv={transid,date,senderprofile,destination,channel,type,sender,result,status,amountpre,vat,amountwithvat,fiscalstamp,totalamount,senderprebalance,senderpostbalance,Receiverprebalance,Receiverpostbalance}

    
    if(this.transactionHistory.length>1){
    
      for(var i=0;i<this.transactionHistory.length;i++){
       var csv={};
        csv['transid']=this.transactionHistory[i].transid;
        csv['date']=this.transactionHistory[i].requestdate[0] + ' ' +this.transactionHistory[i].requestdate[1];
        csv['senderprofile']=this.transactionHistory[i].leveldesc;
        if(this.transactionHistory[i].destination!=undefined){
          csv['destination']=this.transactionHistory[i].destination;
        }
        else{
          csv['destination']=this.transactionHistory[i].usercode;
        }
       
        if(this.transactionHistory[i].clienttype=='SELFCARE' || this.transactionHistory[i].clienttype=='ANDROID' || this.transactionHistory[i].clienttype==undefined){
          if(this.transactionHistory[i].clienttype=='SELFCARE'){
            csv['channel']='Web Portal'; 
          }
          if(this.transactionHistory[i].clienttype=='ANDROID'){
            csv['channel']='Mobile Application'; 
          }
          if(this.transactionHistory[i].clienttype==undefined){
            csv['channel']=''; 
          }
        }
        else{csv['channel']=this.transactionHistory[i].clienttype;}
        if(this.transactionHistory[i].transtype=='TOPUP' || this.transactionHistory[i].transtype=='BULKPINPURCHASE' || this.transactionHistory[i].transtype=='MOVESTOCK' || this.transactionHistory[i].transtype=='WALLETADJ'){
          if(   this.transactionHistory[i].transtype=='MOVESTOCK'){
            csv['type']='Transfer';
          }
          if(this.transactionHistory[i].transtype=='BULKPINPURCHASE' ){
            csv['type']='E-Voucher';
          } 
          if(this.transactionHistory[i].transtype=='TOPUP' ){
            csv['type']='E-Recharge';
          }
          if(this.transactionHistory[i].transtype=='WALLETADJ' ){
            csv['type']='Wallet Adjustment';
          }
        }
        else{ csv['type']=this.transactionHistory[i].transtype;}
       if(this.transactionHistory[i].transtype=='WALLETADJ'){
        csv['sender']='ALFA';
       }
       else{
        csv['sender']=this.transactionHistory[i].usercode;
       }
       
        csv['result']=this.transactionHistory[i].resultDesc;
        if(this.transactionHistory[i].transtype!='ORDER' && this.transactionHistory[i].resultDesc=='Transaction Successful')
        {csv['status']='Success'}
        if(this.transactionHistory[i].transtype!='ORDER' && this.transactionHistory[i].resultDesc!='Transaction Successful')
        {csv['status']='Failure'}
        if(this.transactionHistory[i].transtype=='ORDER')
        { if(this.transactionHistory[i].responsevalue){
          csv['status']=this.transactionHistory[i].responsevalue;
        }
        else{
          csv['status']='Failure'
        }
          }
        //csv['status']=this.transactionHistory[i].responsevalue;
        csv['amountpre']=Number(this.transactionHistory[i].amountpre);
        if(this.transactionHistory[i].transtype=='ORDER'){
          csv['vat']=Number(this.transactionHistory[i].amountpost)-Number(this.transactionHistory[i].amountpre);
        }
        else{
          csv['vat']=Number(this.transactionHistory[i].sourcevat);
        }
        if(this.transactionHistory[i].transtype=='ORDER'){
          csv['amountwithvat']=Number(this.transactionHistory[i].amountpre)+Number(this.transactionHistory[i].amountpost)-Number(this.transactionHistory[i].amountpre);
        }
        else{
          csv['amountwithvat']=Number(this.transactionHistory[i].amountpre)+Number(this.transactionHistory[i].sourcevat);
        }
        
        csv['fiscalstamp']=this.transactionHistory[i].fiscalvat;
        if(this.transactionHistory[i].quantity!=undefined && this.transactionHistory[i].quantity!='0' && this.transactionHistory[i].quantity!=0){
          csv['quantity']=this.transactionHistory[i].quantity;
        }
        else{
          csv['quantity']='';
        }
        if(this.transactionHistory[i].expirydate!=undefined){
          csv['expirydate']=this.transactionHistory[i].expirydate;
        }
        else{
          csv['expirydate']='';
        }
        csv['totalamount']=this.transactionHistory[i].amount;
        if(this.transactionHistory[i].prewalletbalance!=undefined){
          csv['senderprebalance']=this.transactionHistory[i].prewalletbalance;
        }
        else{
          csv['senderprebalance']='';
        }
        if(this.transactionHistory[i].walletbalance!=undefined){
          csv['senderpostbalance']=this.transactionHistory[i].walletbalance;
        }
        else{
          csv['senderpostbalance']='';
        }
        if(this.transactionHistory[i].srcfiscalclosing!=undefined){
          csv['senderpostfiscalbalance']=this.transactionHistory[i].srcfiscalclosing;
        }
        else{
          csv['senderpostfiscalbalance']='';
        }
  
        if(this.transactionHistory[i].srcfiscalopening!=undefined){
          csv['senderprefiscalbalance']=this.transactionHistory[i].srcfiscalopening;
        }
        else{
          csv['senderprefiscalbalance']='';
        }
        if(this.transactionHistory[i].preoprwallet!=undefined){
          csv['Receiverprebalance']=this.transactionHistory[i].preoprwallet;
        }
        else{
          csv['Receiverprebalance']='';
        }
        if(this.transactionHistory[i].oprwallet!=undefined){
          csv['Receiverpostbalance']=this.transactionHistory[i].oprwallet;
        }
        else{
          csv['Receiverpostbalance']='';
        }
      //&& (this.transactionHistory[i].transtype=='MOVESTOCK' || this.transactionHistory[i].transtype=='ORDER' || this.transactionHistory[i].transtype=='WALLETADJ')
        if(this.transactionHistory[i].wallettype!=undefined ){
          if(this.transactionHistory[i].wallettype==true){csv['transfertype']='Stock';}
          if(this.transactionHistory[i].wallettype==false){csv['transfertype']='Fiscal Stamp';}
          if(this.transactionHistory[i].transtype=='ORDER'){
            if(this.transactionHistory[i].wallettype=='Value'){csv['transfertype']='Stock';}
            if(this.transactionHistory[i].wallettype=='Fiscal'){csv['transfertype']='Fiscal Stamp';}
          }
        }
        else{
          csv['transfertype']='';
        }
      
        if(this.transactionHistory[i].amount!=undefined && this.transactionHistory[i].transtype=='WALLETADJ'){
          if(Number(this.transactionHistory[i].amount)>0){csv['adjustmenttype']='Credit';}
          // if(Number(this.transactionHistory[i].amount)<0){csv['adjustmenttype']='Debit';}
          else{csv['adjustmenttype']='Debit';}
        }
        else{
          csv['adjustmenttype']='';
        }
      
        var cs=csv;
        this.csvdata.push(cs);
        //  // console.log(this.transactionHistory[i].transid)
        //  // console.log(cs)
      }
     //  // console.log(this.csvdata);
      this.apiService.downloadFile(this.csvdata, 'AlfaTransactionDetails');
      
    }
    if(this.transactionHistory.length==undefined || this.transactionHistory.length==1 ){
      var csv={};
      csv['transid']=this.transactionHistory.transid;
      csv['date']=this.transactionHistory.requestdate[0] +' ' +this.transactionHistory.requestdate[1];
      csv['senderprofile']=this.transactionHistory.leveldesc;
      if(this.transactionHistory.destination!=undefined){
        csv['destination']=this.transactionHistory.destination;
      }
      else{
        csv['destination']=this.transactionHistory.usercode;
      }
     
      if(this.transactionHistory.clienttype=='SELFCARE' || this.transactionHistory.clienttype=='ANDROID' || this.transactionHistory.clienttype==undefined){
        if(this.transactionHistory.clienttype=='SELFCARE'){
          csv['channel']='Web Portal'; 
        }
        if(this.transactionHistory.clienttype=='ANDROID'){
          csv['channel']='Mobile Application'; 
        }
        if(this.transactionHistory.clienttype==undefined){
          csv['channel']=''; 
        }
      }
      else{csv['channel']=this.transactionHistory.clienttype;}
      if(this.transactionHistory.transtype=='TOPUP' || this.transactionHistory.transtype=='BULKPINPURCHASE' || this.transactionHistory.transtype=='MOVESTOCK' || this.transactionHistory.transtype=='WALLETADJ'){
        if(   this.transactionHistory.transtype=='MOVESTOCK'){
          csv['type']='Transfer';
        }
        if(this.transactionHistory.transtype=='BULKPINPURCHASE' ){
          csv['type']='E-Voucher';
        } 
        if(this.transactionHistory.transtype=='TOPUP' ){
          csv['type']='E-Recharge';
        }
        if(this.transactionHistory.transtype=='WALLETADJ' ){
          csv['type']='Wallet Adjustment';
        }
      }
      else{ csv['type']=this.transactionHistory.transtype;}
     
      if(this.transactionHistory.transtype=='WALLETADJ'){
        csv['sender']='ALFA';
       }
       else{
        csv['sender']=this.transactionHistory.usercode;
       }
      csv['result']=this.transactionHistory.resultDesc;
      if(this.transactionHistory.transtype!='ORDER' && this.transactionHistory.resultDesc=='Transaction Successful')
      {csv['status']='Success'}
      if(this.transactionHistory.transtype!='ORDER' && this.transactionHistory.resultDesc!='Transaction Successful')
      {csv['status']='Failure'}
      if(this.transactionHistory.transtype=='ORDER')
      { if(this.transactionHistory.responsevalue){
        csv['status']=this.transactionHistory.responsevalue;
      }
      else{
        csv['status']='Failure'
      }
        }
      //csv['status']=this.transactionHistory.responsevalue;
      csv['amountpre']=Number(this.transactionHistory.amountpre);
      if(this.transactionHistory.transtype=='ORDER'){
        csv['vat']=Number(this.transactionHistory.amountpost)-Number(this.transactionHistory.amountpre);
      }
      else{
        csv['vat']=Number(this.transactionHistory.sourcevat);
      }
      if(this.transactionHistory.transtype=='ORDER'){
        csv['amountwithvat']=Number(this.transactionHistory.amountpre)+Number(this.transactionHistory.amountpost)-Number(this.transactionHistory.amountpre);
      }
      else{
        csv['amountwithvat']=Number(this.transactionHistory.amountpre)+Number(this.transactionHistory.sourcevat);
      }
      
      csv['fiscalstamp']=this.transactionHistory.fiscalvat;
      if(this.transactionHistory.quantity!=undefined && this.transactionHistory.quantity!='0' && this.transactionHistory.quantity!=0){
        csv['quantity']=this.transactionHistory.quantity;
      }
      else{
        csv['quantity']='';
      }
      if(this.transactionHistory.expirydate!=undefined){
        csv['expirydate']=this.transactionHistory.expirydate;
      }
      else{
        csv['expirydate']='';
      }
      csv['totalamount']=this.transactionHistory.amount;
      if(this.transactionHistory.prewalletbalance!=undefined){
        csv['senderprebalance']=this.transactionHistory.prewalletbalance;
      }
      else{
        csv['senderprebalance']='';
      }
      if(this.transactionHistory.walletbalance!=undefined){
        csv['senderpostbalance']=this.transactionHistory.walletbalance;
      }
      else{
        csv['senderpostbalance']='';
      }
      if(this.transactionHistory.srcfiscalclosing!=undefined){
        csv['senderpostfiscalbalance']=this.transactionHistory.srcfiscalclosing;
      }
      else{
        csv['senderpostfiscalbalance']='';
      }

      if(this.transactionHistory.srcfiscalopening!=undefined){
        csv['senderprefiscalbalance']=this.transactionHistory.srcfiscalopening;
      }
      else{
        csv['senderprefiscalbalance']='';
      }
      if(this.transactionHistory.preoprwallet!=undefined){
        csv['Receiverprebalance']=this.transactionHistory.preoprwallet;
      }
      else{
        csv['Receiverprebalance']='';
      }
      if(this.transactionHistory.oprwallet!=undefined){
        csv['Receiverpostbalance']=this.transactionHistory.oprwallet;
      }
      else{
        csv['Receiverpostbalance']='';
      }
    //&& (this.transactionHistory.transtype=='MOVESTOCK' || this.transactionHistory.transtype=='ORDER' || this.transactionHistory.transtype=='WALLETADJ')
      if(this.transactionHistory.wallettype!=undefined ){
        if(this.transactionHistory.wallettype==true){csv['transfertype']='Stock';}
        if(this.transactionHistory.wallettype==false || this.transactionHistory.wallettype=='Fiscal' || this.transactionHistory.wallettype=='fiscal'){csv['transfertype']='Fiscal Stamp';}
        if(this.transactionHistory.transtype=='ORDER'){
          if(this.transactionHistory.wallettype=='Value'){csv['transfertype']='Stock';}
          if(this.transactionHistory.wallettype=='Fiscal'){csv['transfertype']='Fiscal Stamp';}
        }
      }
      else{
        csv['transfertype']='';
      }
    
      if(this.transactionHistory.amount!=undefined && this.transactionHistory.transtype=='WALLETADJ'){
        if(Number(this.transactionHistory.amount)>0){csv['adjustmenttype']='Credit';}
        // if(Number(this.transactionHistory.amount)<0){csv['adjustmenttype']='Debit';}
        else{csv['adjustmenttype']='Debit';}
      }
      else{
        csv['adjustmenttype']='';
      }
      
      this.apiService.downloadFile(csv, 'AlfaTransactionDetails');
      // console.log(csv);
    }
  }
  tConvert (time) {
  // Check correct time format and split into components
  time = time.toString ().match (/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

  if (time.length > 1) { // If time format correct
    time = time.slice (1);  // Remove full string match value
    time[5] = +time[0] < 12 ? 'AM' : 'PM'; // Set AM/PM
    time[0] = +time[0] % 12 || 12; // Adjust hours
  }
  return time.join (''); // return adjusted time or original string
}
  @Input() items;
  @Input() label: string;
  @Input() pageSize = 10;
  @Input() maxPages = 100;
  @Input() previousLabel = 'Previous';
  @Input() nextLabel = 'Next';
  @Input() screenReaderPaginationLabel = 'Pagination';
  @Input() screenReaderPageLabel = 'page';
  @Input() screenReaderCurrentLabel = `You're on page`;
  @Output() onItemClick: EventEmitter<any> = new EventEmitter();
pagesizechange($event){
  this.pageSize=$event.target.value;
  this.calculateIndexes();
}
  private currentPage = 1;
  private pages: Array<number>;
  private startIndex: number;
  private endIndex: number;
  calculateIndexes() {
    const pagination = paginate(
      this.historyResults.estel.response.records.record.length,
      this.currentPage,
      this.pageSize,
      this.maxPages
    );
    this.currentPage = pagination.currentPage;
    this.pages = pagination.pages;
    this.startIndex = pagination.startIndex;
    this.endIndex = pagination.endIndex;
  }

  previous(e) {
    e.preventDefault();
    this.currentPage--;
    this.calculateIndexes();
  }

  next(e) {
    e.preventDefault();
    this.currentPage++;
    this.calculateIndexes();
  }

  setCurrent(e, page) {
    e.preventDefault();
    this.currentPage = page;
    this.calculateIndexes();
  }

  getLabel(i) {
    return this.items[i].name;
  }

  onClick(item) {
    this.onItemClick.emit(item);
  }
}
const paginate = (
  totalItems: number,
  currentPage: number = 1,
  pageSize: number = 10,
  maxPages: number = 10
) => {
  const totalPages = Math.ceil(totalItems / pageSize);

  if (currentPage < 1) {
    currentPage = 1;
  } else if (currentPage > totalPages) {
    currentPage = totalPages;
  }

  let startPage: number, endPage: number;
  if (totalPages <= maxPages) {
    startPage = 1;
    endPage = totalPages;
  } else {
    const maxPagesBeforeCurrentPage = Math.floor(maxPages / 2);
    const maxPagesAfterCurrentPage = Math.ceil(maxPages / 2) - 1;
    if (currentPage <= maxPagesBeforeCurrentPage) {
      startPage = 1;
      endPage = maxPages;
    } else if (currentPage + maxPagesAfterCurrentPage >= totalPages) {
      startPage = totalPages - maxPages + 1;
      endPage = totalPages;
    } else {
      startPage = currentPage - maxPagesBeforeCurrentPage;
      endPage = currentPage + maxPagesAfterCurrentPage;
    }
  }

  const startIndex = (currentPage - 1) * pageSize;
  const endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1);

  const pages = Array.from(Array((endPage + 1) - startPage).keys()).map(i => startPage + i);

  return {
    totalItems: totalItems,
    currentPage: currentPage,
    pageSize: pageSize,
    totalPages: totalPages,
    startPage: startPage,
    endPage: endPage,
    startIndex: startIndex,
    endIndex: endIndex,
    pages: pages
  };
};
