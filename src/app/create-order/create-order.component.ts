import { Component, OnInit } from '@angular/core';
import{Validators,FormControl,FormGroup} from '@angular/forms';
import { NgbModal,NgbModalRef,NgbModule } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';
import {ApiService} from '../Services/api.service';
import {formatDate} from '@angular/common';
import{AppConfig} from '../app.config';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';   
import {Router} from '@angular/router';       
import{AuthGuard} from '../Services/auth.guard';
@Component({
  selector: 'app-create-order',
  templateUrl: './create-order.component.html',
  styleUrls: ['./create-order.component.scss']
})
export class CreateOrderComponent implements OnInit {


  createOrder;public createOrderForm;screen3;screen4;config;qt;checke;
 
  constructor(private unlock:AuthGuard,private apiService:ApiService,private spinnerService: Ng4LoadingSpinnerService,private router:Router) {}
  items;itemPrice=0;itemName;itemResults;total;vat=0;selectedItem;selectedItemIncode;transactionType;vath;totalh=0;
agenttype;msisdnError=false;msisdnLength=AppConfig.msisdnLength;msisdnErrorText=AppConfig.msisdnError;
checkmsisdnFieldLength(elem){
  let upperCaseCharacters = /[A-Z]+/g;
  let lowerCaseCharacters = /[a-z]+/g;
  let numberCharacters = /[0-9]+/g;
  let specialCharacters = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;
  if ((this.createOrderForm.notifier.toString().length != this.msisdnLength && (this.createOrderForm.notifier.toString().length!=0 || this.createOrderForm.notifier.toString().length!=undefined)) || numberCharacters.test(this.createOrderForm.notifier) === false || upperCaseCharacters.test(this.createOrderForm.notifier) === true || lowerCaseCharacters.test(this.createOrderForm.notifier) === true  || specialCharacters.test(this.createOrderForm.notifier) === true ) {
   this.msisdnError=true;
          
      }
      else{
        this.msisdnError=false;
      }
  }
  numberWithCommas(x) {
    var parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    parts[0].toString();
    return parts.join(".");
}
fromChangeDateEvent(ev){
    
  ev=this.createOrderForm.neededDate.year+'-'+this.createOrderForm.neededDate.month+'-'+this.createOrderForm.neededDate.day;
  
  this.createOrderForm.neededDate=new Date(ev);
  this.createOrderForm.neededDate=formatDate(this.createOrderForm.neededDate,'yyyy/MM/dd','en')
 // this.fDate=new Date(this.createOrderForm.neededDate);
  this.fDate=formatDate(this.createOrderForm.neededDate, 'dd/MM/yyyy', 'en');
// this.createOrderForm.neededDate=this.fDate;
  // this.createOrderForm.neededDate=ev;
  //   // console.log(ev);
}
fromdatemodel;fromDate='';fDate='';
reset(){
  this.checke=undefined;
  this.vath='';
  this.totalh=undefined;
  this.totalc='';
this.createOrderForm.quantity='';
this.createOrderForm.notifier='';
this.createOrderForm.orderDate=new Date(); 
 this.createOrderForm.orderDate=formatDate(this.createOrderForm.orderDate, 'yyyy-MM-dd', 'en')
 //  // console.log(this.createOrderForm.orderDate);
 if(this.agenttype!='Direct'){
  var expiry=new Date();
expiry.setDate(new Date().getDate() + + this.orderexpiry);
//  // console.log(expiry);
  this.createOrderForm.expiryDate=expiry;
  this.createOrderForm.expiryDate=formatDate(this.createOrderForm.expiryDate, 'yyyy-MM-dd', 'en')
}
this.createOrderForm.neededDate=undefined;
this.fDate='';
//  // console.log(this.createOrderForm.quantity,this.createOrderForm.notifier)
}
onChangeItemEvent(ev) {
   // should print option1
  for(var i=0;i<this.items.length;i++){
    if(ev.target.value==this.items[i].incode){
      this.itemPrice=this.items[i].value;
      this.selectedItem=this.items[i].name;
      this.selectedItemIncode=this.items[i].incode;
    //   //  // console.log(this.itemPrice,this.items[i].name);
    }
  }
}
onChangeDateEvent(ev) {
   //   //  // console.log(ev.target.value);
     var orderdate=new Date(ev.target.value);
//  if(this.createOrderForm.type!='Fiscal Stamp' && this.agenttype!='Direct'){
   
    if(this.agenttype!='Direct'){
      var expiry=new Date();
   expiry.setDate(orderdate.getDate() + 30);
  //   //  // console.log(expiry);
      this.createOrderForm.expiryDate=expiry;
      this.createOrderForm.expiryDate=formatDate(this.createOrderForm.expiryDate, 'yyyy-MM-dd', 'en')
    }
//  }
}
totalc='';
amount(ev){

  this.vath=this.itemPrice*this.createOrderForm.quantity*this.vat/100;
  this.totalh=(this.itemPrice*this.createOrderForm.quantity)+(((this.itemPrice*this.createOrderForm.quantity)*this.vat))/100;
  this.createOrderForm.quantity=Math.round(this.createOrderForm.quantity * 100) / 100;
  this.vath=Math.round(this.vath * 100) / 100;
  this.totalh=Math.round(this.totalh * 100) / 100;
  this.totalh.toString();
  this.totalc=this.numberWithCommas(this.totalh);
  this.vath=this.numberWithCommas(this.vath);
}
onChangeTransTypeEvent(ev) {
  // should print option1
 
   if(ev.target.value=='val'){
     this.vat=this.unlock.decryptData((localStorage.getItem('Vat')));
    this.createOrderForm.type='Value'; 
    this.walletType=true;
   }
  
    if(ev.target.value=='fiscal'){
      this.vat=0;
      this.createOrderForm.type='Fiscal Stamp';
      this.walletType=false;
    }
    if(this.createOrderForm.quantity)
    this.amount(ev);
  //   //;
     
   
 //   //  // console.log("order type",this.createOrderForm.type);
 
}
notifiers;
itemcount;
  ngOnInit() {
    this.checke='val';
    this.agenttype=this.unlock.decryptData(localStorage.getItem('agenttype'));
    this.orderexpiry=this.unlock.decryptData(localStorage.getItem('orderexpiry'));
      // console.log(this.orderexpiry);
   this.apiService.getItemApi().subscribe(result=>{ var b:any=result; var resultcode=b.estel.response.resultcode; if(resultcode!=undefined && (resultcode==1956 || resultcode=='1956') ){this.router.navigate(['']);localStorage.clear();}
    
this.itemResults=result;
this.items=this.itemResults.estel.response.records.record;
this.itemcount=this.itemResults.estel.response.recordcount;
this.notifiers=this.itemResults.estel.response.srcnotify;

this.notifiers=this.notifiers.split(",");
for(var i=0;i<this.notifiers.length;i++){
  
 
  this.notifiers[i]=this.notifiers[i].split("0");
  for(var j=0;j<this.notifiers[i].length;j++){
    this.notifiers[i][1]=this.notifiers[i][1].replace(/\s/g, "");
    this.notifiers[i][j]=this.notifiers[i][j].replace('(', "");
    this.notifiers[i][j]=this.notifiers[i][j].replace(')', "");
  }
}

if(this.itemcount>1){
  this.itemPrice=this.items[0].value;
this.selectedItem=this.items[0].name;
this.selectedItemIncode=this.items[0].incode;
}
if(this.itemcount==1){
  this.itemPrice=this.items.value;
this.selectedItem=this.items.name;
this.selectedItemIncode=this.items.incode;
}
   })
    this.createOrder=true;
    this.config=true;
    this.screen3=false;
    this.screen4=false;
    this.createOrderForm = new FormGroup({
      'orderType': new FormControl( [
        Validators.required,
       
      ]),
      'orderDate': new FormControl( [
        Validators.required,
       
      ]),
    
      'quantity': new FormControl( [
        Validators.required,
       
      ]),
      'price': new FormControl( [
        Validators.required,
       
      ]),  'vat': new FormControl( [
        Validators.required,
       
      ]),  'total': new FormControl( [
        Validators.required,
       
      ]),  'expiryDate': new FormControl( [
        Validators.required,
       
      ]),
      'notifier': new FormControl( [
        Validators.required,
       
      ]),  'neededDate': new FormControl( [
        Validators.required,
       
      ]),
    });
    this.qt=this.createOrderForm.quantity;
  
 this.createOrderForm.orderDate=new Date(); this.exp=new Date();this.order=new Date();
 this.createOrderForm.orderDate=formatDate(this.createOrderForm.orderDate, 'yyyy-MM-dd', 'en')
 this.order=formatDate(this.createOrderForm.orderDate, 'dd-MM-yyyy', 'en')
 this.orderexpiry=Number(this.orderexpiry);
 this.vat=this.unlock.decryptData((localStorage.getItem('Vat')));
    this.createOrderForm.type='Value'; 
    this.walletType=true;
 //  // console.log(this.createOrderForm.orderDate);
 if(this.agenttype!='Direct'){
  var expiry=new Date();
expiry.setDate(new Date().getDate() + this.orderexpiry);
//   // console.log(this.orderexpiry,new Date().getDate() + this.orderexpiry,new Date().getDate()+90,new Date().getDate() + 90);
  this.createOrderForm.expiryDate=expiry;
  this.createOrderForm.expiryDate=formatDate(this.createOrderForm.expiryDate, 'yyyy-MM-dd', 'en')
  
  this.exp=formatDate(this.createOrderForm.expiryDate, 'dd-MM-yyyy', 'en')
}
  }exp;order;
  mpinEdit(){
this.createOrder=true;
this.config=false;
  }
orderDate;expiryDate;neededDate;VAT;
  screen3Show(){
    var a=this.createOrderForm.orderDate;
    var b=this.createOrderForm.quantity;
    var c=this.createOrderForm.price;
    var d=this.vat;
    var e=this.createOrderForm.expiryDate;
    var f=this.createOrderForm.notifier;
    var g=this.createOrderForm.neededDate;
    var todaysDate = formatDate(new Date(), 'dd/MM/yyyy', 'en')
  //   //  // console.log("todays date",todaysDate);
    if(a!=undefined && g!=undefined){
      this.orderDate=formatDate(a, 'dd/MM/yyyy', 'en')
     
      this.neededDate=formatDate(g, 'dd/MM/yyyy', 'en')
      a=formatDate(a, 'dd/MM/yyyy', 'en')
     
      g=formatDate(g, 'dd/MM/yyyy', 'en')
      var orderdate=new Date(this.createOrderForm.orderDate);

      var today=new Date();
      var expiry=new Date(this.createOrderForm.expiryDate);
      var needed=new Date(this.createOrderForm.neededDate);
    //   //  // console.log(orderdate.getMonth(),orderdate.getFullYear(),orderdate.getDate());
if(this.createOrderForm.expiryDate!=undefined){
  this.expiryDate=formatDate(e, 'dd/MM/yyyy', 'en'); e=formatDate(e, 'dd/MM/yyyy', 'en');}
  var dateStatus=false;
  if(orderdate.getDate() == today.getDate() && orderdate.getMonth() == today.getMonth() && orderdate.getFullYear() == today.getFullYear())
  {dateStatus=true;}
      if(orderdate.getTime() < today.getTime() && dateStatus==false) {
       
          Swal.fire('Operation Failed', 'Order date cannot be before current date');
        
       
    }
    else{
      if(this.createOrderForm.expiryDate && (orderdate.getTime() > expiry.getTime())) {
        Swal.fire('Operation Failed', 'Expiry cannot be before order date');
    }
    else{
      if((this.createOrderForm.expiryDate && (needed.getTime() > expiry.getTime())) || needed.getTime() < orderdate.getTime()) {
        Swal.fire('Operation Failed', 'Needed date must be between order date and expiry date.');
    }
    else{
   
    if(a==undefined || b==undefined || d==undefined  || f==undefined || g==undefined || a=='' || b==''  || f=="" || g=="" || this.walletType==undefined)
    Swal.fire('Operation Failed', 'All fields are required');
    else{
      if(this.msisdnError==false){
        this.screen3=true;
    
     this.screen4=false;  this.createOrder=false;
     this.config=false;
      }
     
     if(this.msisdnError==true) 
   { Swal.fire('Operation failed', 'Notifier Number is not valid') }
    }
    }
  
    }
    }
    
    }
    else{
      Swal.fire('Operation Failed', 'All fields are required');
    }

   
    
  }
  cancel(){ 
    this.screen3=false;
  this.createOrder=true;
    this.screen4=false;
    this.config=true;
  }
  response;walletType;orderexpiry;
  screen4Show(){
    
    this.spinnerService.show();
   var vat=this.itemPrice*this.createOrderForm.quantity*this.vat/100
    var createOrderResults;
    var createOrderResultsDesc;
    this.total=(this.itemPrice*this.createOrderForm.quantity)+(this.itemPrice*this.createOrderForm.quantity)*this.vat/100
    this.total=Math.round(this.total * 100) / 100;
if(this.agenttype=='Direct'){
  this.apiService.createOrderApi(this.itemPrice*this.createOrderForm.quantity,this.walletType,this.selectedItemIncode,this.createOrderForm.quantity,vat,this.total,this.orderDate,'',this.neededDate,this.createOrderForm.notifier).subscribe(result=>{ var b:any=result; var resultcode=b.estel.response.resultcode; if(resultcode!=undefined && (resultcode==1956 || resultcode=='1956') ){this.router.navigate(['']);localStorage.clear();}
  //   //  // console.log("create order",result);
    createOrderResults=result;
    createOrderResultsDesc=createOrderResults.estel.response.resultdescription;
    if(createOrderResultsDesc=="Transaction Successful"){
      this.response=createOrderResults.estel.response;
      this.VAT=this.response.totalamount - this.response.amount;

      this.VAT=Math.round(this.VAT * 100) / 100;
      this.response.totalamount=Math.round(this.response.totalamount*100)/100;
      
      this.screen4=true;
 this.config=false;
  this.screen3=false;
  this.createOrder=false;
  if(this.itemcount>1){
    this.itemPrice=this.items[0].value;
  this.selectedItem=this.items[0].name;
this.selectedItemIncode=this.items[0].incode;
  }
  if(this.itemcount==1){
    this.itemPrice=this.items.value;
  this.selectedItem=this.items.name;
this.selectedItemIncode=this.items.incode;
  }

this.orderDate='';
this.expiryDate='';this.neededDate='';
this.reset();
this.spinnerService.hide();
    }
    else{
  
        if(createOrderResultsDesc=='Total Amount Of Daily Transactions Limit Reached' || createOrderResults.estel.response.resultcode==431 || createOrderResults.estel.response.resultcode==432 || createOrderResults.estel.response.resultcode==433 || createOrderResults.estel.response.resultcode==434 || createOrderResults.estel.response.resultcode==435 || createOrderResults.estel.response.resultcode==436 || createOrderResults.estel.response.resultcode==437 || createOrderResults.estel.response.resultcode==4377 || createOrderResults.estel.response.resultcode=='431' || createOrderResults.estel.response.resultcode=='432' || createOrderResults.estel.response.resultcode=='433' || createOrderResults.estel.response.resultcode=='434' || createOrderResults.estel.response.resultcode=='435' || createOrderResults.estel.response.resultcode=='436' || createOrderResults.estel.response.resultcode=='437' || createOrderResults.estel.response.resultcode=='4377' ){
       
          if(createOrderResults.estel.response.resultcode==431 || createOrderResults.estel.response.resultcode=='431'){
          Swal.fire('Operation Failed', 'You have reached the Daily amount maximum transaction limit.Kindly contact our Help Line on 119 (free of charge)');
          this.spinnerService.hide();
        }
        if(createOrderResults.estel.response.resultcode==432 || createOrderResults.estel.response.resultcode=='432'){
          Swal.fire('Operation Failed', 'You have reached the Monthly amount maximum transaction limit.Kindly contact our Help Line on 119 (free of charge)');
          this.spinnerService.hide();
        }
        if(createOrderResults.estel.response.resultcode=='433' ||createOrderResults.estel.response.resultcode==433){
          Swal.fire('Operation Failed', 'You have reached the Weekly amount maximum transaction limit.Kindly contact our Help Line on 119 (free of charge)');
          this.spinnerService.hide();
        }
        if(createOrderResults.estel.response.resultcode==434 || createOrderResults.estel.response.resultcode=='434'){
          Swal.fire('Operation Failed', 'You have reached the Daily maximum transaction limit.Kindly contact our Help Line on 119 (free of charge)');
          this.spinnerService.hide();
        }
        if(createOrderResults.estel.response.resultcode==435 || createOrderResults.estel.response.resultcode=='435'){
          Swal.fire('Operation Failed', 'You have reached the Monthly maximum transaction limit.Kindly contact our Help Line on 119 (free of charge)');
          this.spinnerService.hide();
        }
        if(createOrderResults.estel.response.resultcode==436 || createOrderResults.estel.response.resultcode=='436'){
          Swal.fire('Operation Failed', 'You have reached the Weekly maximum transaction limit.Kindly contact our Help Line on 119 (free of charge)');
          this.spinnerService.hide();
        }
        if(createOrderResults.estel.response.resultcode==437 || createOrderResults.estel.response.resultcode=='437'){
          Swal.fire('Operation Failed', 'Your transaction amount is above the maximum authorized limit');
          this.spinnerService.hide();
        }
        if(createOrderResults.estel.response.resultcode==4377 || createOrderResults.estel.response.resultcode=='4377'){
          Swal.fire('Operation Failed', 'Your transaction amount is less than the minimum limit. Kindly contact our Help Line on 119 (free of charge)');
          this.spinnerService.hide();
        }
      
      }
      else{Swal.fire('Operation Failed',createOrderResultsDesc);
   
      this.spinnerService.hide();}
    }
  })
}
  else{
    if(this.walletType==true){
      this.apiService.createOrderApi(this.itemPrice*this.createOrderForm.quantity,this.walletType,this.selectedItemIncode,this.createOrderForm.quantity,vat,this.total,this.orderDate,this.expiryDate,this.neededDate,this.createOrderForm.notifier).subscribe(result=>{ var b:any=result; var resultcode=b.estel.response.resultcode; if(resultcode!=undefined && (resultcode==1956 || resultcode=='1956') ){this.router.navigate(['']);localStorage.clear();}
      //   //  // console.log("create order",result);
        createOrderResults=result;
        createOrderResultsDesc=createOrderResults.estel.response.resultdescription;
        if(createOrderResultsDesc=="Transaction Successful"){
          this.response=createOrderResults.estel.response;
          this.VAT=this.response.totalamount - this.response.amount;

      this.VAT=Math.round(this.VAT * 100) / 100;
      this.response.totalamount=Math.round(this.response.totalamount*100)/100;
          this.screen4=true;
     this.config=false;
      this.screen3=false;
      this.createOrder=false;
      if(this.itemcount>1){
        this.itemPrice=this.items[0].value;
      this.selectedItem=this.items[0].name;
  this.selectedItemIncode=this.items[0].incode;
      }
      if(this.itemcount==1){
        this.itemPrice=this.items.value;
      this.selectedItem=this.items.name;
  this.selectedItemIncode=this.items.incode;
      }
  
  this.orderDate='';
  this.expiryDate='';this.neededDate='';
  this.reset();
  this.spinnerService.hide();
        }
        else{
          if(createOrderResultsDesc=='Total Amount Of Daily Transactions Limit Reached' || createOrderResults.estel.response.resultcode==431 || createOrderResults.estel.response.resultcode==432 || createOrderResults.estel.response.resultcode==433 || createOrderResults.estel.response.resultcode==434 || createOrderResults.estel.response.resultcode==435 || createOrderResults.estel.response.resultcode==436 || createOrderResults.estel.response.resultcode==437 || createOrderResults.estel.response.resultcode==4377 || createOrderResults.estel.response.resultcode=='431' || createOrderResults.estel.response.resultcode=='432' || createOrderResults.estel.response.resultcode=='433' || createOrderResults.estel.response.resultcode=='434' || createOrderResults.estel.response.resultcode=='435' || createOrderResults.estel.response.resultcode=='436' || createOrderResults.estel.response.resultcode=='437' || createOrderResults.estel.response.resultcode=='4377' ){
       
            if(createOrderResults.estel.response.resultcode==431 || createOrderResults.estel.response.resultcode=='431'){
            Swal.fire('Operation Failed', 'You have reached the Daily amount maximum transaction limit.Kindly contact our Help Line on 119 (free of charge)');
            this.spinnerService.hide();
          }
          if(createOrderResults.estel.response.resultcode==432 || createOrderResults.estel.response.resultcode=='432'){
            Swal.fire('Operation Failed', 'You have reached the Monthly amount maximum transaction limit.Kindly contact our Help Line on 119 (free of charge)');
            this.spinnerService.hide();
          }
          if(createOrderResults.estel.response.resultcode=='433' ||createOrderResults.estel.response.resultcode==433){
            Swal.fire('Operation Failed', 'You have reached the Weekly amount maximum transaction limit.Kindly contact our Help Line on 119 (free of charge)');
            this.spinnerService.hide();
          }
          if(createOrderResults.estel.response.resultcode==434 || createOrderResults.estel.response.resultcode=='434'){
            Swal.fire('Operation Failed', 'You have reached the Daily maximum transaction limit.Kindly contact our Help Line on 119 (free of charge)');
            this.spinnerService.hide();
          }
          if(createOrderResults.estel.response.resultcode==435 || createOrderResults.estel.response.resultcode=='435'){
            Swal.fire('Operation Failed', 'You have reached the Monthly maximum transaction limit.Kindly contact our Help Line on 119 (free of charge)');
            this.spinnerService.hide();
          }
          if(createOrderResults.estel.response.resultcode==436 || createOrderResults.estel.response.resultcode=='436'){
            Swal.fire('Operation Failed', 'You have reached the Weekly maximum transaction limit.Kindly contact our Help Line on 119 (free of charge)');
            this.spinnerService.hide();
          }
          if(createOrderResults.estel.response.resultcode==437 || createOrderResults.estel.response.resultcode=='437'){
            Swal.fire('Operation Failed', 'Your transaction amount is above the maximum authorized limit');
            this.spinnerService.hide();
          }
          if(createOrderResults.estel.response.resultcode==4377 || createOrderResults.estel.response.resultcode=='4377'){
            Swal.fire('Operation Failed', 'Your transaction amount is less than the minimum limit. Kindly contact our Help Line on 119 (free of charge)');
            this.spinnerService.hide();
          }
        
        }
        else{Swal.fire('Operation Failed',createOrderResultsDesc);
     
        this.spinnerService.hide();}
        }
      })
    }
    else{
      this.apiService.createOrderApi(this.itemPrice*this.createOrderForm.quantity,this.walletType,this.selectedItemIncode,this.createOrderForm.quantity,vat,this.total,this.orderDate,'',this.neededDate,this.createOrderForm.notifier).subscribe(result=>{ var b:any=result; var resultcode=b.estel.response.resultcode; if(resultcode!=undefined && (resultcode==1956 || resultcode=='1956') ){this.router.navigate(['']);localStorage.clear();}
      
        createOrderResults=result;
        createOrderResultsDesc=createOrderResults.estel.response.resultdescription;
        if(createOrderResultsDesc=="Transaction Successful"){
          this.response=createOrderResults.estel.response;
          this.VAT=this.response.totalamount - this.response.amount;

      this.VAT=Math.round(this.VAT * 100) / 100;
      this.response.totalamount=Math.round(this.response.totalamount*100)/100;
          this.screen4=true;
     this.config=false;
      this.screen3=false;
      this.createOrder=false;
      if(this.itemcount>1){
        this.itemPrice=this.items[0].value;
      this.selectedItem=this.items[0].name;
  this.selectedItemIncode=this.items[0].incode;
      }
      if(this.itemcount==1){
        this.itemPrice=this.items.value;
      this.selectedItem=this.items.name;
  this.selectedItemIncode=this.items.incode;
      }
  
  this.orderDate='';
  this.expiryDate='';this.neededDate='';
  this.reset();
  this.spinnerService.hide();
        }
        else{
          if(createOrderResultsDesc=='Total Amount Of Daily Transactions Limit Reached' || createOrderResults.estel.response.resultcode==431 || createOrderResults.estel.response.resultcode==432 || createOrderResults.estel.response.resultcode==433 || createOrderResults.estel.response.resultcode==434 || createOrderResults.estel.response.resultcode==435 || createOrderResults.estel.response.resultcode==436 || createOrderResults.estel.response.resultcode==437 || createOrderResults.estel.response.resultcode==4377 || createOrderResults.estel.response.resultcode=='431' || createOrderResults.estel.response.resultcode=='432' || createOrderResults.estel.response.resultcode=='433' || createOrderResults.estel.response.resultcode=='434' || createOrderResults.estel.response.resultcode=='435' || createOrderResults.estel.response.resultcode=='436' || createOrderResults.estel.response.resultcode=='437' || createOrderResults.estel.response.resultcode=='4377' ){
       
            if(createOrderResults.estel.response.resultcode==431 || createOrderResults.estel.response.resultcode=='431'){
            Swal.fire('Operation Failed', 'You have reached the Daily amount maximum transaction limit.Kindly contact our Help Line on 119 (free of charge)');
            this.spinnerService.hide();
          }
          if(createOrderResults.estel.response.resultcode==432 || createOrderResults.estel.response.resultcode=='432'){
            Swal.fire('Operation Failed', 'You have reached the Monthly amount maximum transaction limit.Kindly contact our Help Line on 119 (free of charge)');
            this.spinnerService.hide();
          }
          if(createOrderResults.estel.response.resultcode=='433' ||createOrderResults.estel.response.resultcode==433){
            Swal.fire('Operation Failed', 'You have reached the Weekly amount maximum transaction limit.Kindly contact our Help Line on 119 (free of charge)');
            this.spinnerService.hide();
          }
          if(createOrderResults.estel.response.resultcode==434 || createOrderResults.estel.response.resultcode=='434'){
            Swal.fire('Operation Failed', 'You have reached the Daily maximum transaction limit.Kindly contact our Help Line on 119 (free of charge)');
            this.spinnerService.hide();
          }
          if(createOrderResults.estel.response.resultcode==435 || createOrderResults.estel.response.resultcode=='435'){
            Swal.fire('Operation Failed', 'You have reached the Monthly maximum transaction limit.Kindly contact our Help Line on 119 (free of charge)');
            this.spinnerService.hide();
          }
          if(createOrderResults.estel.response.resultcode==436 || createOrderResults.estel.response.resultcode=='436'){
            Swal.fire('Operation Failed', 'You have reached the Weekly maximum transaction limit.Kindly contact our Help Line on 119 (free of charge)');
            this.spinnerService.hide();
          }
          if(createOrderResults.estel.response.resultcode==437 || createOrderResults.estel.response.resultcode=='437'){
            Swal.fire('Operation Failed', 'Your transaction amount is above the maximum authorized limit');
            this.spinnerService.hide();
          }
          if(createOrderResults.estel.response.resultcode==4377 || createOrderResults.estel.response.resultcode=='4377'){
            Swal.fire('Operation Failed', 'Your transaction amount is less than the minimum limit. Kindly contact our Help Line on 119 (free of charge)');
            this.spinnerService.hide();
          }
        
        }
        else{Swal.fire('Operation Failed',createOrderResultsDesc);
     
        this.spinnerService.hide();}
        }
      })
    }

  }
   
  }



}
