import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FiscalStampComponent } from './fiscal-stamp.component';

describe('FiscalStampComponent', () => {
  let component: FiscalStampComponent;
  let fixture: ComponentFixture<FiscalStampComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FiscalStampComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FiscalStampComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
