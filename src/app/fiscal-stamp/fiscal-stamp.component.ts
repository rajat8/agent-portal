// import { Component, OnInit } from '@angular/core';
// import{Validators,FormControl,FormGroup} from '@angular/forms';
// import { NgbModal,NgbModalRef,NgbModule } from '@ng-bootstrap/ng-bootstrap';
// import Swal from 'sweetalert2';
// @Component({
//   selector: 'app-fiscal-stamp',
//   templateUrl: './fiscal-stamp.component.html',
//   styleUrls: ['./fiscal-stamp.component.scss']
// })
// export class FiscalStampComponent implements OnInit {

//   constructor() { }
//   screen1;screen2;screen3;screen4;lengthError;voucher;
//   transType;
//   private transferForm;
//   hero;
//    ngOnInit() {
//      this.screen1=true;
//      this.screen2=false;
//      this.screen3=false;
//      this.screen4=false;
 
//      this.transferForm = new FormGroup({
//        'transactionType': new FormControl( [
//          Validators.required,
//          Validators.minLength(10),
        
//        ]),
//        'ReceiverActorId': new FormControl( [
//          Validators.required,
//          Validators.minLength(10),
        
//        ]),
//        'amount': new FormControl( [
//          Validators.required,
//          Validators.minLength(10),
        
//        ]),
//        'mpin': new FormControl( [
//          Validators.required,
//          Validators.minLength(10),
        
//        ])
//      });
//    }
//  screen2Show(type){
//    if(type=='01'){
//      this.transType='By Value';
//    }
//    else{
//      this.transType='By Fiscal Stamp';
//    }
//    var a=this.transferForm.ReceiverActorId;
//    var e=this.transferForm.mpin;
//    var f=this.transferForm.amount;
//  if(a!=undefined && e!=undefined && f!=undefined){
//    if(a.toString().length!=0){
//      this.screen2=false;
//      this.screen1=false;
//      this.screen3=true;
//      this.screen4=false;
//     }   
//     if(a.toString().length==0) 
//     { Swal.fire('Invalid Receiver ActorId', 'Receiver ActorId cannot be empty ') }
   
//     if(type=='02'){
//       this.voucher='e-voucher';
//     }
//  }
//  else{
//   Swal.fire('Operation Failed', 'All Fields are Required ')
//  }
//  }
//  screen3Show(){


  
//    Swal.fire('Operation Failed', 'Amount and mpin cannot be empty');
  
//     this.screen3=true;
//     this.screen1=false;
//     this.screen2=false;
//     this.screen4=false;
   
 
//    Swal.fire('Operation Failed', 'Amount and mpin cannot be empty');
//  }
//  cancel(){
//    this.screen1=true;
//    this.screen3=false;
//    this.screen2=false;
//    this.screen4=false;
//  }
//  screen4Show(){
//   this.screen4=true;
//   this.screen1=false;
//    this.screen3=false;
//    this.screen2=false;
//  }


// }
import { Component, OnInit, Input, forwardRef, ViewChild, AfterViewInit, Injector } from '@angular/core';
import { NgbTimeStruct, NgbDateStruct, NgbPopoverConfig, NgbPopover, NgbDatepicker } from '@ng-bootstrap/ng-bootstrap';
import { NG_VALUE_ACCESSOR, ControlValueAccessor, NgControl } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { DateTimeModel } from './date-time.model';
import { noop } from 'rxjs';

@Component({
    selector: 'app-fiscal-stamp',
    templateUrl: './fiscal-stamp.component.html',
    styleUrls: ['./fiscal-stamp.component.scss'],
    providers: [
        DatePipe,
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => FiscalStampComponent),
            multi: true
        }
    ]
})
export class FiscalStampComponent implements ControlValueAccessor, OnInit, AfterViewInit {
    @Input()
    dateString: string;

    @Input()
    inputDatetimeFormat = 'M/d/yyyy H:mm:ss';
    @Input()
    hourStep = 1;
    @Input()
    minuteStep = 15;
    @Input()
    secondStep = 30;
    @Input()
    seconds = true;

    @Input()
    disabled = false;

    private showTimePickerToggle = false;

    private datetime: DateTimeModel = new DateTimeModel();
    private firstTimeAssign = true;

    @ViewChild(NgbDatepicker)
    private dp: NgbDatepicker;

    @ViewChild(NgbPopover)
    private popover: NgbPopover;

    private onTouched: () => void = noop;
    private onChange: (_: any) => void = noop;

    private ngControl: NgControl;

    constructor(private config: NgbPopoverConfig, private inj: Injector ) {
        config.autoClose = 'outside';
        config.placement = 'auto';
    }

    ngOnInit(): void {
        this.ngControl = this.inj.get(NgControl);
    }

    ngAfterViewInit(): void {
        this.popover.hidden.subscribe($event => {
            this.showTimePickerToggle = false;
        });
    }

    writeValue(newModel: string) {
        if (newModel) {
            this.datetime = Object.assign(this.datetime, DateTimeModel.fromLocalString(newModel));
            this.dateString = newModel;
            this.setDateStringModel();
        } else {
            this.datetime = new DateTimeModel();
        }
    }

    registerOnChange(fn: any): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }

    toggleDateTimeState($event) {
        this.showTimePickerToggle = !this.showTimePickerToggle;
        $event.stopPropagation();
    }

    setDisabledState?(isDisabled: boolean): void {
        this.disabled = isDisabled;
    }

    onInputChange($event: any) {
        const value = $event.target.value;
        const dt = DateTimeModel.fromLocalString(value);

        if (dt) {
            this.datetime = dt;
            this.setDateStringModel();
        } else if (value.trim() === '') {
            this.datetime = new DateTimeModel();
            this.dateString = '';
            this.onChange(this.dateString);
        } else {
              this.onChange(value);
        }
    }

    onDateChange($event: string | NgbDateStruct) {    
      var ev:any; 
      ev=$event;
      
        if (ev.year){
          $event = `${ev.year}-${ev.month}-${ev.day}`
        }

        const date = DateTimeModel.fromLocalString(ev);
       
        // if (!date) {
        //     this.dateString = this.dateString;
        //       // console.log('test2');
        //     return;
        // }

        // if (!this.datetime) {
        //     this.datetime = date;
        // }

        this.datetime.year = ev.year;
        this.datetime.month = ev.month;
        this.datetime.day = ev.day;
        this.datetime.hour = 0;
        this.datetime.minute = 0;
        this.datetime.second = 0;
        // this.dp.navigateTo({ year: this.datetime.year, month: this.datetime.month });
       
        this.setDateStringModel();
      
    }

    onTimeChange(event: NgbTimeStruct) {
        this.datetime.hour = event.hour;
        this.datetime.minute = event.minute;
        this.datetime.second = event.second;

        this.setDateStringModel();
    }

    setDateStringModel() {
        this.dateString = this.datetime.toString();

        if (!this.firstTimeAssign) {
            this.onChange(this.dateString);
              // console.log(this.datetime,'1');
        } else {
            // console.log(this.datetime,'2');
          this.onChange(this.dateString);
            // Skip very first assignment to null done by Angular
            if (this.dateString !== null) {
                this.firstTimeAssign = false;
                  // console.log(this.datetime,'3');
            }
        }
    }

    inputBlur($event) {
        this.onTouched();
    }
}
