import { Component, OnInit } from '@angular/core';
import{Validators,FormControl,FormGroup} from '@angular/forms';
import { NgbModal,NgbModalRef,NgbModule } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-configuration',
  templateUrl: './configuration.component.html',
  styleUrls: ['./configuration.component.scss']
})
export class ConfigurationComponent {
mpinScreen;languageScreen;public configurationForm;screen3;screen4;config;
  constructor() { }

  ngOnInit() {
    this.languageScreen=false;
    this.mpinScreen=false;
    this.config=true;
    this.screen3=false;
    this.screen4=false;
    this.configurationForm = new FormGroup({
      'oldMpin': new FormControl( [
        Validators.required,
        Validators.minLength(10),
       
      ]),
      'newPin': new FormControl( [
        Validators.required,
        Validators.minLength(10),
       
      ]),
    
      'mpin': new FormControl( [
        Validators.required,
        Validators.minLength(10),
       
      ])
    });
  }
  mpinEdit(){
this.mpinScreen=true;
this.config=false;
  }
  languageEdit(){
    this.languageScreen=true;
    this.config=false;
  }
  screen3Show(){
    var a=this.configurationForm.mpin;
    var b=this.configurationForm.newPin;
    var c=this.configurationForm.oldMpin;
 
   if(a==undefined || b==undefined || c==undefined)
    Swal.fire('Operation Failed', 'All fields are required');
    else{
     this.screen3=true;
     this.languageScreen=false;
     this.screen4=false;  this.mpinScreen=false;
     this.config=false;
    }
    if(a=='' || b=='' || c=='')
    Swal.fire('Operation Failed', 'All fields are required');
  }
  cancel(){
   this.languageScreen=false;
    this.screen3=false;
  this.mpinScreen=false;
    this.screen4=false;
    this.config=true;
  }
  screen4Show(){
   this.screen4=true;
   this.languageScreen=false;
    this.screen3=false;
    this.mpinScreen=false;
  }
}
