import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ForcefulPinChangeComponent } from './forceful-pin-change.component';

describe('ForcefulPinChangeComponent', () => {
  let component: ForcefulPinChangeComponent;
  let fixture: ComponentFixture<ForcefulPinChangeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ForcefulPinChangeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForcefulPinChangeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
