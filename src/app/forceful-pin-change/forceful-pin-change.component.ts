import { Component, OnInit } from '@angular/core';
import{Validators,FormControl,FormGroup} from '@angular/forms';
import { NgbModal,NgbModalRef,NgbModule } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';
import {Router} from '@angular/router';
import{ApiService} from '../Services/api.service';
import{ AuthGuard} from '../Services/auth.guard';
import {AuthenticationService} from '../Services/authentication.service'
@Component({
  selector: 'app-forceful-pin-change',
  templateUrl: './forceful-pin-change.component.html',
  styleUrls: ['./forceful-pin-change.component.scss']
})
export class ForcefulPinChangeComponent implements OnInit {

 
  mpinScreen;languageScreen;public configurationForm;screen3;screen4;config;
  constructor(private unlock:AuthGuard,private apiService:ApiService,private router:Router,private authService:AuthenticationService) { }

  ngOnInit() {
    this.languageScreen=false;
    this.mpinScreen=true;
    this.config=true;
    this.screen3=false;
    this.screen4=false;
    this.configurationForm = new FormGroup({
      'oldMpin': new FormControl( [
        Validators.required,
        Validators.minLength(10),
       
      ]),
      'newPin': new FormControl( [
        Validators.required,
        Validators.minLength(10),
       
      ]),
    
      'mpin': new FormControl( [
        Validators.required,
        Validators.minLength(10),
       
      ])
    });
  }
  mpinEdit(){
this.mpinScreen=true;
this.config=false;
  }
  languageEdit(){
    this.languageScreen=true;
    this.config=false;
  }
  changeMpinResult; changeMpinResultDesc;
  screen3Show(){
    var a=this.configurationForm.mpin;
    var b=this.configurationForm.newPin;
    var c=this.configurationForm.oldMpin;
    a=a.toString();
    
    let upperCaseCharacters = /[A-Z]+/g;
  let lowerCaseCharacters = /[a-z]+/g;
  let numberCharacters = /[0-9]+/g;
  let specialCharacters = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;
  if (upperCaseCharacters.test(this.configurationForm.mpin) === false || lowerCaseCharacters.test(this.configurationForm.mpin) === false || numberCharacters.test(this.configurationForm.mpin) === false || specialCharacters.test(this.configurationForm.mpin) === false || a.length<8) {
     
    Swal.fire('Operation Failed', 'Password must be at least 8 characters and must contain the following: numbers, lowercase letters, uppercase letters and special characters.');
  }
  else{
    if(a==undefined || b==undefined || c==undefined)
    Swal.fire('Operation Failed', 'All fields are required');
    else{
      var agentcode=this.unlock.decryptData(localStorage.getItem('currentUser'));
      if(b!=c){
        if(a==b){
          this.apiService.forcechangeMpinApi(c,b).subscribe(result=>{ var b:any=result; var resultcode=b.estel.response.resultcode; if(resultcode!=undefined && (resultcode==1956 || resultcode=='1956') ){this.router.navigate(['']);localStorage.clear();}
          this.changeMpinResult=result;
          this.changeMpinResultDesc=this.changeMpinResult.estel.response.resultdescription;
          if(this.changeMpinResultDesc=='Transaction Successful'){
            // if(agentcode){
            //   this.authService.lout();
            // }
            
            this.screen3=true;
       this.languageScreen=false;
       this.screen4=false;  this.mpinScreen=false;
       this.config=false;
          }
          else{
            if(this.changeMpinResult.estel.response.resultcode=='60' || this.changeMpinResult.estel.response.resultcode==60 ){
              Swal.fire('Operation Failed','Old Password is invalid');
            }
            else
            Swal.fire('Operation Failed',this.changeMpinResultDesc);
          }
          })
  
        }
        else{Swal.fire('Operation Failed','New Password and confirm Password dont match');}
      }
      else{Swal.fire('Operation Failed','New Password and old Password cant be same');}
     
    }
  }
 
    if(a=='' || b=='' || c=='')
    Swal.fire('Operation Failed', 'All fields are required');
  }
  cancel(){
   this.languageScreen=false;
    this.screen3=false;
  this.mpinScreen=false;
    this.screen4=false;
    this.config=true;
  }
  screen4Show(){
   this.screen4=true;
   this.languageScreen=false;
    this.screen3=false;
    this.mpinScreen=false;
  }
  screenLoginShow(){
    this.router.navigate(['/login']);
  }
}
