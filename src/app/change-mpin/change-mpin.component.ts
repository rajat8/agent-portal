import { Component, OnInit } from '@angular/core';
import{Validators,FormControl,FormGroup,FormBuilder} from '@angular/forms';
import { NgbModal,NgbModalRef,NgbModule } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';
import{ApiService} from '../Services/api.service';
import{AuthGuard} from '../Services/auth.guard';
import {Router} from '@angular/router';
@Component({
  selector: 'app-change-mpin',
  templateUrl: './change-mpin.component.html',
  styleUrls: ['./change-mpin.component.scss']
})
export class ChangeMpinComponent implements OnInit {
  public myForms: FormGroup;
  mpinScreen;languageScreen;public configurationForm;screen3;screen4;config;responseTime;pinType='IPIN';
  constructor(private unlock:AuthGuard,private apiService:ApiService,fb: FormBuilder,private router:Router) {
    // this.myForms = fb.group({
    //   password: [null, Validators.compose([
    //     Validators.required, Validators.minLength(8), PasswordStrengthValidator])]
    // });
   }
msisdn;changeDate;pin='IPIN';
pintypeChange(ev){
  this.pinType=ev.target.value;
  this.pin=ev.target.value;
  this.configurationForm.mpin=undefined;
  this.configurationForm.oldMpin=undefined;
  this.configurationForm.newPin=undefined;
}username;
  ngOnInit() {
    this.username=this.unlock.decryptData(localStorage.getItem('username'));
    this.languageScreen=false;
    this.mpinScreen=true;
    this.config=true;
    this.screen3=false;
    this.screen4=false;
    this.configurationForm = new FormGroup({
      'oldMpin': new FormControl( [
        Validators.required,
        Validators.minLength(10),
       
      ]),
      'newPin': new FormControl( [
        Validators.required,
        Validators.minLength(10),
       
      ]),
    
      'mpin': new FormControl( [
        Validators.required,
        Validators.minLength(10),
       
      ])
    });
    this.msisdn=this.unlock.decryptData(localStorage.getItem('currentUser'));
  //   //  // console.log("msisdn",this.msisdn);
  }
  mpinEdit(){
this.mpinScreen=true;
this.config=false;
  }
  languageEdit(){
    this.languageScreen=true;
    this.config=false;
  }
  changeMpinResult; changeMpinResultDesc;
  screen3Show(){
   if(this.pinType!=undefined && this.configurationForm.oldMpin!=undefined && this.configurationForm.mpin!=undefined && this.configurationForm.newPin!=undefined && this.configurationForm.oldMpin!='' && this.configurationForm.mpin!='' && this.configurationForm.newPin!=''){
   
      var a=this.configurationForm.mpin;
   a=a.toString();
    
    let upperCaseCharacters = /[A-Z]+/g;
  let lowerCaseCharacters = /[a-z]+/g;
  let numberCharacters = /[0-9]+/g;
  let specialCharacters = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;
  
    var b=this.configurationForm.newPin;
    var c=this.configurationForm.oldMpin;
    if ((upperCaseCharacters.test(this.configurationForm.mpin) === false || lowerCaseCharacters.test(this.configurationForm.mpin) === false || numberCharacters.test(this.configurationForm.mpin) === false || specialCharacters.test(this.configurationForm.mpin) === false || a.length<8) && this.pin=='IPIN') {
     
      Swal.fire('Operation Failed', 'Password must be at least 8 characters and must contain the following: numbers, lowercase letters, uppercase letters and special characters.');
    }
    else{
      if((a.length!=6 || upperCaseCharacters.test(this.configurationForm.mpin) === true || lowerCaseCharacters.test(this.configurationForm.mpin) === true) && this.pinType=='TPIN'){
        Swal.fire('Operation Failed', 'Mpin must be 6 digits and contain numeric digits only');
      }
      else{
        if(a==undefined || b==undefined || c==undefined)
        Swal.fire('Operation Failed', 'All fields are required');
        else{
          if(b!=c){
            if(a==b){
              this.apiService.changeMpinApi(c,b,this.pinType).subscribe(result=>{ var b:any=result; var resultcode=b.estel.response.resultcode; if(resultcode!=undefined && (resultcode==1956 || resultcode=='1956') ){this.router.navigate(['']);localStorage.clear();}
              //   //  // console.log("change pin",result);
              this.changeMpinResult=result;
              this.changeMpinResultDesc=this.changeMpinResult.estel.response.resultdescription;
              if(this.changeMpinResultDesc=='Transaction Successful'){
                this.screen3=true;
           this.languageScreen=false;
           this.screen4=false;  this.mpinScreen=false;
           this.config=false;
           this.responseTime=this.changeMpinResult.estel.response.responsects;
           this.changeDate=this.responseTime.split(" ");
           a='';this.configurationForm.mpin='';
         b='';this.configurationForm.newPin='';
        c='';this.configurationForm.oldMpin='';
          }
              else{
                if(this.pinType=='IPIN' && (this.changeMpinResult.estel.response.resultcode==60 || this.changeMpinResult.estel.response.resultcode=='60')){
                  Swal.fire('Operation Failed','Old Password is invalid');
                }
                else
                Swal.fire('Operation Failed',this.changeMpinResultDesc);
              }
              })
      
            }
            else{
                if(this.pinType=='IPIN'){
                  Swal.fire('Operation Failed','New Password and confirm Password dont match');
                }
                else
              Swal.fire('Operation Failed','New mpin and confirm mpin dont match');}
          }
          else{
            if(this.pinType=='IPIN'){
              Swal.fire('Operation Failed','New Password and old Password cant be same');
            }
            else
            Swal.fire('Operation Failed','New mpin and old mpin cant be same');}
         
        }
      }
     
    }
   
    if(a=='' || b=='' || c=='')
    Swal.fire('Operation Failed', 'All fields are required');
   }
   else{
    if(this.configurationForm.oldMpin==undefined || this.configurationForm.mpin==undefined || this.configurationForm.newPin==undefined || this.configurationForm.oldMpin=='' || this.configurationForm.mpin=='' || this.configurationForm.newPin==''){
      Swal.fire('Operation Failed', 'All fields are required');
    }
    else{  Swal.fire('Operation Failed','Please Select Pin Type');} 
  }
  }
  cancel(){
   this.languageScreen=false;
    this.screen3=false;
  this.mpinScreen=true;
    this.screen4=false;
    this.config=true;
  }
  screen4Show(){
   this.screen4=true;
   this.languageScreen=false;
    this.screen3=false;
    this.mpinScreen=false;
  }

}
