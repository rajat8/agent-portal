import { Component, OnInit } from '@angular/core';
import{Validators,FormControl,FormGroup} from '@angular/forms';
import { NgbModal,NgbModalRef,NgbModalConfig,NgbModule } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';
import { NgbTabChangeEvent } from '@ng-bootstrap/ng-bootstrap';
import{ApiService} from '../Services/api.service';
import {Router} from '@angular/router';
import {formatDate} from '@angular/common';
import{AuthGuard} from '../Services/auth.guard';
@Component({
  selector: 'app-search-order',
  templateUrl: './search-order.component.html',
  styleUrls: ['./search-order.component.scss']
})
export class SearchOrderComponent implements OnInit {
  onChangeDateEvent(ev){
    
    ev=this.creationdatemodel.year+'-'+this.creationdatemodel.month+'-'+this.creationdatemodel.day;
    
    this.creationdatemodel=new Date(ev);
    this.cDate=formatDate(this.creationdatemodel, 'dd/MM/yyyy', 'en');
    this.creationDate=this.creationdatemodel;
    
  }
  creationdatemodel;creationDate='';cDate='';

  fromChangeDateEvent(ev){
    
    ev=this.fromdatemodel.year+'-'+this.fromdatemodel.month+'-'+this.fromdatemodel.day;
    
    this.fromdatemodel=new Date(ev);
    this.fDate=formatDate(this.fromdatemodel, 'dd/MM/yyyy', 'en');
    this.fromDate=this.fromdatemodel;
    
  }
  fromdatemodel;fromDate='';fDate='';

  toChangeDateEvent(ev){
 
    ev=this.todatemodel.year+'-'+this.todatemodel.month+'-'+this.todatemodel.day;
 
    this.todatemodel=new Date(ev);
    this.tDate=formatDate(this.todatemodel, 'dd/MM/yyyy', 'en');
    this.toDate=this.todatemodel;
 
  }
  todatemodel;toDate='';tDate='';

  efChangeDateEvent(ev){
   
    ev=this.efdatemodel.year+'-'+this.efdatemodel.month+'-'+this.efdatemodel.day;
   
    this.efdatemodel=new Date(ev);
    this.efromDate=formatDate(this.efdatemodel, 'dd/MM/yyyy', 'en');
    this.efDate=this.efdatemodel;
   
  }
  efdatemodel;efDate='';efromDate='';

  etChangeDateEvent(ev){
 
    ev=this.etdatemodel.year+'-'+this.etdatemodel.month+'-'+this.etdatemodel.day;
 
    this.etdatemodel=new Date(ev);
    this.etillDate=formatDate(this.etdatemodel, 'dd/MM/yyyy', 'en');
    this.etDate=this.etdatemodel;
 
  }
  etdatemodel;etDate='';etillDate='';
  constructor(private unlock:AuthGuard,private apiService:ApiService, private modalService: NgbModal,private router: Router) { }
  screen1;screen2;lengthError;voucher;createOrderForm;
   searchOrderForm;
  hero;
   search={orderId:false,creationDate:false,state:false,fromDate:false,expiryDate:false}
handleClick($event,radioValue) {
  
  //   //  // console.log('Radio radioValue',radioValue);
    if(radioValue==1){
      this.search.orderId=true;
      this.search.creationDate=false;
      this.search.state=false;
      this.search.fromDate=false;
      this.search.expiryDate=false;
    }
    if(radioValue==2){
      this.search.orderId=false;
      this.search.creationDate=true;
      this.search.state=false;
      this.search.fromDate=false;
      this.search.expiryDate=false;
    }
    if(radioValue==3){
      this.search.orderId=false;
      this.search.creationDate=false;
      this.search.state=true;
      this.search.fromDate=false;
      this.search.expiryDate=false;
    }
    if(radioValue==4){
      this.search.orderId=false;
      this.search.creationDate=false;
      this.search.state=false;
      this.search.fromDate=true;
      this.search.expiryDate=false;
    }
    if(radioValue==5){
      this.search.orderId=false;
      this.search.creationDate=false;
      this.search.state=false;
      this.search.fromDate=false;
      this.search.expiryDate=true;
    }
    this.creationdatemodel='';
}
print(): void {
  let printContents, popupWin;
  printContents = document.getElementById('print-section').innerHTML;
  popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
  popupWin.document.open();
  popupWin.document.write(`
    <html>
      <head>
        <title>Print tab</title>
        <style>
        //........Customized style.......
        </style>
      </head>
  <body onload="window.print();window.close()">${printContents}</body>
    </html>`
  );
  popupWin.document.close();
}
open(content,order) {
  this.router.navigate(['/edit-order'], { queryParams: {order:order} });
}
printOrder;
printOpen(content,order){
  this.modalService.open(content,  { windowClass : "myCustomModalClass"});
  this.printOrder=order;
//   //  // console.log("print order",order);
}username;agentcode;agentname;lastname;
   ngOnInit() {
     this.username=this.unlock.decryptData(localStorage.getItem('username'));
     this.agentname=this.unlock.decryptData(localStorage.getItem('agentname'));
     this.lastname=this.unlock.decryptData(localStorage.getItem('lastname'));
    this.createOrderForm = new FormGroup({
      'orderType': new FormControl( [
        Validators.required,
       
      ]),
      'orderDate': new FormControl( [
        Validators.required,
       
      ]),
    
      'quantity': new FormControl( [
        Validators.required,
       
      ]),
      'price': new FormControl( [
        Validators.required,
       
      ]),  'vat': new FormControl( [
        Validators.required,
       
      ]),  'total': new FormControl( [
        Validators.required,
       
      ]),  'expiryDate': new FormControl( [
        Validators.required,
       
      ]),
      'notifier': new FormControl( [
        Validators.required,
       
      ]),  'neededDate': new FormControl( [
        Validators.required,
       
      ]),
    });
     this.screen1=true;
     this.screen2=false;
     this.searchOrderForm = new FormGroup({
       'orderId': new FormControl( [
         Validators.required,
          
       ]),
       'creationDate': new FormControl( [
        Validators.required,
         
      ]),
      'state': new FormControl( [
        Validators.required,
         
      ]),
      'fromDate': new FormControl( [
        Validators.required,
         
      ]),
      'toDate': new FormControl( [
        Validators.required,
         
      ]),
      'expiryFrom': new FormControl( [
        Validators.required,
         
      ]),
      'expiryTill': new FormControl( [
        Validators.required,
         
      ]),
     });
   }
 screen2Show(mpin){  
    this.screen2=true;
    this.screen1=false;
 }

 cancel(){
   this.screen1=true;
   
   this.screen2=false;
 }
orderIdResult;orderIdResultDesc;orderById=[]
searchOrderId(){
  if(this.creationDate){var orderDate=formatDate(this.creationDate, 'MM/dd/yyyy', 'en')}
  else{this.creationDate='';}
  if(this.fromDate){var fromDate=formatDate(this.fromDate, 'MM/dd/yyyy', 'en')}
  else{fromDate='';}
  if(this.toDate){ var toDate=formatDate(this.toDate, 'MM/dd/yyyy', 'en')}
  else{toDate='';}
  if(this.efDate){var expfromDate=formatDate(this.efDate, 'MM/dd/yyyy', 'en')}
  else{expfromDate='';}
  if(this.etDate){var exptillDate=formatDate(this.etDate, 'MM/dd/yyyy', 'en')}
  else{exptillDate='';}
  var validate=true;

  
  if(this.fromdatemodel!=undefined){if(this.fromdatemodel.getTime() > this.todatemodel.getTime()){ validate=false; }}
 if(this.efdatemodel!=undefined){ if(this.efdatemodel.getTime() > this.etdatemodel.getTime()){ validate=false;}}
 
  if((this.searchOrderForm.orderId!=undefined || this.searchOrderForm.state!=undefined || this.creationDate!='' || (fromDate!='' && toDate!='' && fromDate!=undefined && toDate!=undefined) || (expfromDate!='' && exptillDate!='' && expfromDate!=undefined && exptillDate!=undefined ) || this.searchOrderForm.ordertype!=undefined) && validate==true ){
    this.apiService.searchOrderByOrderIdApi(this.searchOrderForm.orderId,this.searchOrderForm.state,orderDate,fromDate,toDate,expfromDate,exptillDate,this.searchOrderForm.ordertype).subscribe(result=>{ var b:any=result; var resultcode=b.estel.response.resultcode; if(resultcode!=undefined && (resultcode==1956 || resultcode=='1956') ){this.router.navigate(['']);localStorage.clear();}
      this.orderIdResult=result;
    //   //  // console.log("search by order Id",this.orderIdResult);
      this.orderIdResultDesc=this.orderIdResult.estel.response.resultdescription;
      if(this.orderIdResultDesc=='Transaction Successful'){
        this.agentcode=this.orderIdResult.estel.response.agentcode;
        this.recordcount=this.orderIdResult.estel.response.recordcount
        if(this.recordcount==1){
          this.orderById['orderid']=this.orderIdResult.estel.response.records.record.orderid;
          this.orderById['transid']=this.orderIdResult.estel.response.records.record.reftransid;
          this.orderById['item']=this.orderIdResult.estel.response.records.record.itemname;
          this.orderById['amount']=this.orderIdResult.estel.response.records.record.amount
          this.orderById['quantity']=this.orderIdResult.estel.response.records.record.quantity;
          this.orderById['orderdate']=this.orderIdResult.estel.response.records.record.orderdate;
          this.orderById['expirydate']=this.orderIdResult.estel.response.records.record.expirydate;
          this.orderById['totalamount']=this.orderIdResult.estel.response.records.record.totalamount;
          this.orderById['status']=this.orderIdResult.estel.response.records.record.status;
          this.orderById['neededdate']=this.orderIdResult.estel.response.records.record.neededdate;
          this.orderById['date']=this.orderIdResult.estel.response.records.record.date;
          this.orderById['vat']=this.orderIdResult.estel.response.records.record.vat;
          this.orderById['destination']=this.orderIdResult.estel.response.records.record.destination;
          this.orderById['wallettype']=this.orderIdResult.estel.response.records.record.wallettype;
        }
       if(this.recordcount>1){
         this.orders=this.orderIdResult.estel.response.records.record;
       }
        this.screen2=true;
    this.screen1=false;
      }
      else{
        Swal.fire('Operation failed', this.orderIdResultDesc);
      }
    })
  }
  else{
    Swal.fire('Operation failed', 'Please enter atleast 1 search criterion correctly');
  }
}
orders;recordcount;

 public beforeChange($event: NgbTabChangeEvent) {
  this.screen2=false;
  this.screen1=true;
   if ($event.nextId === 'tab-preventchange2') {
     $event.preventDefault();
   }
 }
 close(){
   this.screen1=true;
   this.screen2=false;
   this.searchOrderForm.orderId=undefined;
   this.creationdatemodel=undefined;
   this.searchOrderForm.state=undefined;
   this.searchOrderForm.ordertype=undefined;
   this.fromdatemodel=undefined;
   this.todatemodel=undefined;
   this.efdatemodel=undefined;
   this.etdatemodel=undefined;
   this.efromDate='';
   this.etillDate='';
   this.tDate='';
   this.fDate='';
   this.creationDate=undefined;
   this.fromDate=undefined;
   this.toDate=undefined;
   this.efDate=undefined;
   this.etDate=undefined;
   this.cDate='';
 }
rejectResult;rejectResultDesc;
 rejectOrder(orderId){
this.apiService.rejectOrderApi(orderId).subscribe(result=>{ var b:any=result; var resultcode=b.estel.response.resultcode; if(resultcode!=undefined && (resultcode==1956 || resultcode=='1956') ){this.router.navigate(['']);localStorage.clear();}
//   //  // console.log(result);
  this.rejectResult=result;
  this.rejectResultDesc=this.rejectResult.estel.response.resultdescription;
  if(this.rejectResultDesc=='Transaction Successful'){
    Swal.fire( 'Operation Success','Order Canceled');
    if(this.recordcount==1){
      this.orderById['status']='Canceled';
    }
    if(this.recordcount>1){
      for(var i=0;i<this.orders.length;i++){
        if(this.orders[i].orderid==orderId){
          this.orders[i].status='Canceled';
        }
      }
    }
  }
  else{
    Swal.fire('Operation failed', this.rejectResultDesc);
  }
})
 }
}
