import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor() { }
  images=[];
  ngOnInit() {
    this.images[0]='https://dummyimage.com/1366x600';
    this.images[1]='https://dummyimage.com/1366x600';
    this.images[2]='https://dummyimage.com/1366x600';
  }

}
