import { Routes } from '@angular/router';
import { DashboardComponent } from '../../dashboard/dashboard.component';
import { UserProfileComponent } from '../../user-profile/user-profile.component';
import{AuthGuard} from '../../Services/auth.guard'
import { BalanceComponent } from '../../balance/balance.component';
import { HomeComponent } from '../../home/home.component';
import { ETopUpComponent } from '../../e-top-up/e-top-up.component';
import { HistoryComponent } from '../../history/history.component';
import { ConfigurationComponent } from '../../configuration/configuration.component';
import { TransferComponent } from '../../transfer/transfer.component';
import { ChangeMpinComponent } from '../../change-mpin/change-mpin.component';
import { ChangeLanguageComponent } from '../../change-language/change-language.component';
import { EVoucherComponent } from '../../e-voucher/e-voucher.component';
import { ERechargeComponent } from '../../e-recharge/e-recharge.component';
import { FiscalStampComponent } from '../../fiscal-stamp/fiscal-stamp.component';
import { ValueComponent } from '../../value/value.component';
import { CreateOrderComponent } from '../../create-order/create-order.component';
import { SearchOrderComponent } from '../../search-order/search-order.component';
import{EditOrderComponent} from '../../edit-order/edit-order.component';
import { AgentHierarchyComponent } from '../../agent-hierarchy/agent-hierarchy.component';
export const AdminLayoutRoutes: Routes = [
    { path: 'dashboard',  canActivate: [AuthGuard],    component: DashboardComponent },
    { path: 'user-profile', canActivate: [AuthGuard],  component: UserProfileComponent },
    { path: 'balance',  canActivate: [AuthGuard],      component: BalanceComponent },
    { path: 'home',    canActivate: [AuthGuard],    component: HomeComponent },
    { path: 'e-topup',  canActivate: [AuthGuard],      component: ETopUpComponent },
    { path: 'history',   canActivate: [AuthGuard],     component: HistoryComponent },
    { path: 'configuration',  canActivate: [AuthGuard],      component: ConfigurationComponent },
    { path: 'transfer',    canActivate: [AuthGuard],    component: TransferComponent },
    { path: 'changeMpin',   canActivate: [AuthGuard],     component: ChangeMpinComponent },
    { path: 'changeLanguage',  canActivate: [AuthGuard],      component: ChangeLanguageComponent },
    { path: 'e-recharge',  canActivate: [AuthGuard],      component: ERechargeComponent },
    { path: 'e-voucher',   canActivate: [AuthGuard],     component: EVoucherComponent }, 
    { path: 'fiscal',   canActivate: [AuthGuard],     component: FiscalStampComponent }, 
    { path: 'value',  canActivate: [AuthGuard],      component: ValueComponent }, 
    { path: 'create-order',   canActivate: [AuthGuard],     component: CreateOrderComponent }, 
    { path: 'search-order',  canActivate: [AuthGuard],      component: SearchOrderComponent }, 
    { path: 'edit-order',    canActivate: [AuthGuard],    component: EditOrderComponent }, 
    { path: 'agent-hierarchy',   canActivate: [AuthGuard],     component: AgentHierarchyComponent }, 
];
