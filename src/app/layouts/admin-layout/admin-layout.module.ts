import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminLayoutRoutes } from './admin-layout.routing';
import { DashboardComponent } from '../../dashboard/dashboard.component';
import { UserProfileComponent } from '../../user-profile/user-profile.component';
import { ChartsModule } from 'ng2-charts';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ToastrModule } from 'ngx-toastr';
import { ETopUpComponent } from '../../e-top-up/e-top-up.component';
import { BalanceComponent } from '../../balance/balance.component';
import { TransferComponent } from '../../transfer/transfer.component';
import { HistoryComponent } from '../../history/history.component';
import { ConfigurationComponent } from '../../configuration/configuration.component';
import { ChangeMpinComponent } from '../../change-mpin/change-mpin.component';
import { ChangeLanguageComponent } from '../../change-language/change-language.component';
import { HomeComponent } from '../../home/home.component';
import { EVoucherComponent } from '../../e-voucher/e-voucher.component';
import { ERechargeComponent } from '../../e-recharge/e-recharge.component';
import { FiscalStampComponent } from '../../fiscal-stamp/fiscal-stamp.component';
import { ValueComponent } from '../../value/value.component';
import { CreateOrderComponent } from '../../create-order/create-order.component';
import { SearchOrderComponent } from '../../search-order/search-order.component';
import { EditOrderComponent } from '../../edit-order/edit-order.component';
import { AgentHierarchyComponent } from '../../agent-hierarchy/agent-hierarchy.component';
import { NgSelect2Module } from 'ng-select2';
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    ChartsModule,
    NgbModule,
    NgSelect2Module,
    ToastrModule.forRoot()
  ],
  declarations: [
    DashboardComponent,
    UserProfileComponent,
    ETopUpComponent,
    BalanceComponent,
    TransferComponent,
    ChangeMpinComponent,
    ChangeLanguageComponent,
    HistoryComponent,
    ConfigurationComponent,
    HomeComponent,
    AgentHierarchyComponent,
    EVoucherComponent,
    ERechargeComponent,
   
    FiscalStampComponent,
    ValueComponent,
    CreateOrderComponent,
    SearchOrderComponent,
    EditOrderComponent,
  ]
})

export class AdminLayoutModule {}
