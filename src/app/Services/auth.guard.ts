import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { UserIdleService } from 'angular-user-idle';

import{AppConfig} from '../app.config';
import * as CryptoJS from 'crypto-js';
@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private userIdle: UserIdleService,private router: Router) { }
    encryptSecretKey=AppConfig.localkey;
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        this.userIdle.resetTimer();
        // this.userIdle.startWatching();
    
      
        // this.userIdle.onTimerStart().subscribe(count => console.log(count));
        //   // console.log('hi');
      
        // this.userIdle.onTimeout().subscribe(result =>{
        //  var agentcode=this.unlock.decryptData(localStorage.getItem('currentUser'));
        //    // console.log(agentcode);
        //  if(agentcode)
        //   this.apiService.logOutApi().subscribe(result =>{
          
        //    localStorage.clear();
        //    this.router.navigate(['']);
        //   })
        // } 
        
        //);
        //   // console.log(this.encryptData('agentcode'));
        //   // console.log(this.decryptData(this.encryptData('agentcode')));
        if (localStorage.getItem('agentname')) {
            // logged in so return true
            return true;
        }

        // not logged in so redirect to login page with the return url
        this.router.navigate(['/login'], { queryParams: { returnUrl: state.url }});
        return false;
       
    }

    encryptData(data) {
     
        try {
          return CryptoJS.AES.encrypt(JSON.stringify(data), this.encryptSecretKey).toString();
        } catch (e) {
          
        }
      }
    
      decryptData(data) {
    
        try {
          const bytes = CryptoJS.AES.decrypt(data, this.encryptSecretKey);
          if (bytes.toString()) {
            return JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
          }
          return data;
        } catch (e) {
          
        }
      }
}