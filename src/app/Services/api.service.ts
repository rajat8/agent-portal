import { Injectable } from '@angular/core';
import{AppConfig} from '../app.config';
import {Router} from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import Swal from 'sweetalert2';
import { sha256, sha224 } from 'js-sha256';
import * as CryptoJS from 'crypto-js';
import {AuthGuard } from './auth.guard';
const httpOptions = {
  headers: new HttpHeaders({
    "Content-Type":"application/json",
 
   "auth.security.enable":"true",
   "authorization":"2DEF34A67B7C82850E2F531A0168375E4CF5977E6C1984553ECBA9869E379BA1"
  })
};
@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private unlock:AuthGuard,private router: Router,private http: HttpClient) { }
private balanceRequest;
private historyRequest;
agentcode=this.unlock.decryptData(localStorage.getItem('currentUser'));
    tokenFromUI: string = "1234567890123456";
  encrypted: any = "";
  decrypted: string;

  request: string;
  responce: string;
  encryptData(dataToEncrypt) {
   
   if((dataToEncrypt!=undefined && dataToEncrypt!='') || dataToEncrypt===0 || dataToEncrypt===false){
      dataToEncrypt=dataToEncrypt.toString();
    var key = CryptoJS.enc.Utf8.parse(window.atob("MTIzNDU2Nzg5MDEyMzQ1Ng=="));
    // var key = "1234567890123456";
    var encrypted = CryptoJS.AES.encrypt(dataToEncrypt, key
    , {
      mode: CryptoJS.mode.ECB,
      padding: CryptoJS.pad.Pkcs7
    }
    );
   
    var enc=CryptoJS.enc.Hex.stringify(encrypted.ciphertext);

    return enc.toUpperCase();
   }
   else{
      return '';
   }
    
  }

  balanceApi(mpin){
  
   if(mpin!=undefined && mpin!='')
   { var encPin=mpin.toString();
  //   //  // console.log("encPin",sha256(this.unlock.decryptData(localStorage.getItem('currentUser'))+encPin));
  //  // console.log(sha512('hello'));
    this.balanceRequest={"header":{"requesttype":"BALANCE"},"request":{
      "sessionid":this.encryptData(this.unlock.decryptData(localStorage.getItem('sessionid'))), 
      "agentcode":this.encryptData(this.unlock.decryptData(localStorage.getItem('currentUser'))),"pin":this.encryptData(sha256(this.unlock.decryptData(localStorage.getItem('currentUser'))+encPin)),"destination":this.encryptData(this.unlock.decryptData(localStorage.getItem('currentUser'))),"vendorcode":this.encryptData("ALFA"),"clienttype":"SELFCARE","version":this.encryptData("2.5"),"sign":this.encryptData("4n5MPDjukoJqotK5YbL5ExO2gvw")}};
 //   //  // console.log("balance request",this.balanceRequest);
    return this.http.post('/services/alfaRestService',this.balanceRequest,httpOptions)}
    
  }

  historyApi(mpin){
     // console.log("mPin2121212",mpin);
    if(mpin!=undefined && mpin!='')
    { var encPin=mpin.toString();
       // console.log("encPin",sha256(this.unlock.decryptData(localStorage.getItem('currentUser'))+encPin));
       // console.log("mPin",mpin);
     this.historyRequest={"header":{"requesttype":"TRANSSTATUS"},"request":{
        
      "sessionid":this.encryptData(this.unlock.decryptData(localStorage.getItem('sessionid'))),
      "agentcode":this.encryptData(this.unlock.decryptData(localStorage.getItem('currentUser'))),
     "pin":this.encryptData(sha256(this.unlock.decryptData(localStorage.getItem('currentUser'))+encPin)),
     "vpin":"",
     "vendorcode":this.encryptData("ALFA"),
     "rtransid":[],
     "startdate":[],
     "enddate":[],
     "clienttype":"SELFCARE",
     "version":this.encryptData("2.5"),
     "sign":this.encryptData("4n5MPDjukoJqotK5YbL5ExO2gvw")}};
  //   //  // console.log("history request",this.historyRequest);
     return this.http.post('/services/alfaRestService',this.historyRequest,httpOptions)}
  }

  changeMpinApi(pin,newpin,pintype){
     // console.log(pin,newpin);
    if(pin!=undefined && pin!='' && newpin!=undefined && newpin!='')
    {
       if(pintype=='IPIN'){
          var encPin=this.unlock.decryptData(localStorage.getItem('username'))+pin.toString();
      var newEncPin=this.unlock.decryptData(localStorage.getItem('username'))+newpin.toString(); 
       }
       if(pintype=='TPIN'){
           var encPin=this.unlock.decryptData(localStorage.getItem('currentUser'))+pin.toString();
      var newEncPin=this.unlock.decryptData(localStorage.getItem('currentUser'))+newpin.toString(); 
       }
      
      this.changeMpinRequest={"header":{"requesttype":"CHANGEPIN"},"request":{
         
         "sessionid":this.encryptData(this.unlock.decryptData(localStorage.getItem('sessionid'))),
         "agentcode":this.encryptData(this.unlock.decryptData(localStorage.getItem('currentUser'))),
         "pin":this.encryptData(sha256(encPin)),
         "source":this.encryptData(this.unlock.decryptData(localStorage.getItem('currentUser'))),
         "newpin":this.encryptData(sha256(newEncPin)),
      "pintype":this.encryptData(pintype),
      "vendorcode":this.encryptData("ALFA"),
      "comments":[],
      "clienttype":"SELFCARE",
      "version":this.encryptData("2.5"),
      "sign":this.encryptData("4n5MPDjukoJqotK5YbL5ExO2gvw"),
      }}
      return this.http.post('/services/alfaRestService',this.changeMpinRequest,httpOptions)
    }
  }
  changeMpinRequest;
changeLanguageRequest;
  changeLanguageApi(pin,newLanguage){
    if(pin!=undefined && pin!=''){
      var encPin=pin.toString();
      this.changeLanguageRequest={
        "header": {
           "requesttype": "CHANGELANG"
        },
        "request": {
           
         "sessionid":this.encryptData(this.unlock.decryptData(localStorage.getItem('sessionid'))),
         "agentcode": this.unlock.decryptData(localStorage.getItem('currentUser')),
           "pin": sha256(this.unlock.decryptData(localStorage.getItem('currentUser'))+encPin),
           "vendorcode": this.encryptData("ALFA"),
           "requestcts": this.encryptData(new Date()),
           "newlanguage": newLanguage,
           "clienttype": "SELFCARE"
        }
     }
     return this.http.post('/services/alfaRestService',this.changeLanguageRequest,httpOptions);
    }
  }
transferRequest;
  transferApi(pin,destination,amount,wallettype){
    if(pin!=undefined){
      var encPin=pin.toString();
      this.transferRequest={
        "header": {
           "requesttype": "MOVESTOCK"
        },
        "request": {
           
         "sessionid":this.encryptData(this.unlock.decryptData(localStorage.getItem('sessionid'))),
         "agentcode": this.encryptData(this.unlock.decryptData(localStorage.getItem('currentUser'))),
           "pin": this.encryptData(sha256(this.unlock.decryptData(localStorage.getItem('currentUser'))+encPin)),
           "clienttype": "SELFCARE",
           "destination": this.encryptData(destination),
           "amount": this.encryptData(amount),
           "version":this.encryptData("2.5"),
           "sign":this.encryptData("4n5MPDjukoJqotK5YbL5ExO2gvw"),
           "wallettype": this.encryptData(wallettype),
           "vendorcode": this.encryptData("ALFA")
        }
     }
   //   //  // console.log("transfer req",this.transferRequest);
     return this.http.post('/services/alfaRestService',this.transferRequest,httpOptions);
   
    }
  }
getItemRequest;
  getItemApi(){
this.getItemRequest={
  "header": {
     "requesttype": "GETITEM"
  },
  "request": {
     
   "sessionid":this.encryptData(this.unlock.decryptData(localStorage.getItem('sessionid'))),
   "agentcode": this.encryptData(this.unlock.decryptData(localStorage.getItem('currentUser'))),
     "pin": " ",
     "vendorcode": this.encryptData("ALFA"),
     "requestcts": this.encryptData(new Date()),
     "clienttype": "SELFCARE"
  }
}
return this.http.post('/services/alfaRestService',this.getItemRequest,httpOptions);
}
createOrderRequest;
createOrderApi(amount,walletType,itemCode,quantity,vat,totalAmount,orderDate,expiryDate,neededDate,notifier){
   
   walletType=walletType.toString();
  this.createOrderRequest={
    "header": {
       "requesttype": "ORDER"
    },
    "request": {
       
      "sessionid":this.encryptData(this.unlock.decryptData(localStorage.getItem('sessionid'))),
      "agentcode":this.encryptData(this.unlock.decryptData(localStorage.getItem('currentUser'))),
       "pin": " ",
       "vendorcode": this.encryptData("ALFA"),
       "amount": this.encryptData(amount.toString()),
       "clienttype": "SELFCARE",
       "wallettype": this.encryptData(walletType),
       "orderdate": this.encryptData(orderDate),
       "item": this.encryptData(itemCode),
       "quantity": this.encryptData(quantity.toString()),
       "vat": this.encryptData(vat.toString()),
       "totalamount": this.encryptData(totalAmount.toString()),
       "expirydate": this.encryptData(expiryDate),
       "destination": this.encryptData(notifier),
       "neededdate": this.encryptData(neededDate),
       "status":" ",
       "version": this.encryptData("2.5"),
        "sign": this.encryptData("4n5MPDjukoJqotK5YbL5ExO2gvw"),
        "source":this.encryptData(this.unlock.decryptData(localStorage.getItem('currentUser')))
    }
 }
 //  // console.log("create order request",this.createOrderRequest);
 return this.http.post('/services/alfaRestService',this.createOrderRequest,httpOptions);
}
 orderIdRequest;
searchOrderByOrderIdApi(orderId,status,creationdate,orderstartdate,orderenddate,expirystart,expiryend,wallettype){
  this.orderIdRequest={
    
      "header": {
         "requesttype": "SEARCHORDER"
      },
      "request": {
         
         "sessionid":this.encryptData(this.unlock.decryptData(localStorage.getItem('sessionid'))),
         "agentcode": this.encryptData(this.unlock.decryptData(localStorage.getItem('currentUser'))),
         "pin": this.encryptData(this.unlock.decryptData(localStorage.getItem('currentUser'))),
         "vendorcode": this.encryptData("ALFA"),
         "amount": " ",
         "clienttype": "SELFCARE",
         "version": this.encryptData("2.5"),
         "sign": this.encryptData("4n5MPDjukoJqotK5YbL5ExO2gvw"),
         "orderid": this.encryptData(orderId),
         "status":this.encryptData(status),
         "expirydate":this.encryptData(expirystart),
         "expiryenddate":this.encryptData(expiryend),
         "createddate":this.encryptData(creationdate),
         "neededdate":this.encryptData(orderstartdate),
         "orderdate":this.encryptData(orderenddate),
         "wallettype":this.encryptData(wallettype)
      }
   
  }

  return this.http.post('/services/alfaRestService',this.orderIdRequest,httpOptions);
}
billerRequest;
billerApi(){
   var agentcode=this.unlock.decryptData(localStorage.getItem('currentUser'));
 var vpin=agentcode.toString().substring(3,5)+'BILLER';
 var v=sha256(vpin);
  this.billerRequest={
    "header": {
       "requesttype": "BILLER"
    },
    "request": {
       
      "sessionid":this.encryptData(this.unlock.decryptData(localStorage.getItem('sessionid'))),
      "agentcode": this.encryptData(this.unlock.decryptData(localStorage.getItem('currentUser'))),
       "servicetype": this.encryptData("TOPUP"),
       "vendorcode": this.encryptData("ALFA"),
       "clienttype": "SELFCARE",
       "vpin": this.encryptData(v.toUpperCase( )),
       "version": this.encryptData("2.5"),
       "sign": this.encryptData("4n5MPDjukoJqotK5YbL5ExO2gvw")
    }
 }
 //  // console.log("biller req",this.billerRequest);
 //  // console.log('vpin',v.toUpperCase( ));
 return this.http.post('/services/alfaRestService',this.billerRequest,httpOptions);
}

billerVoucherRequest;
billerVoucherApi(){
   var agentcode=this.unlock.decryptData(localStorage.getItem('currentUser'));
 var vpin=agentcode.toString().substring(3,5)+'BILLER';
 var v=sha256(vpin);
  this.billerVoucherRequest={
    "header": {
       "requesttype": "BILLER"
    },
    "request": {
       
      "sessionid":this.encryptData(this.unlock.decryptData(localStorage.getItem('sessionid'))),
      "agentcode": this.encryptData(this.unlock.decryptData(localStorage.getItem('currentUser'))),
       "servicetype":this.encryptData("BULKPINPURCHASE"),
       "vendorcode": this.encryptData("ALFA"),
       "clienttype": "SELFCARE",
       "vpin": this.encryptData(v.toUpperCase( )),
       "version": this.encryptData("2.5"),
       "sign": this.encryptData("4n5MPDjukoJqotK5YbL5ExO2gvw")
    }
 }
 //  // console.log("biller Voucher req",this.billerVoucherRequest);
 //  // console.log('vpin',v.toUpperCase( ));
 return this.http.post('/services/alfaRestService',this.billerVoucherRequest,httpOptions);
}
prodDetailsRequest;
prodDetailsApi(operator){
   var agentcode=this.unlock.decryptData(localStorage.getItem('currentUser'));
   var vpin=agentcode.toString().substring(3,5)+'PRODDETAILS';
   var v=sha256(vpin);
  this.prodDetailsRequest={
     
    "header": {
       "requesttype": "PRODDETAILS"
    },
    "request": {
       
      "sessionid":this.encryptData(this.unlock.decryptData(localStorage.getItem('sessionid'))),
      "agentcode": this.encryptData(this.unlock.decryptData(localStorage.getItem('currentUser'))),
      "vpin": this.encryptData(v.toUpperCase( )),
       "vendorcode": this.encryptData("ALFA"),
       "clienttype": "SELFCARE",
       "operator": this.encryptData(operator),
       "productcode": this.encryptData("TOPUP"),
       "version":this.encryptData("2.5"),
       "sign":this.encryptData("4n5MPDjukoJqotK5YbL5ExO2gvw")
    }
 }
 //  // console.log("prod req",this.prodDetailsRequest);
 return this.http.post('/services/alfaRestService',this.prodDetailsRequest,httpOptions);
}


prodDetailsVoucherRequest;
prodDetailsVoucherApi(operator){
   var agentcode=this.unlock.decryptData(localStorage.getItem('currentUser'));
   var vpin=agentcode.toString().substring(3,5)+'PRODDETAILS';
   var v=sha256(vpin);
  this.prodDetailsVoucherRequest={
     
    "header": {
       "requesttype": "PRODDETAILS"
    },
    "request": {
       
      "sessionid":this.encryptData(this.unlock.decryptData(localStorage.getItem('sessionid'))),
      "agentcode": this.encryptData(this.unlock.decryptData(localStorage.getItem('currentUser'))),
      "vpin": this.encryptData(v.toUpperCase( )),
       "vendorcode": this.encryptData("ALFA"),
       "clienttype": "SELFCARE",
       "operator": this.encryptData(operator),
       "productcode": this.encryptData("BULKPINPURCHASE"),
       "version":this.encryptData("2.5"),
       "sign":this.encryptData("4n5MPDjukoJqotK5YbL5ExO2gvw")
    }
 }
 //  // console.log("prod voucher req",this.prodDetailsVoucherRequest);
 return this.http.post('/services/alfaRestService',this.prodDetailsVoucherRequest,httpOptions);
}
eRechargeRequest;
eRechargeApi(amount,operator,msisdn,productcode){
  this.eRechargeRequest={
    "header": {
       "requesttype": "TOPUP"
    },
    "request": {
       
      "sessionid":this.encryptData(this.unlock.decryptData(localStorage.getItem('sessionid'))),
      "agentcode": this.encryptData(this.unlock.decryptData(localStorage.getItem('currentUser'))),
       "destination": this.encryptData(msisdn),
       "productcode": this.encryptData(productcode),
       "operator": this.encryptData(operator),
       "amount": this.encryptData(amount),
       "clienttype": "SELFCARE",
       "vendorcode": this.encryptData("ALFA"),
        "version":this.encryptData("2.5"),
       "sign":this.encryptData("4n5MPDjukoJqotK5YbL5ExO2gvw")
    }
 }
 //  // console.log("prod req",this.eRechargeRequest);
 return this.http.post('/services/alfaRestService',this.eRechargeRequest,httpOptions);
}

getOtpRequest;
getOtpApi(agentcode){
  this.getOtpRequest={
   "header": {
      "requesttype": "OTP"
   },
   "request": {
      
    
      "agentcode": this.encryptData(agentcode),
      "vendorcode": this.encryptData("ALFA"),
      "requestcts": this.encryptData(new Date()),
      "sign": this.encryptData("4n5MPDjukoJqotK5YbL5ExO2gvw"),
      "version": this.encryptData("2.5"),
      "clienttype": "SELFCARE"
      
   }
}
 //  // console.log("prod req",this.getOtpRequest);
 return this.http.post('/services/alfaRestService',this.getOtpRequest,httpOptions);
}

validateOtpRequest;
validateOtpApi(agentcode,otp){
  this.validateOtpRequest={
   "header": {
   "requesttype": "VALIDATEOTP"
   },
   "request": {
   
   
      "agentcode": this.encryptData(agentcode),
   "vendorcode": this.encryptData("ALFA"),
   "requestcts": this.encryptData(new Date()),
   "sign": this.encryptData("4n5MPDjukoJqotK5YbL5ExO2gvw"),
   "version": this.encryptData("2.5"),
   "clienttype": "SELFCARE",
   "otp": this.encryptData(otp)
   }
   }
 //  // console.log("prod req",this.validateOtpRequest);
 return this.http.post('/services/alfaRestService',this.validateOtpRequest,httpOptions);
}

forgotPwdRequest;
forgotPwdApi(agentcode,pin,pintype,username){
   var encPin=pin.toString();
 //   //  // console.log(encPin);
  this.forgotPwdRequest={
   "header": {
      "requesttype": "FORGOTPWD"
   },
   "request": {
      
  
      "agentcode": this.encryptData(agentcode),
      "vendorcode": this.encryptData("ALFA"),
      "requestcts": this.encryptData(new Date()),
      "sign": this.encryptData("4n5MPDjukoJqotK5YbL5ExO2gvw"),
      "version": this.encryptData("2.5"),
      "clienttype": "SELFCARE",
      "pintype":this.encryptData(pintype),
      "pin": this.encryptData(sha256(username+encPin))
   }
}
 //  // console.log("prod req",this.forgotPwdRequest);
 return this.http.post('/services/alfaRestService',this.forgotPwdRequest,httpOptions);}
updateOrderRequest;
updateOrderApi(orderid,amount,item,quantity,vat,total,expiry,needed,notifier,wallettype){
   
   wallettype=wallettype.toString();
  
  this.updateOrderRequest={
    "header": {
      "requesttype": "UPDATEORDER"
   },
   "request": {
      
      "sessionid":this.encryptData(this.unlock.decryptData(localStorage.getItem('sessionid'))),
      "agentcode":this.encryptData(this.unlock.decryptData(localStorage.getItem('currentUser'))),
      
      "vendorcode": this.encryptData("ALFA"),
      "orderid": this.encryptData(orderid),
      "amount": this.encryptData(amount),
      "clienttype": "SELFCARE",
      "item": this.encryptData(item),
      "quantity": this.encryptData(quantity),
      "vat": this.encryptData(vat),
      "totalamount": this.encryptData(total),
      "expirydate": this.encryptData(expiry),
      "neededdate": this.encryptData(needed),
      "destination": this.encryptData(notifier),
      "wallettype":this.encryptData(wallettype)
   }
 }
//    // console.log(needed);
 return this.http.post('/services/alfaRestService',this.updateOrderRequest,httpOptions);
}

agentDetailsRequest;
agentDetailsApi(destination,pin){
   var encPin=pin.toString();
  this.agentDetailsRequest={
   "header": {
      "requesttype": "AGENTDETAILS"
   },
   "request": {
      "sign": this.encryptData("4n5MPDjukoJqotK5YbL5ExO2gvw"),
      "vendorcode": this.encryptData("ALFA"),
      
      "sessionid":this.encryptData(this.unlock.decryptData(localStorage.getItem('sessionid'))),
      "agentcode":this.encryptData(this.unlock.decryptData(localStorage.getItem('currentUser'))),
      "clienttype": "SELFCARE",
      "pin": this.encryptData(sha256(this.unlock.decryptData(localStorage.getItem('currentUser'))+encPin)),
      "version": this.encryptData("2.5"),
      "destination": this.encryptData(destination)
   }
}
 //  // console.log("prod req",this.agentDetailsRequest);
 return this.http.post('/services/alfaRestService',this.agentDetailsRequest,httpOptions);
}

evoucherRequest;
evoucherApi(pin,productcode,amount,msisdn,quantity){
   var encPin=pin.toString();
  this.evoucherRequest={
   "header": {
      "requesttype": "BULKPINPURCHASE"
   },
   "request": {
      
      "sessionid":this.encryptData(this.unlock.decryptData(localStorage.getItem('sessionid'))),
      "agentcode":this.encryptData(this.unlock.decryptData(localStorage.getItem('currentUser'))),
      "pin": this.encryptData(sha256(this.unlock.decryptData(localStorage.getItem('currentUser'))+encPin)),
      "vendorcode": this.encryptData("ALFA"),
      "productcode": this.encryptData(productcode),
      "clienttype": "SELFCARE",
      "sign": this.encryptData("4n5MPDjukoJqotK5YbL5ExO2gvw"),
      "version": this.encryptData("2.5"),
      "denomination": this.encryptData(amount),
      "destination": this.encryptData(msisdn),
      "qty": this.encryptData(quantity)
   }
}
 //  // console.log("evouch req",this.evoucherRequest);
 return this.http.post('/services/alfaRestService',this.evoucherRequest,httpOptions);
}

forcechangeMpinApi(pin,newpin){
   if(pin!=undefined && pin!='' && newpin!=undefined && newpin!='')
   {
        // console.log(this.unlock.decryptData(localStorage.getItem('username')));
     var encPin=this.unlock.decryptData(localStorage.getItem('username'))+pin.toString();
     var newEncPin=this.unlock.decryptData(localStorage.getItem('username'))+newpin.toString(); 
     this.forcechangeMpinRequest={"header":{"requesttype":"FORCECHANGEPIN"},"request":{
        
     
      "agentcode":this.encryptData(this.unlock.decryptData(localStorage.getItem('currentUser'))),
     "pin":this.encryptData(sha256(encPin)),
     "source":this.encryptData(this.unlock.decryptData(localStorage.getItem('currentUser'))),
     "newpin":this.encryptData(sha256(newEncPin)),
     "vendorcode":this.encryptData("ALFA"),"clienttype":"SELFCARE",
     "version":this.encryptData("2.5"),"sign":this.encryptData("4n5MPDjukoJqotK5YbL5ExO2gvw"),
     
     "pintype":this.encryptData("IPIN")}}
     return this.http.post('/services/alfaRestService',this.forcechangeMpinRequest,httpOptions)
   }
 }
 forcechangeMpinRequest;

 rejectOrderApi(orderId){
   
   {
     this.rejectOrderRequest={
      "header": {
         "requesttype": "ORDERUPDATE"
      },
      "request": {
         
         "sessionid":this.encryptData(this.unlock.decryptData(localStorage.getItem('sessionid'))),
         "agentcode":this.encryptData(this.unlock.decryptData(localStorage.getItem('currentUser'))),
         "orderid": this.encryptData(orderId),
         "updateflag": this.encryptData("CANCEL"),
         "vendorcode": this.encryptData("ALFA"),
         "clienttype": "SELFCARE",
      }
   }
 //   //  // console.log(this.rejectOrderRequest);
     return this.http.post('/services/alfaRestService',this.rejectOrderRequest,httpOptions)
   }
 }
 rejectOrderRequest;

 viewWalletApi(){
   
   {
     this.viewWalletRequest={
      "header": {
         "requesttype": "ORDERBALANCE"
      },
      "request": {
         
         "sessionid":this.encryptData(this.unlock.decryptData(localStorage.getItem('sessionid'))),
         "agentcode":this.encryptData(this.unlock.decryptData(localStorage.getItem('currentUser'))),
         "mobileno": this.encryptData(this.unlock.decryptData(localStorage.getItem('currentUser'))),
         "vendorcode": this.encryptData("ALFA"),
           "clienttype": "SELFCARE",
      }
   }
 //   //  // console.log(this.viewWalletRequest);
     return this.http.post('/services/alfaRestService',this.viewWalletRequest,httpOptions)
   }
 }
 viewWalletRequest; 

 searchWalletApi(msisdn,date){
   
   {
     this.searchWalletRequest={
      "header": {
         "requesttype": "ORDERBALANCE"
      },
      "request": {
         
         "sessionid":this.encryptData(this.unlock.decryptData(localStorage.getItem('sessionid'))),
         "agentcode":this.encryptData(this.unlock.decryptData(localStorage.getItem('currentUser'))),
         "mobileno": this.encryptData(this.unlock.decryptData(localStorage.getItem('currentUser'))),
         "source": this.encryptData(msisdn),
         "vendorcode": this.encryptData("ALFA"),
         "date": this.encryptData(date),
           "clienttype": "SELFCARE",
      }
   }
 //   //  // console.log(this.searchWalletRequest);
     return this.http.post('/services/alfaRestService',this.searchWalletRequest,httpOptions)
   }
 }
 searchWalletRequest; 

 transStatusApi(mobileno,pin){
   
   {
      var agentcode=this.unlock.decryptData(localStorage.getItem('currentUser'));
 var vpin=agentcode.substring(3,5)+'TRANSSTATUS';
 var v=sha256(vpin);
 var encPin=pin.toString();
     this.transStatusRequest={
      "header": {
         "requesttype": "TRANSSTATUS"
      },
      "request": {
         "sign": this.encryptData("4n5MPDjukoJqotK5YbL5ExO2gvw"),
         "vendorcode": this.encryptData("ALFA"),
         
         "sessionid":this.encryptData(this.unlock.decryptData(localStorage.getItem('sessionid'))),
         "agentcode": this.encryptData(this.unlock.decryptData(localStorage.getItem('currentUser'))),
         "clienttype": "SELFCARE",
         "pin": this.encryptData(sha256(this.unlock.decryptData(localStorage.getItem('currentUser'))+encPin)),
         "vpin": this.encryptData(v.toUpperCase()),
         "version": this.encryptData("2.5"),
         "mobileno": this.encryptData(mobileno)
      }
   }
 //   //  // console.log(this.transStatusRequest);
     return this.http.post('/services/alfaRestService',this.transStatusRequest,httpOptions)
   }
 }
 transStatusRequest; 

 agentHierarchyApi(pin){
   
   {
      var agentcode=this.unlock.decryptData(localStorage.getItem('currentUser'));
 var encPin=pin.toString();
     this.agentHierarchyRequest={
      "header": {
         "requesttype": "HIERARCHYVIEW"
      },
      "request": {
         "vendorcode": this.encryptData("ALFA"),
         
         "sessionid":this.encryptData(this.unlock.decryptData(localStorage.getItem('sessionid'))),
         "agentcode": this.encryptData(agentcode),
         "clienttype": "SELFCARE",
         "pin": this.encryptData(sha256(this.unlock.decryptData(localStorage.getItem('currentUser'))+encPin)),
         "pintype": this.encryptData("IPIN")
      }
   }
 //   //  // console.log(this.agentHierarchyRequest);
     return this.http.post('/services/alfaRestService',this.agentHierarchyRequest,httpOptions)
   }
 }
 agentHierarchyRequest; 

 logOutApi(){
   
   
      var agentcode=this.unlock.decryptData(localStorage.getItem('currentUser'));
  
     this.logOutRequest={
      "header": {
         "requesttype": "LOGOUT"
      },
      "request": {
         
        
         "agentcode": this.encryptData(agentcode),
          "vendorcode": this.encryptData("ALFA"),
         "comments": [],
         "clienttype": "SELFCARE",
         "version": this.encryptData("2.5"),
         "sign": this.encryptData("4n5MPDjukoJqotK5YbL5ExO2gvw"),
         "sessionid":this.encryptData(this.unlock.decryptData(localStorage.getItem('sessionid')))
      }
   }
 //   //  // console.log(this.logOutRequest);
 
     return this.http.post('/services/alfaRestService',this.logOutRequest,httpOptions)
   
 }
 logOutRequest;

 transf1Api(mpin,username,status,type,startdate,enddate){
   
   
   var agentcode=this.unlock.decryptData(localStorage.getItem('currentUser'));

  this.transf1Request={
   
      "header": {
         "requesttype": "TRANSSTATUS"
      },
      "request": {
         
         "sessionid":this.encryptData(this.unlock.decryptData(localStorage.getItem('sessionid'))),
         "agentcode": this.encryptData(agentcode),
         "clienttype": "SELFCARE",
         "pin": this.encryptData(sha256(this.unlock.decryptData(localStorage.getItem('currentUser'))+mpin)),
         
         "sign": this.encryptData("4n5MPDjukoJqotK5YbL5ExO2gvw"),
         "vendorcode": this.encryptData("ALFA"),
         "version": this.encryptData("2.5"),
         "username": this.encryptData(username),
         "transstatus": this.encryptData(status),
         "transtype": this.encryptData(type),
         "startdate": this.encryptData(startdate),
         "enddate": this.encryptData(enddate)
      }
   
}
//   //  // console.log(this.transf1Request);

  return this.http.post('/services/alfaRestService',this.transf1Request,httpOptions)

}
transf1Request;

transf2Api(mpin,transid){
   
   
   var agentcode=this.unlock.decryptData(localStorage.getItem('currentUser'));

  this.transf2Request={
   "header": {
      "requesttype": "TRANSSTATUS"
   },
   "request": {
      
      "sessionid":this.encryptData(this.unlock.decryptData(localStorage.getItem('sessionid'))),
      "agentcode": this.encryptData(agentcode),
      "clienttype": "SELFCARE",
      "pin": this.encryptData(sha256(this.unlock.decryptData(localStorage.getItem('currentUser'))+mpin)),
      
      "sign": this.encryptData("4n5MPDjukoJqotK5YbL5ExO2gvw"),
      "vendorcode": this.encryptData("ALFA"),
      "version": this.encryptData("2.5"),
      "rtransid": this.encryptData(transid)
   }
}
//   //  // console.log(this.transf2Request);

  return this.http.post('/services/alfaRestService',this.transf2Request,httpOptions)

}
transf2Request;

transf3Api(mpin,mobile){
   
   
   var agentcode=this.unlock.decryptData(localStorage.getItem('currentUser'));

  this.transf3Request={
   "header": {
      "requesttype": "TRANSSTATUS"
   },
   "request": {
      
      "sessionid":this.encryptData(this.unlock.decryptData(localStorage.getItem('sessionid'))),
      "agentcode": this.encryptData(agentcode),
      "clienttype": "SELFCARE",
      "pin": this.encryptData(sha256(this.unlock.decryptData(localStorage.getItem('currentUser'))+mpin)),
      
      "sign": this.encryptData("4n5MPDjukoJqotK5YbL5ExO2gvw"),
      "vendorcode": this.encryptData("ALFA"),
      "version": this.encryptData("2.5"),
      "referencecode": this.encryptData(mobile)
   }
}
//   //  // console.log(this.transf3Request);

  return this.http.post('/services/alfaRestService',this.transf3Request,httpOptions)

}
transf3Request;


   downloadFile(data, filename='data') {
      //   // console.log('this is data csv',data)
        let csvData = this.ConvertToCSV(data, ['Transaction Id','Date', 'Sender Profile', 'Destination', 'Channel', 'Type', 'Sender','Transfer/Order Type','Adjustment Type', 'Result Description', 'Status','Amount without vat','Vat','Amount with vat','Fiscal Stamp','Quantity','Expiry Date','Total amount','Sender pre balance','Sender post balance','Sender pre fiscal balance','Sender post fiscal balance','Receiver pre balance','Receiver post balance']);
      //    // console.log(csvData)
        let blob = new Blob(['\ufeff' + csvData], { type: 'text/csv;charset=utf-8;' });
        let dwldLink = document.createElement("a");
        let url = URL.createObjectURL(blob);
        let isSafariBrowser = navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1;
        if (isSafariBrowser) {  //if Safari open in new window to save file with random filename.
            dwldLink.setAttribute("target", "_blank");
        }
        dwldLink.setAttribute("href", url);
        dwldLink.setAttribute("download", filename + ".csv");
        dwldLink.style.visibility = "hidden";
        document.body.appendChild(dwldLink);
        dwldLink.click();
        document.body.removeChild(dwldLink);
    }

    ConvertToCSV(objArray, headerList) {
       if(objArray.length>1){

     //      // console.log('hello',objArray);
       let array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
       let str = '';
       let row = 'S.No,';
   //   // console.log('hello1',array);
       for (let index in headerList) {
           row += headerList[index] + ',';
           
       }
       row = row.slice(0, -1);
       str += row + '\r\n';
      
       for (let i = 0; i < array.length; i++) {
           let line = (i+1)+'';
           for (let index in headerList) {
              let head = headerList[index];
         
            if(head=='Transaction Id')
            {line += ',' + array[i].transid;
           }
            if(head=='Date')
            {line += ',' + array[i].date;}
            if(head=='Sender Profile')
            {line += ',' + array[i].senderprofile;}
            if(head=='Destination')
            {line += ',' + array[i].destination;}
            if(head=='Channel')
            {line += ',' + array[i].channel;}
            if(head=='Type')
            {line += ',' + array[i].type;}
            if(head=='Sender')
            {line += ',' + array[i].sender;}
            if(head=='Result Description')
            {line += ',' + array[i].result;}
            if(head=='Status')
            {line += ',' + array[i].status;}
            if(head=='Amount without vat')
            {line += ',' + array[i].amountpre;}
            if(head=='Vat')
            {line += ',' + array[i].vat;}
            if(head=='Amount with vat')
            {line += ',' + array[i].amountwithvat;}
            if(head=='Fiscal Stamp')
            {line += ',' + array[i].fiscalstamp;}
            if(head=='Quantity')
            {line += ',' + array[i].quantity;}
            if(head=='Expiry Date')
            {line += ',' + array[i].expirydate;}
            if(head=='Total amount')
            {line += ',' + array[i].totalamount;}
            if(head=='Sender pre balance')
            {line += ',' + array[i].senderprebalance;}
            if(head=='Sender post balance')
            {line += ',' + array[i].senderpostbalance;}
            if(head=='Sender pre fiscal balance')
            {line += ',' + array[i].senderprefiscalbalance;}
            if(head=='Sender post fiscal balance')
            {line += ',' + array[i].senderpostfiscalbalance;}
            if(head=='Receiver pre balance')
            {line += ',' + array[i].Receiverprebalance;}
            if(head=='Receiver post balance')
            {line += ',' + array[i].Receiverpostbalance;}
            if(head=='Transfer/Order Type')
            {line += ',' + array[i].transfertype;}
            if(head=='Adjustment Type')
            {line += ',' + array[i].adjustmenttype;}
           }
           str += line + '\r\n';
          
       }
       return str;
   }
   else{
      
       //      // console.log('hello',objArray);
       let array =typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
       let str = '';
       let row = 'S.No,';
  //       // console.log('hello1',array);
       for (let index in headerList) {
           row += headerList[index] + ',';
           
       }
       row = row.slice(0, -1);
       str += row + '\r\n';
      
       
           let line = 1+'';
           for (let index in headerList) {
              let head = headerList[index];
         
              if(head=='Transaction Id')
              {line += ',' + array.transid;
             }
              if(head=='Date')
              {line += ',' + array.date;}
              if(head=='Sender Profile')
              {line += ',' + array.senderprofile;}
              if(head=='Destination')
              {line += ',' + array.destination;}
              if(head=='Channel')
              {line += ',' + array.channel;}
              if(head=='Type')
              {line += ',' + array.type;}
              if(head=='Sender')
              {line += ',' + array.sender;}
              if(head=='Result Description')
              {line += ',' + array.result;}
              if(head=='Status')
              {line += ',' + array.status;}
              if(head=='Amount without vat')
              {line += ',' + array.amountpre;}
              if(head=='Vat')
              {line += ',' + array.vat;}
              if(head=='Amount with vat')
              {line += ',' + array.amountwithvat;}
              if(head=='Fiscal Stamp')
              {line += ',' + array.fiscalstamp;}
              if(head=='Quantity')
              {line += ',' + array.quantity;}
              if(head=='Expiry Date')
            {line += ',' + array.expirydate;}
              if(head=='Total amount')
              {line += ',' + array.totalamount;}
              if(head=='Sender pre balance')
              {line += ',' + array.senderprebalance;}
              if(head=='Sender post balance')
              {line += ',' + array.senderpostbalance;}
              if(head=='Sender pre fiscal balance')
              {line += ',' + array.senderprefiscalbalance;}
              if(head=='Sender post fiscal balance')
              {line += ',' + array.senderpostfiscalbalance;}
              if(head=='Receiver pre balance')
              {line += ',' + array.Receiverprebalance;}
              if(head=='Receiver post balance')
              {line += ',' + array.Receiverpostbalance;}
              if(head=='Transfer/Order Type')
              {line += ',' + array.transfertype;}
              if(head=='Adjustment Type')
              {line += ',' + array.adjustmenttype;}
           
       
       }
       str += line + '\r\n';
  
       return str;
   }
    }
}
