import { Injectable } from '@angular/core';
import{AppConfig} from '../app.config';
import {Router} from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import Swal from 'sweetalert2'
import { sha256 } from 'js-sha256';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import * as CryptoJS from 'crypto-js';
import {AuthGuard } from './auth.guard';
const httpOptions = {
  headers: new HttpHeaders({
    "Content-Type":"application/json",
   "auth.security.enable":"true",
   "authorization":"2DEF34A67B7C82850E2F531A0168375E4CF5977E6C1984553ECBA9869E379BA1"
  })
};
@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
loginClick=false;
encryptData(dataToEncrypt: string) {
   
    
    var key = CryptoJS.enc.Utf8.parse(window.atob("MTIzNDU2Nzg5MDEyMzQ1Ng=="));
    // var key = "1234567890123456";
    var encrypted = CryptoJS.AES.encrypt(dataToEncrypt, key
    , {
      mode: CryptoJS.mode.ECB,
      padding: CryptoJS.pad.Pkcs7
    }
    );
   
    var enc=CryptoJS.enc.Hex.stringify(encrypted.ciphertext);

    return enc.toUpperCase();
  }
  constructor(private unlock:AuthGuard,private router: Router,private http: HttpClient,private spinnerService: Ng4LoadingSpinnerService) {
    this.http.get<{ip:string}>('https://jsonip.com')
    .subscribe( data => {
    //   //  // console.log('th data', data);
      this.ipAddress = data
    })
   }
  private req;ipAddress:any;
  login(model){
    this.loginClick=true;
    var username=model.username;
    var password=model.password;
    var pin=password;
    var encPin=sha256(username+pin);
    
  //   //  // console.log('username',username);
  //   //  // console.log('password',password);
  //   //  // console.log('pin',pin);
  //   //  // console.log('encPin',encPin);
    
    
    this.req= {
      "header":{"requesttype":"LOGIN"}, 
      request:{agentcode:this.encryptData(username),
      pin:this.encryptData(encPin),
      vendorcode:this.encryptData("ALFA"),
      clienttype:"SELFCARE",
      clientip:this.encryptData(this.ipAddress.ip),
      version:this.encryptData("2.5"),
      sign:this.encryptData("4n5MPDjukoJqotK5YbL5ExO2gvw")}
    }
      //   //  // console.log("request",this.req);
      // if(username!=undefined && password !=undefined) { this.http.post('/services/alfaRestService',this.req,httpOptions).subscribe(result => {}); 
      // }  
      
      // else{
       
      //     Swal.fire('Invalid Request', 'Kindly Enter Login Name and Password'); 
      //     this.spinnerService.hide();
      // }
      return this.http.post('/services/alfaRestService',this.req,httpOptions);
  }
  logOutRequest;
  logout() {
    var agentcode=this.unlock.decryptData(localStorage.getItem('currentUser'));
    agentcode=agentcode.toString();
    this.logOutRequest={
      "header": {
         "requesttype": "LOGOUT"
      },
      "request": {
         "agentcode":this.encryptData(agentcode),
          "vendorcode": this.encryptData("ALFA"),
         
         "clienttype": "SELFCARE",
         "version": this.encryptData("2.5"),
         "sign": this.encryptData("4n5MPDjukoJqotK5YbL5ExO2gvw"),
         "sessionid":this.encryptData(this.unlock.decryptData(localStorage.getItem('sessionid')))
      }
   }

      this.http.post('/services/alfaRestService',this.logOutRequest,httpOptions).subscribe(result=>
        {
          localStorage.removeItem('currentUser');
          localStorage.removeItem('agenttype');
          this.router.navigate(['']);
         
          localStorage.clear();
        })
   
   
    // remove user from local storage to log user out
  
  
}
lout() {
  var agentcode=this.unlock.decryptData(localStorage.getItem('currentUser'));
    // console.log(agentcode);
  this.logOutRequest={
    "header": {
       "requesttype": "LOGOUT"
    },
    "request": {
       "agentcode":this.encryptData(agentcode),
        "vendorcode": "ALFA",
       "comments": [],
       "clienttype": "SELFCARE",
       "version": "2.5",
       "sign": "4n5MPDjukoJqotK5YbL5ExO2gvw",
         "sessionid":this.encryptData(this.unlock.decryptData(localStorage.getItem('sessionid')))
    }
 }

    this.http.post('/services/alfaRestService',this.logOutRequest,httpOptions).subscribe(result=>
      {
          // console.log(agentcode);
      })
 
 
  // remove user from local storage to log user out


}
}
