import { Component, OnInit } from '@angular/core';
import{Validators,FormControl,FormGroup} from '@angular/forms';
import { NgbModal,NgbModalRef,NgbModule } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';
import{ApiService} from '../Services/api.service';
import{AppConfig} from '../app.config';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import {Router} from '@angular/router';
import{AuthGuard} from '../Services/auth.guard';
@Component({
  selector: 'app-transfer',
  templateUrl: './transfer.component.html',
  styleUrls: ['./transfer.component.scss']
})
export class TransferComponent implements OnInit {

  constructor(private unlock:AuthGuard,private apiService:ApiService,private spinnerService: Ng4LoadingSpinnerService, private modalService: NgbModal,private router:Router) { }
  screen1;screen2;screen3;screen4;lengthError;voucher;
  transType;transferAmount;transferTime;transferDestination;balance;fiscalBalance;
   transferForm;amountError=false;msisdnError=false;msisdnLength=AppConfig.actoridLength;msisdnErrorText=AppConfig.actoridError;
  hero;modalRecord;
  openModal(content){
    this.modalService.open(content,  { windowClass : "myCustomModalClass"});  
  }
  clicked = false;
  print(): void {
    let printContents, popupWin;
    printContents = document.getElementById('print-section').innerHTML;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>Print tab</title>
          <style>
        @media print {
          html, body {
            width: 7.2cm;
            height: 15cm;
          }
          prints{
              width: 7.2cm;
              height: 15cm;
          }
        
      }
        //........Customized style.......
        </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
    );
    popupWin.document.close();
  }
  checkNumberFieldLength(elem){
    if (this.transferForm.amount.toString().length > 10) {
     this.amountError=true;
    }
        else{
          this.amountError=false;
        }
    }
    checkmsisdnFieldLength(elem){
      if (this.transferForm.ReceiverActorId.toString().length != this.msisdnLength && (this.transferForm.ReceiverActorId.toString().length!=0 || this.transferForm.ReceiverActorId.toString().length!=undefined)) {
       this.msisdnError=true;
              
          }
          else{
            this.msisdnError=false;
          }
      }firstname;lastname;
   ngOnInit() {
     this.screen1=true;
     this.screen2=false;
     this.screen3=false;
     this.screen4=false; 
     this.firstname=this.unlock.decryptData(localStorage.getItem('agentname'));
    this.lastname=this.unlock.decryptData(localStorage.getItem('lastname'));
     this.transferForm = new FormGroup({
       'transactionType': new FormControl( [
         Validators.required,
         Validators.minLength(10),
        
       ]),
       'ReceiverActorId': new FormControl( [
         Validators.required,
         Validators.minLength(10),
        
       ]),
       'amount': new FormControl( [
         Validators.required,
         Validators.minLength(10),
        
       ]),
       'mpin': new FormControl( [
         Validators.required,
         Validators.minLength(10),
        
       ])
     });
   }
   tType;agentDetailsResult;agentDetailsResultDesc;agentDetails;
 screen2Show(type){
   
   if(type=='true'){
     
     this.tType="Value";
   }
   else{
     
     this.tType="Fiscal Stamp";
   }
  this.transType=type;
   var a=this.transferForm.ReceiverActorId;
   var e=this.transferForm.mpin;
   var f=this.transferForm.amount;
 if(a!=undefined && e!=undefined && f!=undefined){
   if(this.msisdnError==false && this.amountError==false){
    this.apiService.agentDetailsApi(a,e).subscribe(result=>{ var b:any=result; var resultcode=b.estel.response.resultcode; if(resultcode!=undefined && (resultcode==1956 || resultcode=='1956') ){this.router.navigate(['']);localStorage.clear();}
     
      this.agentDetailsResult=result;
      this.agentDetailsResultDesc=this.agentDetailsResult.estel.response.resultdescription;
      if(this.agentDetailsResultDesc=='Transaction Successful'){
        this.agentDetails=this.agentDetailsResult.estel.response;
      //   //  // console.log("agent",this.agentDetails);  
        this.screen2=false;
        this.screen1=false;
        this.screen3=true;
        this.screen4=false;
       
      }
      else{
        if(this.agentDetailsResultDesc=='Blocked Dest Customer' || this.agentDetailsResult.estel.response.resultcode==132 || this.agentDetailsResult.estel.response.resultcode=='132' || this.agentDetailsResult.estel.response.resultcode==14 || this.agentDetailsResult.estel.response.resultcode=='14'){
          Swal.fire('Operation Failed', 'Blocked Subscriber/Agent');
        
        }
        else{
          if(this.agentDetailsResult.estel.response.resultcode==18 || this.agentDetailsResult.estel.response.resultcode=='18' || this.agentDetailsResult.estel.response.resultcode==926 || this.agentDetailsResult.estel.response.resultcode=='926' || this.agentDetailsResult.estel.response.resultcode==105 || this.agentDetailsResult.estel.response.resultcode=='105'){
            Swal.fire('Operation Failed', 'You are not allowed to transfer to this actor');
          }
          else
          Swal.fire('Operation Failed', this.agentDetailsResultDesc);
        
        }
        // Swal.fire('Operation Failed',this.agentDetailsResultDesc);
      }
      })
     
    }   
    
    else
    { Swal.fire('Operation Failed', 'Actor does not exist') }
   
 }
 else{
  Swal.fire('Operation Failed', 'All Fields are Required ')
 }
 }
 screen3Show(){


  
   Swal.fire('Operation Failed', 'Amount and mpin cannot be empty');
  
    this.screen3=true;
    this.screen1=false;
    this.screen2=false;
    this.screen4=false;
   
 
   Swal.fire('Operation Failed', 'Amount and mpin cannot be empty');
 }
 cancel(){
   this.screen1=true;
   this.screen3=false;
   this.screen2=false;
   this.screen4=false;
   this.clicked=false;
 }
 transferResult;transferResultDesc;transferDate;walletType;fromFirstname;fromLastname;fromnumber;
 screen4Show(){
  this.spinnerService.show();
  var a=this.transferForm.ReceiverActorId;
  var e=this.transferForm.mpin;
  var f=this.transferForm.amount;
   this.apiService.transferApi(e,a,f,this.transType).subscribe(result=>{ var b:any=result; var resultcode=b.estel.response.resultcode; if(resultcode!=undefined && (resultcode==1956 || resultcode=='1956') ){this.router.navigate(['']);localStorage.clear();}
this.transferResult=result;
//  // console.log("transfer",this.transferResult);
this.transferResultDesc=this.transferResult.estel.response.resultdescription;
this.transferAmount=this.transferResult.estel.response.amount;
this.transferDestination=this.transferResult.estel.response.destination;
this.transferTime=this.transferResult.estel.response.responsects;
this.walletType=this.transferResult.estel.response.wallettype;
this.fromnumber=this.transferResult.estel.response.agentcode;
this.transferDate=this.transferTime.split(" ");
this.balance=this.transferResult.estel.response.walletbalance;
this.fiscalBalance=this.transferResult.estel.response.srcfiscalclosing;
this.fromFirstname=this.transferResult.estel.response.firstname;
this.fromLastname=this.transferResult.estel.response.lastname;
if(this.transferResultDesc=='Transaction Successful'){
  this.screen4=true;
  this.screen1=false;
   this.screen3=false;
   this.screen2=false;
    a='';this.transferForm.ReceiverActorId='';
   e='';this.transferForm.mpin='';
   f='';this.transferForm.amount='';
   this.transType=true;
   this.spinnerService.hide();
}
else{
  if(this.transferResultDesc=='Suspend Subscriber/Agent' || this.transferResultDesc=='Subscriber/Agent not in same hierarchy' || this.transferResultDesc=='Locked Subscriber/Agent')
  {
    Swal.fire('Operation Failed', 'You are not allowed to transfer to this actor');
  this.spinnerService.hide();
  }
  else{

    if(this.transferResultDesc=='Total Amount Of Daily Transactions Limit Reached' || this.transferResult.estel.response.resultcode==431 || this.transferResult.estel.response.resultcode==432 || this.transferResult.estel.response.resultcode==433 || this.transferResult.estel.response.resultcode==434 || this.transferResult.estel.response.resultcode==435 || this.transferResult.estel.response.resultcode==436 || this.transferResult.estel.response.resultcode==437 || this.transferResult.estel.response.resultcode==4377 || this.transferResult.estel.response.resultcode=='431' || this.transferResult.estel.response.resultcode=='432' || this.transferResult.estel.response.resultcode=='433' || this.transferResult.estel.response.resultcode=='434' || this.transferResult.estel.response.resultcode=='435' || this.transferResult.estel.response.resultcode=='436' || this.transferResult.estel.response.resultcode=='437' || this.transferResult.estel.response.resultcode=='4377' ){
      if(this.transferResult.estel.response.resultcode==431 || this.transferResult.estel.response.resultcode=='431'){
        Swal.fire('Operation Failed', 'You have reached the Daily amount maximum transaction limit.Kindly contact our Help Line on 119 (free of charge)');
        this.spinnerService.hide();
      }
      if(this.transferResult.estel.response.resultcode==432 || this.transferResult.estel.response.resultcode=='432'){
        Swal.fire('Operation Failed', 'You have reached the Monthly amount maximum transaction limit.Kindly contact our Help Line on 119 (free of charge)');
        this.spinnerService.hide();
      }
      if(this.transferResult.estel.response.resultcode==433 || this.transferResult.estel.response.resultcode=='433'){
        Swal.fire('Operation Failed', 'You have reached the Weekly amount maximum transaction limit.Kindly contact our Help Line on 119 (free of charge)');
        this.spinnerService.hide();
      }
      if(this.transferResult.estel.response.resultcode==434 || this.transferResult.estel.response.resultcode=='434'){
        Swal.fire('Operation Failed', 'You have reached the Daily maximum transaction limit.Kindly contact our Help Line on 119 (free of charge)');
        this.spinnerService.hide();
      }
      if(this.transferResult.estel.response.resultcode==435 || this.transferResult.estel.response.resultcode=='435'){
        Swal.fire('Operation Failed', 'You have reached the Monthly maximum transaction limit.Kindly contact our Help Line on 119 (free of charge)');
        this.spinnerService.hide();
      }
      if(this.transferResult.estel.response.resultcode==436 || this.transferResult.estel.response.resultcode=='436'){
        Swal.fire('Operation Failed', 'You have reached the Weekly maximum transaction limit.Kindly contact our Help Line on 119 (free of charge)');
        this.spinnerService.hide();
      }
      if(this.transferResult.estel.response.resultcode==437 || this.transferResult.estel.response.resultcode=='437'){
        Swal.fire('Operation Failed', 'Your transaction amount is above the maximum authorized limit');
        this.spinnerService.hide();
      }
      if(this.transferResult.estel.response.resultcode==4377 || this.transferResult.estel.response.resultcode=='4377'){
        Swal.fire('Operation Failed', 'You are transferring amount less than the minimum transfer limit. Kindly contact our Help Line on 119 (free of charge)');
        this.spinnerService.hide();
      }
    
    }
    else{
      if(this.transferResultDesc=='Blocked Dest Customer' || this.transferResult.estel.response.resultcode==132 || this.transferResult.estel.response.resultcode=='132' || this.transferResult.estel.response.resultcode==14 || this.transferResult.estel.response.resultcode=='14'){
        Swal.fire('Operation Failed', 'Blocked Subscriber/Agent');
      this.spinnerService.hide();
      }
      else{
        if(this.transferResult.estel.response.resultcode==18 || this.transferResult.estel.response.resultcode=='18' || this.transferResult.estel.response.resultcode==926 || this.transferResult.estel.response.resultcode=='926' || this.transferResult.estel.response.resultcode==105 || this.transferResult.estel.response.resultcode=='105'){
          Swal.fire('Operation Failed', 'You are not allowed to transfer to this actor');
          this.spinnerService.hide();
        }
        else
        Swal.fire('Operation Failed', this.transferResultDesc);
      this.spinnerService.hide();
      }
     
    }
    
  }
  
 }
   })
 
 }

}
