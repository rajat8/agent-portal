import { Component, OnInit } from '@angular/core';
import{Validators,FormControl,FormGroup} from '@angular/forms';
import { NgbModal,NgbModalRef,NgbModule } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';
import{ApiService} from '../Services/api.service';
import{AuthGuard} from '../Services/auth.guard';
import{AppConfig} from '../app.config';
import { NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import {Router} from '@angular/router';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import PerfectScrollbar from 'perfect-scrollbar';
import{AdminLayoutComponent} from '../layouts/admin-layout/admin-layout.component'
import {formatDate} from '@angular/common';
@Component({
  selector: 'app-e-voucher',
  templateUrl: './e-voucher.component.html',
  styleUrls: ['./e-voucher.component.scss']
})
export class EVoucherComponent implements OnInit {
  constructor(private unlock:AuthGuard,private ad:AdminLayoutComponent,config: NgbModalConfig, private modalService: NgbModal,private apiService:ApiService,private spinnerService: Ng4LoadingSpinnerService,private router:Router) {
    config.backdrop = 'static';
    config.keyboard = false;
   }
 screen1;screen2;screen3;lengthError;voucher;agenttype;totalamountwords;reviewVat;reviewTotal;
  topupForm;msisdnError=false;msisdnLength=AppConfig.msisdnLength;msisdnErrorText=AppConfig.msisdnError;
 hero;

 checkmsisdnFieldLength(elem){
  let upperCaseCharacters = /[A-Z]+/g;
  let lowerCaseCharacters = /[a-z]+/g;
  let numberCharacters = /[0-9]+/g;
  let specialCharacters = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;
  if ((this.topupForm.phone.toString().length != this.msisdnLength && (this.topupForm.phone.toString().length!=0 || this.topupForm.phone.toString().length!=undefined)) || numberCharacters.test(this.topupForm.phone) === false || upperCaseCharacters.test(this.topupForm.phone) === true || lowerCaseCharacters.test(this.topupForm.phone) === true  || specialCharacters.test(this.topupForm.phone) === true) {
   this.msisdnError=true;
          
      }
      else{
        this.msisdnError=false;
      }
  }
 a = [
  '',
  'One ',
  'Two ',
  'Three ',
  'Four ',
  'Five ',
  'Six ',
  'Seven ',
  'Eight ',
  'Nine ',
  'Ten ',
  'Eleven ',
  'Twelve ',
  'Thirteen ',
  'Fourteen ',
  'Fifteen ',
  'Sixteen ',
  'Seventeen ',
  'Eighteen ',
  'Nineteen '];

b = [
  '',
  '',
  'Twenty',
  'Thirty',
  'Forty',
  'Fifty',
  'Sixty',
  'Seventy',
  'Eighty',
  'Ninety'];

transform(value: any, args?: any): any {
  if (value) {
    let num: any = Number(value);
    if (num) {
      if ((num = num.toString()).length > 9)  { return 'We are not the Iron Bank, you can lower down the stakes :)'; }
      const n = ('000000000' + num).substr(-9).match(/^(\d{2})(\d{2})(\d{2})(\d{1})(\d{2})$/);
      if (!n) {return ''; }
      let str = '';
      str += (Number(n[1]) !== 0) ? (this.a[Number(n[1])] || this.b[n[1][0]] + ' ' + this.a[n[1][1]]) + 'CRORE ' : '';
      str += (Number(n[2]) !== 0) ? (this.a[Number(n[2])] || this.b[n[2][0]] + ' ' + this.a[n[2][1]]) + 'LAKH ' : '';
      str += (Number(n[3]) !== 0) ? (this.a[Number(n[3])] || this.b[n[3][0]] + ' ' + this.a[n[3][1]]) + 'THOUSAND ' : '';
      str += (Number(n[4]) !== 0) ? (this.a[Number(n[4])] || this.b[n[4][0]] + ' ' + this.a[n[4][1]]) + 'HUNDRED ' : '';
      str += (Number(n[5]) !== 0) ? ((str !== '') ? 'and ' : '') +
      (this.a[Number(n[5])] || this.b[n[5][0]] + ' ' +
      this.a[n[5][1]]) + '' : '';
    //   //  // console.log(str);
     
      return str;
    } else {
      return '';
    }
  } else {
    return '';
  }
}
withDecimal(n) {
  var nums = n.toString().split('.')
  var whole = this.transform(nums[0])
  if (nums.length == 2) {
      var fraction = this.transform(nums[1])
      return whole + 'Us Dollars And ' + fraction + 'Cents Only';
    } else {
        return whole + 'Us Dollars Only';
  }
}
scrollToTop(el) {
  //  const elemMainPanel = <HTMLElement>document.querySelector('.main-panel');
  //  const ps = new PerfectScrollbar(elemMainPanel);
  //     ps.update();
  //   el.scrollTop = 0;
  //   elemMainPanel.scrollTop = 0;
  this.ad.scrollToTop();
  }
evoucherResult;evoucherResultDesc;evoucherReciepts;voucherCount;quantitymax;Vat;
  ngOnInit() {
    
    this.screen1=true;
    this.screen2=false;
    this.screen3=false;
    this.Vat=this.unlock.decryptData((localStorage.getItem('Vat')));
    this.agenttype=this.unlock.decryptData(localStorage.getItem('agenttype'));
    this.quantitymax=this.unlock.decryptData(localStorage.getItem('quantity'));
    this.topupForm = new FormGroup({
      'phone': new FormControl( [
        Validators.required,
        Validators.minLength(10),
       
      ]),
      'mpin': new FormControl( [
        Validators.required,
        Validators.minLength(10),
       
      ])
    });
    this.apiService.billerVoucherApi().subscribe(result=>{ var b:any=result; var resultcode=b.estel.response.resultcode; if(resultcode!=undefined && (resultcode==1956 || resultcode=='1956') ){this.router.navigate(['']);localStorage.clear();}
    //   //  // console.log("biller voucher resp",result);  
      this.billerResult=result;
      this.billerResultDesc=this.billerResult.estel.response.resultdescription;
      if(this.billerResultDesc=='Transaction Successful'){
        this.biller=this.billerResult.estel.response.records.record;
        
       
      }
      else{
        Swal.fire('Operation Failed',this.billerResultDesc);
      }
      })
  }
  totalamount;amount;operator='';biller;billerResult;billerResultDesc;products;productCount;productsResult;productsResultDesc;agentDetailsResult;agentDetailsResultDesc;agentDetails;
  onChangeoperatorEvent(ev) {
//    // console.log(ev,this.operator);this.productCount=undefined;
  this.operator=ev.target.value;
  this.productCount=undefined;
  if(ev.target.value)
  this.apiService.prodDetailsVoucherApi(ev.target.value).subscribe(result=>{ var b:any=result; var resultcode=b.estel.response.resultcode; if(resultcode!=undefined && (resultcode==1956 || resultcode=='1956') ){this.router.navigate(['']);localStorage.clear();}
  //   //  // console.log("prod resp",result);  
    this.productsResult=result;
    this.productsResultDesc=this.productsResult.estel.response.resultdescription;
    this.productCount=this.productsResult.estel.response.recordcount;
    if(this.productsResultDesc=='Transaction Successful'){
      
      this.products=this.productsResult.estel.response.records.record;
      if(this.productCount>1){
        this.products=  this.products.sort((a,b) => a.pricewithoutvat - b.pricewithoutvat);
        for(var i=0;i<this.products.length;i++){
          this.products[i].pricewithoutvat=this.products[i].pricewithoutvat.toString();
          if(this.products[i].pricewithoutvat.split(".")!=undefined){
            this.products[i].pricewithoutvat=this.products[i].pricewithoutvat.split(".");
           
          }
          if(this.products[i].productdesc!=undefined)
          {this.products[i].productdesc=this.products[i].productdesc.split("delimitter");
          for(var j=0;j<this.products[i].productdesc.length;j++){
            this.products[i].productdesc[j]=this.products[i].productdesc[j].split(":");
          }
        }
         
        }
        
      }
      if(this.productCount==1){
        this.products.pricewithoutvat=this.products.pricewithoutvat.toString();
        if(this.products.pricewithoutvat.split(".")!=undefined){
          this.products.pricewithoutvat=this.products.pricewithoutvat.split(".");
         
        }
        this.products.productdesc=this.products.productdesc.split("delimitter");
        for(var j=0;j<this.products.productdesc.length;j++){
          this.products.productdesc[j]=this.products.productdesc[j].split(":");
        }
      }

    }
    else{
      Swal.fire('Operation Failed',this.productsResultDesc);
    }
    })
 }
 vat;
//  onChangeamountEvent(ev) {
// //   //  // console.log(ev.target.value);
// this.amount=ev.target.value;
// }
productCode;pricewithoutvat;productdesc;
screen2Show(totalamount,amount,productcode,pwvat,taxcount,taxtypes){
  this.totalamount=totalamount;
  this.totalamount=Math.round(this.totalamount * 100) / 100;
  this.amount=amount;
  this.productCode=productcode;
  this.pricewithoutvat=pwvat;
  if(taxcount>1){
    for(var i=0;i<taxtypes.tax.length;i++){
        // console.log(taxtypes.tax[i].taxtype.length);
      if(taxtypes.tax[i].taxtype.length>5){
        taxtypes.tax[i].taxtype= taxtypes.tax[i].taxtype.split(":");
      }
      
      var type=taxtypes.tax[i].taxtype[2];
      if(type=='VAT'){
        this.fiscalStamp=undefined;
        this.vat=taxtypes.tax[i].taxtype[0];
      }
      if(type=='Fiscal'){
        this.fiscalStamp=taxtypes.tax[i].taxtype[0];
      }
        // console.log(taxtypes.tax[i].taxtype);
    }
  }
  if(taxcount==1){
    if(taxtypes.tax.taxtype.length>5){
      taxtypes.tax.taxtype= taxtypes.tax.taxtype.split(":");
    }
      var type=taxtypes.tax.taxtype[2];
      if(type=='VAT'){
        this.vat=taxtypes.tax.taxtype[0];
      }
      if(type=='Fiscal'){
        this.fiscalStamp=taxtypes.tax.taxtype[0];
      }
        // console.log(taxtypes.tax.taxtype);
    
  }
  if(taxcount==0){
    
    
      this.vat=0;
    
      this.fiscalStamp=undefined;
    
  
  }
//   //  // console.log(this.amount );
//   //  // console.log( this.topupForm.quantity);
if(this.pricewithoutvat.length>1){
  this.pricewithoutvat=this.pricewithoutvat[0]+'.'+this.pricewithoutvat[1];
}
//this.reviewprice=this.pricewithoutvat;
  this.reviewVat=(this.pricewithoutvat * this.topupForm.quantity) *this.vat/100;
  this.reviewTotal=(totalamount * this.topupForm.quantity) ;
  this.reviewVat=Math.round(this.reviewVat * 100) / 100;
 this.reviewTotal= Math.round(this.reviewTotal * 100) / 100;
  var a=this.topupForm.phone;
  this.reviewprice=(this.pricewithoutvat * this.topupForm.quantity) ;
 this.reviewprice= Math.round(this.reviewprice * 100) / 100;
 this.reviewfiscal=(this.fiscalStamp * this.topupForm.quantity) ;
 this.reviewfiscal= Math.round(this.reviewfiscal * 100) / 100;
if(a!=undefined && this.topupForm.mpin!=undefined && this.topupForm.quantity!=undefined){
  if(a.toString().length==this.msisdnLength){
   
    if(this.topupForm.quantity<=this.quantitymax){
      this.screen2=true;
      this.screen1=false;
      this.screen3=false;
    }
    else{
      Swal.fire('Operation Failed', 'Quantity is more than maximum quantiy limit.')
    }
   }   
   if(a.toString().length!=this.msisdnLength) 
   { Swal.fire('Operation Failed', this.msisdnErrorText) }
  
 
}
else{
  Swal.fire('Operation Failed', 'All Fields are required')
}
}
fiscalStamp;respVat;modalReference;reviewfiscal;reviewprice;
screen3Show(content){
  this.spinnerService.show();
  

  this.apiService.evoucherApi(this.topupForm.mpin,this.productCode,this.amount,this.topupForm.phone,this.topupForm.quantity).subscribe(result=>{ var b:any=result; var resultcode=b.estel.response.resultcode; if(resultcode!=undefined && (resultcode==1956 || resultcode=='1956') ){this.router.navigate(['']);localStorage.clear();}
  //   //  // console.log("evoucher resp",result);  
    this.evoucherResult=result;
    this.evoucherResultDesc=this.evoucherResult.estel.response.resultdescription;
    if(this.evoucherResultDesc=='Transaction Successful'){
      this.respVat=this.evoucherResult.estel.response.amount*this.vat/100;
      this.evoucherResult.estel.response.responsects=this.evoucherResult.estel.response.responsects.split(" ");
      this.evoucherResult.estel.response.responsects[1]=this.tConvert(this.evoucherResult.estel.response.responsects[1]);
      this.respVat=Math.round(this.respVat * 100) / 100;
      var total=this.evoucherResult.estel.response.totalamount;
      ;
      this.totalamountwords=this.withDecimal(total);
      this.totalamountwords.toUpperCase( );
      this.evoucherReciepts=this.evoucherResult.estel.response.records.record;
      if(this.evoucherReciepts.expirydate){
        // this.evoucherReciepts.expirydate=Date(this.evoucherReciepts.expirydate);
        // this.evoucherReciepts.expirydate=formatDate(this.evoucherReciepts.expirydate, 'dd/MM/yyyy', 'en');
        var stringDate1=this.evoucherReciepts.expirydate;
var splitDate1 = stringDate1.split('-');
var day1  = splitDate1[0];
var month1 = splitDate1[1];
var year1 = splitDate1[2];
this.evoucherReciepts.expirydate =day1+"/"+ month1+"/"+year1;
      }
      else{
        for(var i=0;i<this.evoucherReciepts.length;i++){
          // this.evoucherReciepts[i].expirydate=Date(this.evoucherReciepts[i].expirydate);
          // this.evoucherReciepts[i].expirydate=formatDate(this.evoucherReciepts[i].expirydate, 'dd/MM/yyyy', 'en');
          var stringDate1=this.evoucherReciepts[i].expirydate;
          var splitDate1 = stringDate1.split('-');
          var day1  = splitDate1[0];
          var month1 = splitDate1[1];
          var year1 = splitDate1[2];
          this.evoucherReciepts[i].expirydate =day1+"/"+ month1+"/"+year1;
        }
      }
      this.voucherCount=this.evoucherResult.estel.response.recordcount;
      this.screen3=true;
      this.screen1=false;
      this.screen2=false;
      this.topupForm.mpin=undefined;
      this.topupForm.quantity=undefined;
      this.topupForm.phone=undefined;this.productCount=undefined;
      this.spinnerService.hide();
      this.modalReference =  this.modalService.open(content);
    }
    else{
      if(this.evoucherResultDesc=='Insufficient Pin')
      {Swal.fire('Operation Failed','Product Out of Stock');
      this.spinnerService.hide();}

      else{ 
        if(this.evoucherResultDesc=='Total Amount Of Daily Transactions Limit Reached' || this.evoucherResult.estel.response.resultcode==431 || this.evoucherResult.estel.response.resultcode==432 || this.evoucherResult.estel.response.resultcode==433 || this.evoucherResult.estel.response.resultcode==434 || this.evoucherResult.estel.response.resultcode==435 || this.evoucherResult.estel.response.resultcode==436 || this.evoucherResult.estel.response.resultcode==437 || this.evoucherResult.estel.response.resultcode==4377 || this.evoucherResult.estel.response.resultcode=='431' || this.evoucherResult.estel.response.resultcode=='432' || this.evoucherResult.estel.response.resultcode=='433' || this.evoucherResult.estel.response.resultcode=='434' || this.evoucherResult.estel.response.resultcode=='435' || this.evoucherResult.estel.response.resultcode=='436' || this.evoucherResult.estel.response.resultcode=='437' || this.evoucherResult.estel.response.resultcode=='4377' ){
          if(this.evoucherResult.estel.response.resultcode==431 || this.evoucherResult.estel.response.resultcode=='431'){
            Swal.fire('Operation Failed', 'You have reached the Daily amount maximum transaction limit.Kindly contact our Help Line on 119 (free of charge)');
            this.spinnerService.hide();
          }
          if(this.evoucherResult.estel.response.resultcode==432 || this.evoucherResult.estel.response.resultcode=='432'){
            Swal.fire('Operation Failed', 'You have reached the Monthly amount maximum transaction limit.Kindly contact our Help Line on 119 (free of charge)');
            this.spinnerService.hide();
          }
          if(this.evoucherResult.estel.response.resultcode=='433' ||this.evoucherResult.estel.response.resultcode==433){
            Swal.fire('Operation Failed', 'You have reached the Weekly amount maximum transaction limit.Kindly contact our Help Line on 119 (free of charge)');
            this.spinnerService.hide();
          }
          if(this.evoucherResult.estel.response.resultcode==434 || this.evoucherResult.estel.response.resultcode=='434'){
            Swal.fire('Operation Failed', 'You have reached the Daily maximum transaction limit.Kindly contact our Help Line on 119 (free of charge)');
            this.spinnerService.hide();
          }
          if(this.evoucherResult.estel.response.resultcode==435 || this.evoucherResult.estel.response.resultcode=='435'){
            Swal.fire('Operation Failed', 'You have reached the Monthly maximum transaction limit.Kindly contact our Help Line on 119 (free of charge)');
            this.spinnerService.hide();
          }
          if(this.evoucherResult.estel.response.resultcode==436 || this.evoucherResult.estel.response.resultcode=='436'){
            Swal.fire('Operation Failed', 'You have reached the Weekly maximum transaction limit.Kindly contact our Help Line on 119 (free of charge)');
            this.spinnerService.hide();
          }
          if(this.evoucherResult.estel.response.resultcode==437 || this.evoucherResult.estel.response.resultcode=='437'){
            Swal.fire('Operation Failed', 'Your transaction amount is above the maximum authorized limit');
            this.spinnerService.hide();
          }
          if(this.evoucherResult.estel.response.resultcode==4377 || this.evoucherResult.estel.response.resultcode=='4377'){
            Swal.fire('Operation Failed', 'You are transferring amount less than the minimum transfer limit. Kindly contact our Help Line on 119 (free of charge)');
            this.spinnerService.hide();
          }
        
        }
        else{Swal.fire('Operation Failed',this.evoucherResultDesc);
        this.spinnerService.hide();}
        }
     

    }
    })

}
print(): void {
  let printContents, popupWin;
  printContents = document.getElementById('print-section').innerHTML;
  popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
  popupWin.document.open();
  popupWin.document.write(`
    <html>
      <head>
        <title>Print tab</title>
        <style>
        @media print {
          html, body {
            width: 7.2cm;
            height: 15cm;
          }
          prints{
              width: 7.2cm;
              height: 15cm;
          }
          .fsize-10{
            font-size: 12px ;
          }
          .fsize-12{
            font-size: 14px ;
          }
          .fsize-7{
            font-size: 8px ;
            line-height:1;
          }
          .text-center{
            text-align:center;
          }
          .margin-b-2{
            margin-bottom:10px !important;
            padding-bottom:0 !important;
          }
        
      }
        //........Customized style.......
        </style>
      </head>
  <body onload="window.print();window.close()">${printContents}</body>
    </html>`
  );
  popupWin.document.close();
  this.modalReference.close();
}
clicked = false;
cancel(){
  this.screen1=true;
  this.screen3=false;
  this.screen2=false;
  this.totalamountwords='';
  this.clicked=false;
  this.scrollToTop('el');
}
tConvert (time) {
  // Check correct time format and split into components
  time = time.toString ().match (/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

  if (time.length > 1) { // If time format correct
    time = time.slice (1);  // Remove full string match value
    time[5] = +time[0] < 12 ? 'AM' : 'PM'; // Set AM/PM
    time[0] = +time[0] % 12 || 12; // Adjust hours
  }
  return time.join (''); // return adjusted time or original string
}
}
