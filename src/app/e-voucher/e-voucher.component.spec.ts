import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EVoucherComponent } from './e-voucher.component';

describe('EVoucherComponent', () => {
  let component: EVoucherComponent;
  let fixture: ComponentFixture<EVoucherComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EVoucherComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EVoucherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
