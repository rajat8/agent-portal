export const AppConfig = {
    Logininputs: [
      { type: "text", name: "username",placeholder: "Enter Mobile Number", icon: "fas fa-user", },
      { type: "password", name: "password", placeholder: "Enter Password", icon: "fas fa-unlock-alt",  },
      {type:"select",name:"language",icon:"fas fa-language",options:[{name:'English',value:1},{name:'Arabic',value:2}]}
    ],
    LoginDestination: 'home',
    msisdnLength:8,
    msisdnError:'Enter the 8 digits valid Alfa Mobile Number.',
    actoridLength:7,
    actoridError:'Enter the 7 digits valid Actor ID.',
localkey:'rjtprosecurity'
  
  };