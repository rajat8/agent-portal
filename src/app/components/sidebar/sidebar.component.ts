import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {AuthenticationService} from '../../Services/authentication.service'
import Swal from 'sweetalert2';
import{ApiService} from '../../Services/api.service';

import { AuthGuard } from '../../Services/auth.guard';
declare interface RouteInfo {
    path?: string;
    menuCode:string;
    title?: string;
    icon?: string;
    class?: string;
    children?:RouteInfo[];
    desc:string;
}

 
@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  menuItems: any[];
  public ROUTES = [
    // { path: '/home', title: 'Home',  icon: 'design_app', class: '' },
    // { path: '/icons', title: 'Icons',  icon:'education_atom', class: '' },
    // { path: '/maps', title: 'Maps',  icon:'location_map-big', class: '' },
    // { path: '/notifications', title: 'Notifications',  icon:'ui-1_bell-53', class: '' },
  
    // { path: '/user-profile', title: 'User Profile',  icon:'users_single-02', class: '' },
    // { path: '/table-list', title: 'Table List',  icon:'design_bullet-list-67', class: '' },
    // { path: '/typography', title: 'Typography',  icon:'text_caps-small', class: '' },
    
    { path: '',menuCode:'Recharge', desc:'',title: 'Recharge',  icon: 'design_bullet-list-67', class: '' ,children:[
      { path: '/e-recharge',menuCode:'TOPUP', desc:'',title: 'E-RECHARGE',  icon: 'now-ui-icons business_money-coins', class: '' },
    { path: '/e-voucher',menuCode:'BULKPINPURCHASE', desc:'',title: 'E-VOUCHER',  icon: 'now-ui-icons tech_mobile', class: '' },
    ]},
    { path: '/transfer',menuCode:'MOVESTOCK', desc:'',title: 'Transfer',  icon: 'text_caps-small', class: '' },
    { path: '/balance',menuCode:'BALANCE', desc:'',title: 'Balance Inquiry',  icon: 'business_bank', class: '' },
    { path: '',menuCode:'Orders', desc:'',title: 'Orders',  icon: 'files_paper',class:'', children:[
      { path: '/create-order',menuCode:'ORDER', desc:'',title: 'ORDER',  icon: 'fas fa-cart-plus', class: '' },
    { path: '/search-order',menuCode:'SEARCH', desc:'',title: 'SEARCH ORDER',  icon: 'fas fa-search', class: '' },
    ] },
    { path: '/history',menuCode:'TRANSSTATUS', desc:'',title: 'Transaction History',  icon: 'files_paper', class: '' },
    { path: '/changeMpin', menuCode:'Settings',desc:'',title: 'Settings',  icon: 'design_bullet-list-67',class:'', children:[
      { path: '/changeMpin',menuCode:'CHANGEPIN', desc:'',title: 'Change MPIN',  icon: 'now-ui-icons shopping_credit-card', class: '' },
    { path: '/changeMpin',menuCode:'CHANGELANG',desc:'',title: 'Change Language',  icon: 'now-ui-icons education_atom', class: '' },
    
    ] },
    { path: '/agent-hierarchy',menuCode:'HIERARCHYVIEW',desc:'',title: 'AGENT HIERARCHY',  icon: '', class: '' },
    { path: '',menuCode:'LOGOUT',desc:'',title: 'LOG OUT',  icon: '', class: '' }
  ];
  constructor(private unlock:AuthGuard,private router:Router,private authService:AuthenticationService,private apiService:ApiService) { }
balance=false;topup=false;voucher=false;transfer=false;order=false;searchorder=false;changepin=false;changelanguage=false;history=false;agentHierarchy=false;
menu=[];
storedMenu=[];
  ngOnInit() {
   this.ROUTES = [
      // { path: '/home', title: 'Home',  icon: 'design_app', class: '' },
      // { path: '/icons', title: 'Icons',  icon:'education_atom', class: '' },
      // { path: '/maps', title: 'Maps',  icon:'location_map-big', class: '' },
      // { path: '/notifications', title: 'Notifications',  icon:'ui-1_bell-53', class: '' },
    
      // { path: '/user-profile', title: 'User Profile',  icon:'users_single-02', class: '' },
      // { path: '/table-list', title: 'Table List',  icon:'design_bullet-list-67', class: '' },
      // { path: '/typography', title: 'Typography',  icon:'text_caps-small', class: '' },
      
      { path: '',menuCode:'Recharge', desc:'',title: 'Recharge',  icon: 'design_bullet-list-67', class: '' ,children:[
        { path: '/e-recharge',menuCode:'TOPUP', desc:'',title: 'E-RECHARGE',  icon: 'now-ui-icons business_money-coins', class: '' },
      { path: '/e-voucher',menuCode:'BULKPINPURCHASE', desc:'',title: 'E-VOUCHER',  icon: 'now-ui-icons tech_mobile', class: '' },
      ]},
      { path: '/transfer',menuCode:'MOVESTOCK', desc:'',title: 'Transfer',  icon: 'text_caps-small', class: '' },
      { path: '/balance',menuCode:'BALANCE', desc:'',title: 'Balance Inquiry',  icon: 'business_bank', class: '' },
      { path: '',menuCode:'Orders', desc:'',title: 'Orders',  icon: 'files_paper',class:'', children:[
        { path: '/create-order',menuCode:'ORDER', desc:'',title: 'ORDER',  icon: 'fas fa-cart-plus', class: '' },
      { path: '/search-order',menuCode:'SEARCH', desc:'',title: 'SEARCH ORDER',  icon: 'fas fa-search', class: '' },
      ] },
      { path: '/history',menuCode:'TRANSSTATUS', desc:'',title: 'Transaction History',  icon: 'files_paper', class: '' },
      { path: '/changeMpin', menuCode:'Settings',desc:'',title: 'Settings',  icon: 'design_bullet-list-67',class:'', children:[
        { path: '/changeMpin',menuCode:'CHANGEPIN', desc:'',title: 'Change MPIN',  icon: 'now-ui-icons shopping_credit-card', class: '' },
      { path: '/changeMpin',menuCode:'CHANGELANG',desc:'',title: 'Change Language',  icon: 'now-ui-icons education_atom', class: '' },
      
      ] },
      { path: '/agent-hierarchy',menuCode:'HIERARCHYVIEW',desc:'',title: 'AGENT HIERARCHY',  icon: '', class: '' },
      { path: '',menuCode:'LOGOUT',desc:'',title: 'LOG OUT',  icon: '', class: '' }
    ];
    this.storedMenu=[];
    this.menuItems=null;
    this.storedMenu=this.unlock.decryptData(localStorage.getItem('menu'));
    //this.menuItems = ROUTES.filter(menuItem => menuItem);
  this.menuItems=this.ROUTES;
    var c=this.menuItems;

  for(var i=0;i<this.storedMenu.length;i++){
    
    if(this.storedMenu[i].transcode=='HIERARCHYVIEW'){
      var index = this.menuItems.findIndex(x => x.menuCode ==="HIERARCHYVIEW");
      this.menuItems[index].desc=this.storedMenu[i].transdesc;
      this.agentHierarchy=true; 
    }
    if(this.storedMenu[i].transcode=='BALANCE'){
      var index = this.menuItems.findIndex(x => x.menuCode ==="BALANCE");
      this.menuItems[index].desc=this.storedMenu[i].transdesc;
      this.balance=true;
    }
    if(this.storedMenu[i].transcode=='TOPUP'){
      var index = this.menuItems.findIndex(x => x.menuCode ==="Recharge");
      var childindex=this.menuItems[index].children.findIndex(x => x.menuCode ==="TOPUP");
      this.menuItems[index].children[childindex].desc=this.storedMenu[i].transdesc;
      this.topup=true;
    }
    if(this.storedMenu[i].transcode=='CHANGEPIN'){
      var index = this.menuItems.findIndex(x => x.menuCode ==="Settings");
      
      var childindex=this.menuItems[index].children.findIndex(x => x.menuCode ==="CHANGEPIN");
      this.menuItems[index].children[childindex].desc=this.storedMenu[i].transdesc;
      this.changepin=true;
    }
    if(this.storedMenu[i].transcode=='CHANGELANG'){
      // var index = this.menuItems.findIndex(x => x.menuCode ==="Settings");
      // var childindex=this.menuItems[index].children.findIndex(x => x.menuCode ==="CHANGELANG");
      // this.menuItems[index].children[childindex].desc=this.storedMenu[i].transdesc;
      // this.changelanguage=true;
    }
    if(this.storedMenu[i].transcode=='BULKPINPURCHASE'){
      var index = this.menuItems.findIndex(x => x.menuCode ==="Recharge");
      var childindex=this.menuItems[index].children.findIndex(x => x.menuCode ==="BULKPINPURCHASE");
      this.menuItems[index].children[childindex].desc=this.storedMenu[i].transdesc;
      this.voucher=true;
    }
    if(this.storedMenu[i].transcode=='TRANSSTATUS'){
      var index = this.menuItems.findIndex(x => x.menuCode ==="TRANSSTATUS");
      this.menuItems[index].desc=this.storedMenu[i].transdesc;
      this.history=true;
    }
    if(this.storedMenu[i].transcode=='MOVESTOCK'){
      var index = this.menuItems.findIndex(x => x.menuCode ==="MOVESTOCK");
      this.menuItems[index].desc=this.storedMenu[i].transdesc;
      this.transfer=true;
    }
    if(this.storedMenu[i].transcode=='ORDER'){
     //   // console.log(this.storedMenu[i].transcode);
      var index = this.menuItems.findIndex(x => x.menuCode ==="Orders");
      var childindex=this.menuItems[index].children.findIndex(x => x.menuCode ==="ORDER");
      this.menuItems[index].children[childindex].desc=this.storedMenu[i].transdesc;
      this.order=true;
    }
    if(this.storedMenu[i].transcode=='SEARCH'){
     
      var index = this.menuItems.findIndex(x => x.menuCode ==="Orders");
      var childindex=this.menuItems[index].children.findIndex(x => x.menuCode ==="SEARCH");
     
      this.menuItems[index].children[1].desc=this.storedMenu[i].transdesc;
      ;
      this.searchorder=true;
    
    }
  }
  
 
  var index = this.menuItems.findIndex(x => x.title ==="Settings");
  var childindex=this.menuItems[index].children.findIndex(x => x.title ==="Change Language");
//  // console.log("index , child",index,childindex);
 
  // if(this.topup==false && this.voucher==false){
  //   var index = this.menuItems.findIndex(x => x.title ==="Recharge");
  //   this.menuItems.splice(index,1);
  //     // console.log('case1');
  // }
  // if(this.topup==false && this.voucher!=false){
  //   var index = this.menuItems.findIndex(x => x.title ==="Recharge");
  //   var childindex=this.menuItems[index].children.findIndex(x => x.title ==="E-RECHARGE");
  //   this.menuItems[index].children.splice(childindex,1);
  //     // console.log('case1');
  // }
  // if(this.voucher==false && this.topup!=false){
  //   var index = this.menuItems.findIndex(x => x.title ==="Recharge");
  // var childindex=this.menuItems[index].children.findIndex(x => x.title ==="E-VOUCHER");
  //   this.menuItems[index].children.splice(childindex,1);
  //     // console.log('case1');
  // }
  // if(this.transfer==false){
  //   var index = this.menuItems.findIndex(x => x.title ==="Transfer");
  //   this.menuItems.splice(index,1);
  //     // console.log('case1');
  // }
  // if(this.balance==false){
  //   var index = this.menuItems.findIndex(x => x.title ==="Balance Enquiry");
  //   this.menuItems.splice(index,1);
  //     // console.log('case1');
  // }
 
  // if(this.history==false){
  //   var index = this.menuItems.findIndex(x => x.title ==="Transaction History");
  //   this.menuItems.splice(index,1);
  //     // console.log('case1');
  // }
  // if(this.agentHierarchy==false){
  //   var index = this.menuItems.findIndex(x => x.title ==="AGENT HIERARCHY");
  //   this.menuItems.splice(index,1);
  //     // console.log('case1');
  // }
  //  if( this.changepin==false){
  //   var index = this.menuItems.findIndex(x => x.title ==="Settings");
  //   this.menuItems.splice(index,1);
  //     // console.log('case1');
  // }
 
  
  // if(this.order==false && this.searchorder==false){
  //     // console.log('case1');
  //   var index = this.menuItems.findIndex(x => x.title ==="Orders");
  //   this.menuItems.splice(index,1);
  // }
  // if(this.order==false && this.searchorder!=false){
    
  //   var index = this.menuItems.findIndex(x => x.title ==="Orders");
  //   var childindex=this.menuItems[index].children.findIndex(x => x.title ==="ORDER");
  //   this.menuItems[index].children.splice(childindex,1);
    
  //     // console.log('case1');
  // }
  // if(this.searchorder==false && this.order!=false){
  //     // console.log('case3');
  //   var index = this.menuItems.findIndex(x => x.title ==="Orders");
  // var childindex=this.menuItems[index].children.findIndex(x => x.title ==="SEARCH ORDER");
  //   this.menuItems[index].children.splice(childindex,1);
  // }
  
}
  isMobileMenu() {
      if ( window.innerWidth > 576) {
          return false;
      }
      return true;
  };
  dropNav(i){
// for(var j=0;j<8;j++){
  
  if(this.menu[i]==true){
    this.menu[i]=false;
   
  }
  else{
    this.menu[i]=true;
  
  }
// }
  }
  closeNav(i){
    for(var j=0;j<8;j++){
      
      if(i!=j){
        this.menu[j]=false;
       
      }
      
    }
      }
  logOut(){

    Swal.fire({
      title: 'Log Out',
      text: "Are you sure?",
     
      showCancelButton: true,
      confirmButtonColor: '#F4A51C',
      cancelButtonColor: '#bfbcbc',
      confirmButtonText: 'Confirm'
    }).then((result) => {
      if (result.value) {
        this.authService.logout();
        
      }
    })
   
  }
}
