import { Component, HostListener} from '@angular/core';
import { UserIdleService } from 'angular-user-idle';
import {Router} from '@angular/router';
import * as CryptoJS from 'crypto-js';
import{ApiService} from './Services/api.service';
import{AuthGuard} from './Services/auth.guard';
import { HttpClient, HttpHeaders } from '@angular/common/http';
const httpOptions = {
  headers: new HttpHeaders({
    "Content-Type":"application/json",
 
   "auth.security.enable":"true",
   "authorization":"2DEF34A67B7C82850E2F531A0168375E4CF5977E6C1984553ECBA9869E379BA1"
  })
};
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
encryptData(dataToEncrypt: string) {

    
    var key = CryptoJS.enc.Utf8.parse(window.atob("MTIzNDU2Nzg5MDEyMzQ1Ng=="));
    // var key = "1234567890123456";
    var encrypted = CryptoJS.AES.encrypt(dataToEncrypt, key
    , {
      mode: CryptoJS.mode.ECB,
      padding: CryptoJS.pad.Pkcs7
    }
    );
      // console.log(encrypted)
    var enc=CryptoJS.enc.Hex.stringify(encrypted.ciphertext);

    return enc.toUpperCase();
  }
  constructor(private unlock:AuthGuard,private userIdle: UserIdleService,private router:Router,private apiService:ApiService,private http: HttpClient) {
     //Start watching for user inactivity.
     this.userIdle.startWatching();
    
     // Start watching when user idle is starting.
     this.userIdle.onTimerStart().subscribe(count => console.log(count));
     
     // Start watch when time is up.
     this.userIdle.onTimeout().subscribe(result =>{
       //this.unlock.decryptData()
      var agentcode=this.unlock.decryptData(localStorage.getItem('currentUser'));
        // console.log(agentcode);
      if(agentcode)
       this.apiService.logOutApi().subscribe(result =>{
        //location.reload();
        localStorage.clear();
        this.router.navigate(['']);
        this.userIdle.resetTimer();
       })
     } 
     
     );
  }
 
  ngOnInit() {
   
  }
 
  stop() {
    this.userIdle.stopTimer();
  }
 
  stopWatching() {
    this.userIdle.stopWatching();
  }
 
  startWatching() {
    this.userIdle.startWatching();
  }
 
  restart() {
    this.userIdle.resetTimer();
  }logOutRequest;
  @HostListener("window:beforeunload",["$event"])
  clearLocalStorage(event){
    this.userIdle.resetTimer();
    var agentcode=this.unlock.decryptData(localStorage.getItem('currentUser'));
      // console.log(agentcode);
    this.logOutRequest={
      "header": {
         "requesttype": "LOGOUT"
      },
      "request": {
         "agentcode":this.encryptData(agentcode),
          "vendorcode": this.encryptData("ALFA"),
      
         "clienttype": "SELFCARE",
         "version": this.encryptData("2.5"),
         "sign": this.encryptData("4n5MPDjukoJqotK5YbL5ExO2gvw"),
         "sessionid":this.encryptData(this.unlock.decryptData(localStorage.getItem('sessionid')))
      }
   }

      this.http.post('/services/alfaRestService',this.logOutRequest,httpOptions).subscribe(result=>
        {
          localStorage.removeItem('currentUser');
          localStorage.removeItem('agenttype');
          this.router.navigate(['']);
         
          localStorage.clear();
        })
        let xhr = new XMLHttpRequest()
     xhr.open("POST",'/services/alfaRestService',false);
     xhr.send(this.logOutRequest);
      // console.log(event);
      this.apiService.logOutApi().subscribe(result=>{
         // console.log(result);
       
      });
      localStorage.clear();
      
  }
}
